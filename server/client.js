/**
 * Redis client
 *
 * @description :: Create redis client and handles redis connectivity
 * @help        :: See https://redis.io/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   Folder renamed from redis to server.
 */

/**
 * Module dependencies.
 */
var redis = require('redis');
var logger = require('../util/logger');
var config = require('../configs/config.global');
var aiLogger = require('../util/aiLogger');

//var client = redis.createClient({host: '127.0.0.1', port: 6379});

/* Azure Dev Server */
//var client = redis.createClient(6380, 'azupsdsstred1.redis.cache.windows.net', {auth_pass: 'Ze6sjIhQGTl96rbe+mA3O3hbrskbRdlmpNUIbczv1Oo=', tls: {servername: 'azupsdsstred1.redis.cache.windows.net'}});
var client = redis.createClient(process.env.SSLCFG_REDISCACHE_PORT, process.env.SSLCFG_REDISCACHE_HOST, {auth_pass: process.env.SSLCFG_REDISCACHE_AUTHPASS, tls: {servername: process.env.SSLCFG_REDISCACHE_HOST}});

/* Azure QA Server */
//var client = redis.createClient(6380, 'azupsqfsstred1.redis.cache.windows.net', {auth_pass: 'yR5LSI/ZxptLojbv+Ut4rqDXOjmBywJRogZa7FaVv5k=', tls: {servername: 'azupsqfsstred1.redis.cache.windows.net'}});

/* Azure QAP Server */
//var client = redis.createClient(6380, 'azupsqpsstred1.redis.cache.windows.net', {auth_pass: 'p4P0AHI26Rkb3GnHDB3EXl3P74pRKjTPVg7+GeA2a0s=', tls: {servername: 'azupsqpsstred1.redis.cache.windows.net'}});

/**
 * Connecting redis server.
 */
client.on('connect', function () {
    console.log('Redis connected.');
});

/**
 * Checking redis server is ready.
 */
client.on('ready', function () {
    console.log("Redis is ready.");
});

/**
 * Handles redis server errors.
 */
client.on('error', function (error) {
    redisAlert(error);
});

/**
 * Method helps to log error.
 */
function redisAlert(err) {
    /* v0.2 - Begin : Exception handling changes. */
    logger.log(config.logger.level, 'Redis - Exception handler.', {type: 'Redis - Exception handler.', env: config.env, code: err.code, address: err.address, port: err.port, stack: err});
   //aiLogger.log(config.logger.level, JSON.stringify(err));
    /* v0.2 - End : Exception handling changes. */
}

/**
 * Exports client object.
 */
module.exports.client = client;









