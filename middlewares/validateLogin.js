'use strict';
// Import JSON Token library
const jwt = require('jsonwebtoken');
// Import Config settings
const config = require('../configs/config.global');
// Import User Model
const user = require("../models/user");
const errorObjectHelper = require('../helpers/errorTemplate');
const generalHelper = require('../helpers/genralHelper');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
const loginController = require('../controllers/loginController');
const tokenHelper = require('../helpers/tokenHelper');


/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This Express Middleware will check if the Request has valid token set or not
 */

// This file will act as the Login Check Middleware

module.exports = (req, res, next) => {
    req = generalHelper.reWriter(req);
    let mrktCd = req.params.mrktCd;
    let langCd = req.params.langCd;

    // Assume we are handling Session Token
    let tokenState = mro_constant.constantData.SESSION_TOKEN;
    // Token Decrypr Key
    var tokenDecryptKey = config.constant.SECRET;
    // check header or url parameters or post parameters for token
    var token = req.headers['x-access-token'];
    // Check if its the case for Refresh Token
    if (req.headers['x-refresh-token']) {
        tokenState = mro_constant.constantData.REFRESH_TOKEN;
        token = req.headers['x-refresh-token'];
        tokenDecryptKey = config.constant.REFRESHTOKENSECRET;
        if (req.url.match('logout')) {
            let error = new Error('Invalid Token MW');
            // Logout is not supposed to refresh token
            error.status = mro_constant.middlwrCustErrCds.refreshTokenInvalid;
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(
                errorFormater.mwErrorArray
            );
        }
    }
    if (token) {
        jwt.verify(token, tokenDecryptKey, (err, decoded) => {
            // Case 1: Incorrect JWT Token Received
            if (err) {
                // Check for JWT Token Error Type
                if (err.name) {
                    if (err.name == 'TokenExpiredError') {
                        // Token expired - Return Error
                        req.tokenExpired = true;
                        let error = new Error('Token Expired');
                        // Assume that Session Token has Expired
                        error.status = mro_constant.middlwrCustErrCds.tokeExpired;
                        // In case Refresh Token has expired, change the error message accordingly
                        if (tokenState === mro_constant.constantData.REFRESH_TOKEN) {
                            error.status = mro_constant.middlwrCustErrCds.refreshTokenExpired;
                        }
                        let errorData = errorObjectHelper(error);
                        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
                    }
                    // Token signature error
                    else {
                        let error = new Error('Invalid Token MW');
                        // Assume that Session Token has Expired
                        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
                        // In case Refresh Token has expired, change the error message accordingly
                        if (tokenState === mro_constant.constantData.REFRESH_TOKEN) {
                            error.status = mro_constant.middlwrCustErrCds.refreshTokenInvalid;
                        }
                        let errorFormater = errorObjectHelper(error);
                        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(
                            errorFormater.mwErrorArray
                        );
                    }
                }
            }
            // Case 2: Correct JWT Token received
            else {
                // if everything is good, extract the JWT payload ane proceed to the route
                // Check the device ID and match it from the Payload
                // Find the AGS Token based on the Account Number and return the same
                let uniqueID = generalHelper.decrypt(decoded.uniqueid);
                let acctNum = generalHelper.decrypt(decoded.acctNr);
                if (uniqueID == req.headers['uniqueid'] &&
                    acctNum == req.params.acctNr) {
                    user.findOne({ acctNr: acctNum },
                        (err, userData) => {
                            // Error in find the record in DB
                            if (err) {
                                let errorObj = new Error('DB Error');
                                errorObj.status = mro_constant.middlwrCustErrCds.dbError;
                                let errorFormater = errorObjectHelper(errorObj);
                                return res.status(mro_constant.httpErrCds.internalServerError)
                                    .send(errorFormater.mwErrorArray);
                            }
                            // Got the AGS token from DB
                            else {
                                // Attach the AGS Tokens to the Original Request
                                req.tokenPayload = userData;
                                // Check if tokenState is REFRESH_TOKEN
                                if (tokenState === mro_constant.constantData.REFRESH_TOKEN) {
                                    // Refresh the Token via AGS
                                    loginController.refreshtokenController(req, res)
                                        .then(tokenObj => {
                                            // Set the Session Token
                                            res.setHeader('x-access-token', tokenObj.sessionToken);
                                            // Set the Refresh Token
                                            res.setHeader('x-refresh-token', tokenObj.refreshToken);
                                            // Set the Token Expiry
                                            res.setHeader('x-access-expiry', config.constant.TOKENLIFE)
                                            res.setHeader('x-refresh-expiry', config.constant.REFRESHTOKENLIFE);
                                            // Proceed to the Original Request handler
                                            // Get the Updated Token again from AGS
                                            user.findOne({ acctNr: acctNum },
                                                (err, UpdatedUserData) => {
                                                    if (err) {
                                                        let errorObj = new Error('DB Error');
                                                        errorObj.status = mro_constant.middlwrCustErrCds.dbError;
                                                        let errorFormater = errorObjectHelper(errorObj);
                                                        return res.status(mro_constant.httpErrCds.internalServerError)
                                                            .send(errorFormater.mwErrorArray);
                                                    } else {
                                                        req.tokenPayload = UpdatedUserData
                                                        next();
                                                    }
                                                });
                                        })
                                        .catch(error => {
                                            // Unexpected Error
                                            let errorObj = new Error('Unexpected Error in Refreshing Token');
                                            errorObj.status = mro_constant.middlwrCustErrCds.unexpectedError;
                                            let errorFormater = errorObjectHelper(errorObj);
                                            return res.status(mro_constant.httpErrCds.internalServerError)
                                                .send(errorFormater.mwErrorArray);
                                        })
                                }
                                // SESSION_TOKEN case
                                else {
                                    // Generate a new SESSION_TOKEN and Append in the Response
                                    tokenHelper(uniqueID, acctNum)
                                        .then(token => {
                                            // Set the Session Token
                                            res.setHeader('x-access-token', token);
                                            res.setHeader('x-access-expiry', config.constant.TOKENLIFE)
                                            // Proceed to the Original Request Handler
                                            next();
                                        })
                                        .catch(error => {
                                            // Unexpected Error
                                            let errorObj = new Error('Unexpected Error in Refreshing Token');
                                            errorObj.status = mro_constant.middlwrCustErrCds.unexpectedError;
                                            let errorFormater = errorObjectHelper(errorObj);
                                            return res.status(mro_constant.httpErrCds.internalServerError)
                                                .send(errorFormater.mwErrorArray);
                                        })
                                }
                            }
                        });
                } else {
                    let errorObj = new Error('Params Not Matching');
                    errorObj.status = mro_constant.middlwrCustErrCds.paramsNotMatching;
                    logger.log(config.logger.level, 'validateLogin - Exception handler.', {
                        type: 'validateLogin Middleware.',
                        env: config.env,
                        message: errorObj,
                    });
                    let errorFormater = errorObjectHelper(errorObj);
                    return res.status(mro_constant.httpErrCds.UnauthorizedAccess)
                        .send(errorFormater.mwErrorArray);
                }
            }
        });
    } else {
        // if there is no token
        // return an error
        let error = new Error('Unauthorized');
        error.status = mro_constant.middlwrCustErrCds.tokenRequired;
        logger.log(config.logger.level, 'validateLogin - Exception handler.', {
            type: 'validateLogin Middleware.',
            env: config.env,
            message: error,
        });
        let errorFormater = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.forbiddenAccess).send(
            errorFormater.mwErrorArray
        );
    }
}