/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This Express Middleware will act as Global Error Handler
 */

module.exports = (e, req, res, next) => { 
    res.status(e.status || 500);
    res.json({
        message: e.message,
        error: e
    });
}