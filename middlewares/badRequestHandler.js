/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This Express Middleware will act as Global Bad Request handler
 */
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
let config = require('../configs/config.global');

module.exports = (e, req, res, next) => { 
    if (e.message === "Bad request") {
        let error = new Error('Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        logger.log(config.logger.level, 'badRequest - Exception handler.', 
        { type: 'badRequest-Middleware', 
            env: config.env, 
            message: error
        });
        let errorFormater = errorObjectHelper(error);
    	res.status(400).json({error: e.message});
    }
}