'use strict'
const logger = require('../util/logger');
const errorObjectHelper = require('../helpers/errorTemplate');
const generalHelper = require('../helpers/genralHelper');
const mro_constant = require('../helpers/constants');
let config = require('../configs/config.global');
/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This Express Middleware will check if the Request has all the mandatory parameters set
 */

module.exports = (req, res, next) => {
    if (!req) {
        let error = new Error('Request aborted - Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        logger.log(config.logger.level, 'validateRequest - Exception handler.',
            {
                type: 'validateRequest-Middleware',
                env: config.env,
                message: error
            });
        let errorFormater = errorObjectHelper(error);
        res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
    }
    req = generalHelper.reWriter(req);
    if (req) {
        // Check for Market and Languange in every API Request
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        // Check for Market and Languange in every API Request
        //Check if any of the 2 paramaters are missing in the request
        if (!mrktCd || !langCd) {
            let error = new Error('Missing Params');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            logger.log(config.logger.level, 'validateRequest - Exception handler.',
                {
                    type: 'validateRequest-Middleware',
                    env: config.env,
                    message: error
                });
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
        }
        // Make the Market and Language code to be Uppercase as per AGS requirement
        req.params.langCd = req.params.langCd.toUpperCase();
        req.params.mrktCd = req.params.mrktCd.toUpperCase();
        // If no errors found, then forward request to Route Handler
        next();
    }
}
