'use strict'
const logger = require('../util/logger');
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
let config = require('../configs/config.global');
const generalHelper = require('../helpers/genralHelper');
/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This Express Middleware will check if the Request has all the mandatory parameters set
 */
module.exports = (req, res, next) => {
    if (!req) {
        let error = new Error('Request aborted - Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        logger.log(config.logger.level, 'validateRequest - Exception handler.',
            {
                type: 'validateReqParams-Middleware',
                env: config.env,
                message: error
            });
        let errorFormater = errorObjectHelper(error);
        res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
    }
    req = generalHelper.reWriter(req);
    if (req) {
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let dwnldType = req.params.downloadType;
        let mrktConfigType = req.params.mrktConfigType;
        // Make the request params to the desired cases(upper/lower) 
        if (mrktCd) {
            req.params.mrktCd = req.params.mrktCd.toUpperCase();
        }
        if (langCd) {
            req.params.langCd = req.params.langCd.toLowerCase();
        }
        if (dwnldType) {
            req.params.downloadType = req.params.downloadType.toLowerCase();
        }
        /* if (mrktConfigType) {
            req.params.mrktConfigType = req.params.mrktConfigType.toLowerCase();
        } */
        // If no errors found, then forward request to Route Handler
        next();
    }
}
