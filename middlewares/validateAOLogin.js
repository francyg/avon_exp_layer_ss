'use strict';

const cfg = require('../configs/config.global');
const env = cfg.env || 'dev', config = require('../configs/config.' + env);
// let config = require('../configs/config.global');

const errorObjectHelper = require('../helpers/errorTemplate');
const helper = require("../helpers/agsHelper");
let logger = require('../util/logger');
let validator = require('../util/validator');

module.exports = (req, res, next) => {
    // Request Inputs
    let mrktCd = req.params.mrktCd;
    let langCd = req.params.langCd;
    let userId = req.params.userId;
    let acctNr = req.params.acctNr;
    let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];
    // let refreshToken = req.headers["x-rfrsh-token"];

    // Fetch AGS Response
    let agsConfigs = config.ao.ags;
    let agsRefreshConfigs = config.ao.ags.renewAccessToken;
    let payLoad = {};
    let version = agsRefreshConfigs.versions.hasOwnProperty(mrktCd) ? agsRefreshConfigs.versions[mrktCd] : agsRefreshConfigs.versions["default"];
    payLoad.heartBeatReq = {
        devKey: agsConfigs.devKey,
        version: version,
        mrktCd: mrktCd,
        userId: userId,
        acctNr: acctNr,
        token: token
    };
    let agsURL = agsRefreshConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsRefreshConfigs.classicBaseURLs[mrktCd] : agsRefreshConfigs.classicBaseURLs["default"];
    let options = {
        method: agsRefreshConfigs.method,
        url: agsURL + agsRefreshConfigs.apiEndPoint,
        data: payLoad,
        timeout: agsConfigs.timeOut
    };
    let inputs = {
        controllerName: "AO Login Validator",
        respObjName: "heartBeatResp",
        serviceName: options.url
    };
    helper.invokeService(options, inputs)
        .then(agsResponse => {
            if (agsResponse.success) {
                agsResponse = agsResponse.response;
                if (typeof agsResponse != "undefined" && agsResponse.heartBeatResp.hasOwnProperty("success") && !agsResponse.heartBeatResp.success) {
                    logger.log(config.logger.level, 'AO Login Validator - Exception handler', {
                        type: 'AGS returned error response - Refresh Heart Beat',
                        env: config.env,
                        message: "Refresh Heart Beat Service failed",
                        stack: agsResponse
                    });
                    res.status(401);
                    return res.send({
                        error: {
                            "code": agsResponse.heartBeatResp.code,
                            "message": agsResponse.heartBeatResp.msg
                        }
                    });
                }
                else {
                    res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, validator.checkNotNull(agsResponse.heartBeatResp.token) ? agsResponse.heartBeatResp.token : null);
                    next();
                }
            }
            else {
                res.status(agsResponse.status);
                return res.send({
                    error: agsResponse.error
                });
            }
        })
        .catch(error => {
            logger.log(config.logger.level, 'AO Login Validator - Exception handler', {
                type: 'From Catch block of AO Login Validator',
                env: config.env,
                message: error.message,
                statusText: error.statusText,
                status: error.status,
                stack: error.stack
            });
            let errObj = errorObjectHelper(error);
            return res.status(error.httpCode || error.status).send({
                error: {
                    "code": agsConfigs.unknownErrorCode,
                    "message": agsConfigs.unknownErrorMsg
                }
            });
        });
}