/**
 * cryptos
 *
 * @description :: Handles encrypt decrypt logic
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var crypto = require('crypto'),
        algorithm = 'aes-256-ctr',
        password = 'd6F3Efeq';

/**
 * Encryption logic.
 */
module.exports.encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

/**
 * Decryption logic.
 */
module.exports.decrypt = function (cryptedText) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(cryptedText, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}