/**
 * aiLogger
 *
 * @description :: Handles error logging
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 */

/**
 * Module dependencies.
 */
var appInsights = require("applicationinsights");
var aiLogger = require('winston-azure-application-insights').AzureApplicationInsightsLogger;
var winston = require('winston');

var Config = require('../configs/config.global');

appInsights.setup(Config.insights.instrumentationKey)
        .setAutoDependencyCorrelation(Config.insights.autoDependencyCorrelation)
        .setAutoCollectRequests(Config.insights.autoCollectRequests)
        .setAutoCollectPerformance(Config.insights.autoCollectPerformance)
        .setAutoCollectExceptions(Config.insights.autoCollectExceptions)
        .setAutoCollectDependencies(Config.insights.autoCollectDependencies)
        .start();
/**
 * Use an existing app insights SDK instance 
 */

winston.emitErrs = true;

// Create a new app insights client with another key
winston.add(aiLogger, {
    client: appInsights.getClient(Config.insights.instrumentationKey)
});

/**
 * Exports winston object.
 */
module.exports = winston;