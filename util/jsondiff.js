/**
 * jsondiff
 *
 * @description :: Handles delta logic
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Creates JSONDiff object.
 */
var JSONDiff = JSONDiff || {};

/**
 * Method to find delta between two json objects.
 */
JSONDiff.delta = function (objOne, objTwo) {
    var self = this;
    var k, kDiff, diff = {};
    for (k in objOne) {
        if (!objOne.hasOwnProperty(k)) {
        } else if (typeof objOne[k] != 'object' || typeof objTwo[k] != 'object') {
            if (!(k in objTwo) || objOne[k] !== objTwo[k]) {
                diff[k] = objTwo[k];
            }
        } else if (kDiff = self.delta(objOne[k], objTwo[k])) {
            diff[k] = kDiff;
        }
    }
    for (k in objTwo) {
        if (objTwo.hasOwnProperty(k) && !(k in objOne)) {
            diff[k] = objTwo[k];
        }
    }
    for (k in diff) {
        if (diff.hasOwnProperty(k)) {
            return diff;
        }
    }
    return false;
};

JSONDiff.checkValueExists = function (jsonArr, key, value) {
    var hasMatch = false;
    for (var i = 0; i < jsonArr.length; i++) {
        if (jsonArr[i][key] == value) {
            hasMatch = true;
            break;
        }
    }
    return hasMatch;
};

/**
 * Exports JSONDiff object.
 */
module.exports.JSONDiff = JSONDiff;