/**
 * app.js
 *
 * @description :: Initializes the app and glues everything together
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   Added MongoDB connection pool.
 *                      -   Handled bad requests (400).
 */

/**
 * Module dependencies.
 */
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var brochureAPIs = require('./api_routes/brochures');
var configAPIs = require('./api_routes/config');
var translationAPIs = require('./api_routes/translations');
var marketAPIs = require('./api_routes/markets');
var svcVersionsAPIs = require('./api_routes/svcversions');
var helpsAPIs = require('./api_routes/helps');
var gamificationAPIs = require('./api_routes/gamification');
var sharecontentAPIs = require('./api_routes/sharecontent');
var faqAPIs = require('./api_routes/faq');
var avoncausesAPIs = require('./api_routes/avoncauses');
var stepinstructAPIs = require('./api_routes/stepinstruct');
var analyticsAPIs = require('./api_routes/analytics');
var lmscontentAPIs = require('./api_routes/lmscontent');
var notificationAPIs = require('./api_routes/notification');
var helpsupportAPIs = require('./api_routes/helpsupport');
var serviceurlAPIs = require('./api_routes/serviceurl');
var termsCondtionsAPIs = require('./api_routes/termscondtions');
var helpsupportAPIs = require('./api_routes/helpsupport');
var aoAppRoute = require('./api_routes/aoAppRoute');
var loginRoute = require('./api_routes/userRoute');
var userRoute = require('./api_routes/loginRoute');
var sshRoute = require('./api_routes/socialsharinghub');
var dashboardRoute = require('./api_routes/dashboard');
var ugcRoute = require('./api_routes/ugc');
var avonInboxAPIs = require('./api_routes/avonnotification');
// Importing the Lead Route
var leadRoute = require('./api_routes/leadRoute');
// Importing the Static Code Route
var codesRoute = require('./api_routes/staticCodeRoute');
var cors = require('cors');
// Importing the appointment Route
var appointmentsRoute = require('./api_routes/appointmentsRoute');
//var mongoose = require('mongoose');
//var secrets = require('./configs/config.mongo');
var logger = require('./util/logger');
var morgan = require('morgan');
var Config = require('./configs/config.global');
var expressSanitizer = require('express-sanitizer');
var session = require('cookie-session');
var expiryDate = new Date(Date.now() + 60 * 30 * 1000) // 0.5 hour
var helmet = require('helmet');

var app = express();
app.use(cors());
app.set('trust proxy', 1) // trust first proxy
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// Mount express-sanitizer here
app.use(expressSanitizer()); // this line follows bodyParser() instantiations
app.use(cookieParser());
// app.disable('x-powered-by');
 app.set('etag', false); // turn off
// app.use(session({
//     name: 'session',
//     keys: ['key1', 'key2'],
//     cookie: {
//       secure: true,
//       httpOnly: true,
//       domain: 'example.com',
//       path: 'foo/bar',
//       expires: expiryDate
//     }
//   }))
app.use(helmet.hidePoweredBy({setTo: 'Server 1.0'})); //change value of X-Powered-By header to given value
app.use(helmet.noCache({noEtag: true})); //set Cache-Control header
app.use(helmet.noSniff());    // set X-Content-Type-Options header
app.use(helmet.frameguard()); // set X-Frame-Options header
app.use(helmet.xssFilter());  // set X-XSS-Protection header
  app.use(session({
    name: 'SESS_ID',
    secret: Config.constant.SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: true,
        httpOnly: true,
        expires: expiryDate
    }
}));
//**********Initializing the DB Connection to Cosmos DB via Mongoose JS ****************/
const mongoose = require('./configs/mongo')
//****************Connection Created****************************************************/

/**
 * Includes all the routes.
 */
app.use('/', brochureAPIs, configAPIs, translationAPIs, marketAPIs, svcVersionsAPIs, helpsAPIs, gamificationAPIs, sharecontentAPIs, faqAPIs, avoncausesAPIs, stepinstructAPIs, analyticsAPIs, lmscontentAPIs, notificationAPIs, helpsupportAPIs, serviceurlAPIs,termsCondtionsAPIs, aoAppRoute, userRoute, loginRoute, appointmentsRoute, leadRoute, codesRoute,avonInboxAPIs, sshRoute, dashboardRoute, ugcRoute);

/**
 * Catch 400 and forward to error handler.
 */
app.all('*', function (req, res, next) {
    //throw new Error("Bad request")
    var err = new Error('Bad Request');
    err.status = 400;
    next(err);
});

var port = process.env.PORT ||'5031';
app.listen(port, () => console.log('Avon App listening on port 5031'));