'use strict'
var mongoose = require('mongoose');
let ObjectId = mongoose.Schema.ObjectId;
//User Schema
// Added AGS Refresh Token in the User Schema
var UserSchema = new mongoose.Schema({
  _id : { type: ObjectId, required: true },
  acctNr:{ type: String, required: true },
  userId:{ type: String, required: true },
  mrktCd:{ type: String, required: true },
  lngCd:{ type: String, required: true },
  acctType:{ type: String, required: true },
  appVersion:{type:String,default: 'v1'},
  AGS_Token:{ type: String, required: true },
  AGS_Refresh_Token: { type: String, required: true},
  createdAt:{ type: Date,required: true, default: Date.now },
  lastUpdatedAT:{ type: Date }
}, {collection : "user"});

// // Sets the createdAt parameter equal to the current time
UserSchema.pre('save', next => {
  let now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

//Creating User model
module.exports = mongoose.model("user", UserSchema);