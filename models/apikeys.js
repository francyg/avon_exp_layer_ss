/**
 * apikeys model
 *
 * @description :: Represents data, implements business logic and handles storage
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

/**
 * Defining apikeys schema Model in mongoose.
 */
var apikeysSchema = new mongoose.Schema({
}, {
    collection: 'apikeys'
});

/**
 * Exports apikeys model object.
 */
module.exports = mongoose.model('apikeys', apikeysSchema);
