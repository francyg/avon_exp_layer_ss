/**
 * configs model
 *
 * @description :: Represents data, implements business logic and handles storage
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

/**
 * Defining configs schema Model in mongoose.
 */
var appconfigSchema = new mongoose.Schema({
}, {
    collection: 'configs'
});

/**
 * Exports configs model object.
 */
module.exports = mongoose.model('configs', appconfigSchema);
