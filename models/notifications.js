/**
 * notifications model
 *
 * @description :: Represents data, implements business logic and handles storage
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *              0.5     -   One collection different documents.        
 */

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');

/**
 * Defining notification schema Model in mongoose.
 */
var notificationsSchema = new mongoose.Schema({
}, {
    collection: 'notifications'
});

/**
 * Exports brochures model object.
 */
module.exports = mongoose.model('notifications', notificationsSchema);
