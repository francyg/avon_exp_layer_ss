/**
 * @author ajay dua
 * @dated 18-june-2018
 * update on @dated 06-AUG-2018
 * added aplctn_nm to the schema
 * TODO: should not be defualt value, once integrated should be changed to required field
 * default: 'com.avon.mra.qa'
 * Purpose follows
 * This class will hold the INBOX-API Notification mongoose Model.  
  * note: the code was already written and working for socialsailing app.
 * requirement: needed to be a part of exp. layer so the code is moved here in experience layer.
 * added functionality: TODO:discuss and apply if required schema change msg_id => msg_nm
 /**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Defining notification schema Model in mongoose.
 */
var avonnotificationSchema = new mongoose.Schema({
	 _id: Schema.Types.ObjectId,
    mrkt_cd:{type:String,required:true},
	user_id:{type:String,required:true},
	lang_cd:{type:String,required:true},
	ntfctn_typ:{type:String,required:true},
	aplctn_nm:{type:String, default: 'com.avon.mra.qa'},
	app_nm:{type:String},
	messages: [{}],
	brdcst_read_msg_list:{} 
}, {versionKey: false, collection: 'avonnotification'});

/**
 * Exports brochures model object.
 */
module.exports = mongoose.model('avonnotification', avonnotificationSchema);
