/**
 * notificationCtlr
 *
 * @description :: Server-side logic for managing notification APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var notifications = require('../models/notifications');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var JSONDiff = require('../util/jsondiff').JSONDiff;
var constants = require('../util/constants');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /notification
 * Returns notification task details.
 */
exports.getUserTasks = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        var apiVersion = parseInt(req.params.apiVrsn);

		var appNm = null;

        // Support for new format where AppNm is URL Params
        if (req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if (req.query.appNm) {
            appNm = req.query.appNm
        }
		
        var langCd = req.params.langCd;
        var userid = req.headers['sstuserid'];
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';

        var data = data || {};
        var condition = condition || {};
        condition.api_version = apiVersion;
		condition.app_nm = appNm;
        condition.mrkt_cd = mrktCd;
        condition.lang_cd = langCd;
        if (currDataVer) {
            condition.data_version = parseInt(currDataVer);
        }
        if (userid) {
            var redisKey = appNm + '_usertasks_' + apiVersion + '_' + mrktCd + '_' + langCd + '_' + userid;
            client.exists(redisKey, function (err, reply) {
                if (!err) {
                    if (reply === 1) {
                        client.get(redisKey, function (err, uTasksData) {
                            if (err) {
                                res.send({status: false, message: 'Error while reading from Redis cache.'});
                            } else {
                                var userTasksData = JSON.parse(uTasksData);
                                res.send(userTasksData);
                                res.end();
                            }
                        });
                    } else {
                        notifications.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                            if (!err) {
                                if (typeof row != 'undefined' && row != null) {
                                    data._id = row._id;
                                    data.mrkt_cd = row.mrkt_cd;
                                    data.lang_cd = row.lang_cd;
                                    data.api_version = row.api_version;
                                    data.data_version = row.data_version;
                                    data.lookup_id = row.lookup_id;
                                    data.usertasks = [];
                                    (row.usertasks).forEach(function (task) {
                                        if (task.task_users.search(userid) >= 0) {
                                            delete task.task_users;
                                            data.usertasks.push(task);
                                        }
                                    });
                                    client.set(redisKey, JSON.stringify(data));
                                }
                                res.send(data);
                                res.end();
                            } else {
                                res.send({success: false, message: 'Error while fetching Notifications.'});
                                res.end();
                            }
                        });
                    }
                } else {
                    res.send({success: false, message: 'Error while reading from Redis cache - ' + redisKey});
                    res.end();
                }
            });
        } else {
            res.send({success: false, message: 'Invalid userID.'});
            res.end();
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Notifications - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};












