/**
 * @author ajay dua
 * @dated 18-june-2018
 * 
 * Purpose follows
 * This class will hold the INBOX-API & preferences, and would call corresponding APIs on AGS-Clasic and response back 
 * to this controller.
 * 
 * this also holds APIs for updating preferences of the User.
 * 
 * note: the code was already written and working for socialsailing app.
 * requirement: needed to be a part of exp. layer so the code is moved here in experience layer.
 *
 * Module dependencies. */
let mongoose = require('mongoose');
const DateDiff = require('date-diff');
/**
 * imports model avonnotification for the CRUD operations 
 */
const avonnotifications = require('../models/avonnotification');
//const getUserPrefTemplate = require('../helpers/getDevicePrefTemplate');
/**
 * imports constants for the AGNS msg_type 
 */
var constants = require('../helpers/constants');
var Config = require('../configs/config.global');
let logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');
const helper = require("../helpers/loginHelper");
//const deviceRegPrefTemplate = require('../helpers/deviceRegPrefHelper');
const dbhelper = require("../helpers/databaseInteractor");
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
/** GET getInboxData Purpose :
 
* To return list of messages for the account number received in request.
* Messages must be read from the COSMOS DB collection.
* All messages associated with the account number as well as broadcast messages must be returned. 
* viewed_ind to be used to mark the message as read/unread must be part of response for each message.
* Done : Only the broadcast messages that are generated after the account number was registered must be returned in response. 
* For this part of the functionality, registration date of the user should be sent as part of Get Message API request for 
identifying the broadcast messages to be part of response.
 As broadcast messages will be maintained as a single independent document in COSMOS database collection; the viewed_ind 
of broadcast message should be managed by updating the brdcst_read_msg_list attribute of each user document with the broadcast
 message id once it is read by the user.
TODO: discuss and apply if required schema change msg_id => msg_nm, introduced lookup_id
 */
exports.getInboxData = function (req, res) {
	try {
		// following variables are the serach criteria for fetching user specific details for individual & Broadcast messages. 
		var mrktCd = req.params.mrktCd;
		var userId = req.params.acctNr;
		//var userId = req.tokenPayload.userId;
		//TODO change it:var langCd = req.params.langCd;
		var langCd = req.params.langCd.toLowerCase();
		//var regDate = new Date(req.tokenPayload.registerationDate);
		var regDate = new Date(req.query.usrRegDate);
		var currentDate = new Date();
		var weekDiff = null;
		var indMsgCreationDate = null;
		var brdMsgCreationDate = null;
		var regMsgDiff = null;
		var resMsgStructure = null;
		let whereCondition = {
			mrkt_cd: mrktCd,
			user_id: userId,
			lang_cd: langCd,
			ntfctn_typ: constants.AGNS_NTFCTN_TYP.INDV
		};
		if (req.params.appNm !== "mra") {
			whereCondition["app_nm"] = req.params.appNm
		}
		//find all INDVIDUAL massages for this user, would be one document returned.
		avonnotifications.findOne(whereCondition)
			.lean()
			//note: removed sort & limit not required here since there would be only one Indvidual document per user in the entire collection.
			.exec(function (err, row) {
				if (!err) {
					//data found for this account/user
					var broadcastMsgReadList = {};
					if (row) {
						broadcastMsgReadList = row.brdcst_read_msg_list;
					}
					//find all BRODCAST msgs for this user 
					whereCondition = {
						mrkt_cd: mrktCd,
						lang_cd: langCd,
						ntfctn_typ: constants.AGNS_NTFCTN_TYP.BRODC
					};
					if (req.params.appNm !== "mra") {
						whereCondition["app_nm"] = req.params.appNm
					}
					avonnotifications.findOne(whereCondition)
						.lean()
						//note: removed sort & limit not required here since there would be only one broadcast document in the entire collection.
						.exec(function (broadcastErr, broadcastMsg) {
							if (!broadcastErr) {
								//prepare the message for response push all the msgs with ntfctn_typ = INDV
								var msgList = [];
								if (row && row.messages && Array.isArray(row.messages)) {
									for (var msgCnt = 0; msgCnt < row.messages.length; msgCnt++) {
										var msg = row.messages[msgCnt];
										msg.ntfctn_typ = constants.AGNS_NTFCTN_TYP.INDV;
										indMsgCreationDate = row.messages[msgCnt].creat_ts
										weekDiff = new DateDiff(currentDate, indMsgCreationDate);
										//return msg should not be a deleted msg
										if (weekDiff.weeks() < 4 && msg.deleted_ind === 0) {
											if (req.params.appNm == "mra") {
												resMsgStructure = finalIndRespnse(msg);
											}
											else {
												resMsgStructure = msg;
											}
											msgList.push(resMsgStructure);
										}
									}
								}

								if (broadcastMsg && broadcastMsg.messages && broadcastMsg.messages.length) {
									//Note: below code is a logic for BRODCAST shoud not be deleted.
									//prepare the message for response push all the msgs with ntfctn_typ = BRODCAST
									for (var bcmsgCnt = 0; bcmsgCnt < broadcastMsg.messages.length; bcmsgCnt++) {
										/* brdMsgCreationDate = broadcastMsg.messages[bcmsgCnt].creat_ts;
										// check if user registration date is earlier then msg creation date
										regMsgDiff = new DateDiff(brdMsgCreationDate, regDate);
										//OLDCODE : if (new Date(broadcastMsg.messages[bcmsgCnt].creat_ts.toString()) > new Date(regDate.toString()) {
										//console.log(regMsgDiff.seconds());
										if (regMsgDiff.seconds() > 0) { */
										var bcmsg = broadcastMsg.messages[bcmsgCnt];
										indMsgCreationDate = bcmsg.creat_ts
										weekDiff = new DateDiff(currentDate, indMsgCreationDate);
										//return msg should not be a deleted msg
										if (weekDiff.weeks() < 4) {
											//SET delete and viewed index values
											if (broadcastMsgReadList && broadcastMsgReadList[bcmsg.inbox_id]) {
												bcmsg.viewed_ind = broadcastMsgReadList[bcmsg.inbox_id].viewed_ind ? broadcastMsgReadList[bcmsg.inbox_id].viewed_ind : 0;
												bcmsg.deleted_ind = broadcastMsgReadList[bcmsg.inbox_id].deleted_ind ? broadcastMsgReadList[bcmsg.inbox_id].deleted_ind : 0;
											}
											else {
												bcmsg.viewed_ind = 0;
												bcmsg.deleted_ind = 0;
											}
											bcmsg.ntfctn_typ = constants.AGNS_NTFCTN_TYP.BRODC;
											//comsole.log();
											if (bcmsg.deleted_ind === 0) {
												msgList.push(bcmsg);
											}
										}
										// }
									}
								}
								var resp = {
									msgList: msgList
								};
								res.status(200).send(resp);
								res.end();
							} else {
								//BRD-CAST msg Not Found
								var resp = {
									msgList: []
								};
								res.status(200).send(resp);
								res.end();
							}
						})
				} else {
					//IND msg not Found
					var resp = {
						msgList: []
					};
					res.status(200).send(resp);
					res.end();
				}
			});
	} catch (e) {
		/* v0.2 - Begin : Exception handling changes. */
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'avonNotificationCtrl- Get - Exception handler.' });
		aiLogger.log(Config.logger.level, e.message);
		res.status(500).send({ status: false, message: 'Internal server error.' });
		res.end();
		/* v0.2 - End : Exception handling changes. */
	}
};
/**
 
	Purpose of this API is:
	*	To update viewed indicator based on viewed_ind in request
	*	viewed_ind for INDIVIDUAL messages
	*	brdcst_read_msg_list for BROADCAST messages. 
*/
exports.updateInboxData = function (req, res) {
	try {
		// following variables are the serach criteria for fetching user specific details for individual & Broadcast messages. 
		var mrktCd = req.params.mrktCd;
		var userId = req.params.acctNr;
		//TODO change it:var langCd = req.params.langCd;
		var langCd = req.params.langCd.toLowerCase();
		var msgesResponse = req.body.msgList;
		var msgToUpdate = req.body.msgList;
		let whereCondition = {
			mrkt_cd: mrktCd,
			user_id: userId,
			lang_cd: langCd,
			ntfctn_typ: constants.AGNS_NTFCTN_TYP.INDV
		};
		if (req.params.appNm !== "mra") {
			whereCondition["app_nm"] = req.params.appNm
		}
		avonnotifications.findOne(whereCondition)
			.lean()
			.exec(function (err, dbResp) {
				if (!err) {
					if (!dbResp) {
						dbResp = {};
					}
					let finalResult = [];
					var broadcastReadList = dbResp.brdcst_read_msg_list || {};
					var messages = dbResp.messages || [];
					for (var msgToUpdtCnt = 0; msgToUpdtCnt < msgToUpdate.length; msgToUpdtCnt++) {
						var msg = msgToUpdate[msgToUpdtCnt];
						if (msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.BRODC) {
							//	console.log('In broadcast if' + JSON.stringify(broadcastReadList[msg.inbox_id]));
							if (broadcastReadList[msg.inbox_id]) {
								broadcastReadList[msg.inbox_id].viewed_ind = msg.viewed_ind
							} else {
								var newBrdcastReadMsg = {
									'viewed_ind': msg.viewed_ind
								};
								broadcastReadList[msg.inbox_id] = newBrdcastReadMsg;
							}
						}
						else if (msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.INDV) {
							// if (dbResp.messages && dbResp.messages.length) {
							// for (var dbMsgCnt = 0; dbMsgCnt < dbResp.messages.length; dbMsgCnt++) {
							for (var dbMsgCnt = 0; dbMsgCnt < messages.length; dbMsgCnt++) {
								if (msg.inbox_id == messages[dbMsgCnt].inbox_id) {
									messages[dbMsgCnt].viewed_ind = msg.viewed_ind;
									finalResult.push(messages[dbMsgCnt]);
									break;
								}
							}
							// }
						}
					}
					let updtData = {};
					updtData.brdcst_read_msg_list = broadcastReadList;
					updtData.messages = messages;
					/* dbResp.brdcst_read_msg_list = broadcastReadList;
					delete dbResp._id; */
					avonnotifications.update(whereCondition, updtData
						/*dbResp*/, { 'upsert': true }, function (err, updatedRow) {
							if (err) {
								expLayerSetError(res);
							}
							else {
								/* // Logic to send back only the Updated Records								
								for (var index = 0; index < msgToUpdate.length; index++) {
									var msg = msgToUpdate[index];
									for (var updatedRowIndex = 0; updatedRowIndex < updatedRow.messages.length; updatedRowIndex++) {
										console.log(msg.ntfctn_typ);
										console.log(updatedRow);
										if (msg.ntfctn_typ == updatedRow.ntfctn_typ && msg.inbox_id == updatedRow.messages[updatedRowIndex].inbox_id) {
											finalResult.push(updatedRow.messages[updatedRowIndex]);
											break;
										}
									}
								} */
								if (req.params.appNm === "mra") {
									res.status(200).send(finalResult);
								}
								else {
									res.status(204).send();
								}
								res.end();
							}
						});
				} else {
					expLayerSetError(res);
				}
			});
	} catch (e) {
		/* v0.2 - Begin : Exception handling changes. */
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'avonNotificationCtrl-update - Exception handler.' });
		aiLogger.log(Config.logger.level, e.message);
		res.status(500).send({ status: false, message: 'Internal server error.' });
		res.end();
		/* v0.2 - End : Exception handling changes. */
	}
};
function finalIndRespnse(msg) {
	//TODO: if condition for BRDCST or IND msg
	delete msg.deleted_ind
	return msg;
}
/**
 Purpose of this API is:
	*	To delete messages based on deleted_ind in request
	*	deleted_ind for INDIVIDUAL messages
	*	brdcst_read_msg_list for BROADCAST messages
 */
exports.deleteInboxData = function (req, res) {
	try {
		// following variables are the serach criteria for fetching user specific details for individual & Broadcast messages. 
		var mrktCd = req.params.mrktCd;
		var userId = req.params.acctNr;
		//TODO change it:var langCd = req.params.langCd;
		var langCd = req.params.langCd.toLowerCase();
		var msgToUpdate = req.body.msgList;
		//console.log('msgToUpdate:', msgToUpdate);
		whereCondition = {
			mrkt_cd: mrktCd,
			user_id: userId,
			lang_cd: langCd,
			ntfctn_typ: constants.AGNS_NTFCTN_TYP.INDV
		};
		if (req.params.appNm !== "mra") {
			whereCondition["app_nm"] = req.params.appNm
		}
		avonnotifications.findOne(whereCondition)
			.lean()
			.exec(function (err, dbResp) {
				if (!err && dbResp) {
					var broadcastReadList = dbResp.brdcst_read_msg_list || {};
					for (var msgToUpdtCnt = 0; msgToUpdtCnt < msgToUpdate.length; msgToUpdtCnt++) {
						var msg = msgToUpdate[msgToUpdtCnt];
						if (msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.BRODC) {
							if (broadcastReadList[msg.inbox_id]) {
								broadcastReadList[msg.inbox_id].deleted_ind = msg.deleted_ind;
							} else {
								var newBrdcastReadMsg = {
									'deleted_ind': msg.deleted_ind
								};
								broadcastReadList[msg.inbox_id] = newBrdcastReadMsg;
							}
						}
						else if (msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.INDV) {
							for (var dbMsgCnt = 0; dbMsgCnt < dbResp.messages.length; dbMsgCnt++) {
								if (msg.inbox_id == dbResp.messages[dbMsgCnt].inbox_id && msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.INDV) {
									dbResp.messages[dbMsgCnt].deleted_ind = msg.deleted_ind;
									break;
								}
							}
						}
					}
					dbResp.brdcst_read_msg_list = broadcastReadList;
					delete dbResp._id;
					avonnotifications.findOneAndUpdate(whereCondition,
						dbResp, { 'new': true }, function (err, updatedRow) {
							if (err) {
								expLayerSetError(res);
							}
							else {
								// Logic to send back only the Deleted Records
								let finalResult = [];
								for (var index = 0; index < msgToUpdate.length; index++) {
									var msg = msgToUpdate[index];
									if (msg.ntfctn_typ == constants.AGNS_NTFCTN_TYP.INDV) {
										for (var updatedRowIndex = 0; updatedRowIndex < updatedRow.messages.length; updatedRowIndex++) {
											if (msg.inbox_id == updatedRow.messages[updatedRowIndex].inbox_id) {
												finalResult.push(updatedRow.messages[updatedRowIndex]);
												break;
											}
										}
									}
								}
								if (req.params.appNm === "mra") {
									res.status(200).send(finalResult);
								}
								else {
									res.status(204).send();
								}
								res.end();
							}
						});
				} else {
					expLayerSetError(res);
				}
			});
	} catch (e) {
		/* v0.2 - Begin : Exception handling changes. */
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'avonNotificationCtrl-Delete - Exception handler.' });
		aiLogger.log(Config.logger.level, e.message);
		res.status(500).send({ status: false, message: 'Internal server error.' });
		res.end();
		/* v0.2 - End : Exception handling changes. */
	}
};
function expLayerSetError(res) {
	var resp = {
		msgList: []
	};
	res.status(200).send(resp);
	res.end();
}
