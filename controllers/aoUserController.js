'use strict'
/**
 * This class is the Controller class for AO App Login
 */

const cfg = require('../configs/config.global');
const env = cfg.env || 'dev', config = require('../configs/config.' + env);
// let config = require('../configs/config.global');

const helper = require("../helpers/agsHelper");
let logger = require('../util/logger');
const errorObjectHelper = require('../helpers/errorTemplate');
let validator = require('../util/validator');

// Define the Controller file and export the same
/**
 * @method for login 
 * @param {Http} req
 * @param {Http} res
 */


module.exports = {
    getPreferences: function (req, res) {
        // Request Inputs 
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let acctNr = req.params.acctNr;
        let applicationNm = req.params.applicationNm;
        let platfrmNm = req.params.platfrmNm;
        let devcId = req.params.devcId;
        let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsGetUsrPrfncConfigs = config.ao.ags.getUserPrfrnc;
        var deviceInfo = {
            devcId: devcId,
            pltfrmNm: platfrmNm,
            aplctnNm: agsGetUsrPrfncConfigs.aplctnNm
        };
        let payLoad = {};
        let version = agsGetUsrPrfncConfigs.versions.hasOwnProperty(mrktCd) ? agsGetUsrPrfncConfigs.versions[mrktCd] : agsGetUsrPrfncConfigs.versions["default"];
        payLoad.getUserPrfrncReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            acctNr: acctNr,
            token: token,
            deviceInfo: deviceInfo
        };
        let agsURL = agsGetUsrPrfncConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsGetUsrPrfncConfigs.classicBaseURLs[mrktCd] : agsGetUsrPrfncConfigs.classicBaseURLs["default"];
        let options = {
            method: agsGetUsrPrfncConfigs.method,
            url: agsURL + agsGetUsrPrfncConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO User Controller",
            respObjName: "getUserPrfrncResp",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.getUserPrfrncResp.hasOwnProperty("success") && !agsResponse.getUserPrfrncResp.success) {
                        logger.log(config.logger.level, 'AO User - Exception handler', {
                            type: 'AGS returned error response - Get User Preference',
                            env: config.env,
                            message: "Get User Preference Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.getUserPrfrncResp.code,
                                "message": agsResponse.getUserPrfrncResp.msg
                            }
                        });
                    }
                    else {
                        return res.send({
                            userPrfrncList: validator.checkNotNull(agsResponse.getUserPrfrncResp.userPrfrncList) ? agsResponse.getUserPrfrncResp.userPrfrncList : null
                        });
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO User - Exception handler', {
                    type: 'From Catch block of AO User Controller - Get User Preference',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    },
    mergePreferences: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.params.userId;
        let acctNr = req.params.acctNr;

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsMergeUsrPrfncConfigs = config.ao.ags.mergeUserPrfrnc;
        let updatePreferenceRequest = req.body.updatePreferenceRequest;
        let userPrfrncList = updatePreferenceRequest.userPrfrncList;
        let deviceInfo = updatePreferenceRequest;
        if (validator.checkNotNull(deviceInfo)) {
            deviceInfo.aplctnNm = agsMergeUsrPrfncConfigs.aplctnNm;
        }
        delete deviceInfo.userPrfrncList;
        let payLoad = {};
        let version = agsMergeUsrPrfncConfigs.versions.hasOwnProperty(mrktCd) ? agsMergeUsrPrfncConfigs.versions[mrktCd] : agsMergeUsrPrfncConfigs.versions["default"];
        payLoad.mergeUserPrfrncReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            userId: userId,
            acctNr: acctNr,
            deviceInfo: deviceInfo,
            userPrfrncList: userPrfrncList
        };
        let agsURL = agsMergeUsrPrfncConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsMergeUsrPrfncConfigs.classicBaseURLs[mrktCd] : agsMergeUsrPrfncConfigs.classicBaseURLs["default"];
        let options = {
            method: agsMergeUsrPrfncConfigs.method,
            url: agsURL + agsMergeUsrPrfncConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO User Controller",
            respObjName: "mergeUserPrfrncResp",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.mergeUserPrfrncResp.hasOwnProperty("success") && !agsResponse.mergeUserPrfrncResp.success) {
                        logger.log(config.logger.level, 'AO User - Exception handler', {
                            type: 'AGS returned error response - Merge User Preference',
                            env: config.env,
                            message: "Merge User Preference Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.mergeUserPrfrncResp.code,
                                "message": agsResponse.mergeUserPrfrncResp.msg
                            }
                        });
                    }
                    else {
                        res.status(204);
                        return res.send();
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO User - Exception handler', {
                    type: 'From Catch block of AO User Controller - Merge User Preference',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    },
    saveAgreements: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.params.userId;
        let acctNr = req.params.acctNr;
        let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];
        let agreementUpdate = [];
        agreementUpdate.push(req.body.agreementUpdate);
        // let refreshToken = req.headers["x-rfrsh-token"];

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsSaveAgrmtConfigs = config.ao.ags.saveAgreement;
        let payLoad = {};
        let version = agsSaveAgrmtConfigs.versions.hasOwnProperty(mrktCd) ? agsSaveAgrmtConfigs.versions[mrktCd] : agsSaveAgrmtConfigs.versions["default"];
        payLoad.saveAgrmntReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            langCd: langCd,
            userId: userId,
            acctNr: acctNr,
            token: token,
            userAgrmnts: agreementUpdate
        };
        let agsURL = agsSaveAgrmtConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsSaveAgrmtConfigs.classicBaseURLs[mrktCd] : agsSaveAgrmtConfigs.classicBaseURLs["default"];
        let options = {
            method: agsSaveAgrmtConfigs.method,
            url: agsURL + agsSaveAgrmtConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO User Controller",
            respObjName: "saveAgrmntResp",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.saveAgrmntResp.hasOwnProperty("success") && !agsResponse.saveAgrmntResp.success) {
                        logger.log(config.logger.level, 'AO User - Exception handler', {
                            type: 'AGS returned error response - Refresh Heart Beat',
                            env: config.env,
                            message: "Refresh Heart Beat Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.saveAgrmntResp.code,
                                "message": agsResponse.saveAgrmntResp.msg
                            }
                        });
                    }
                    else {
                        return agsResponse;
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .then(saveAgrmtsResult => {
                if (typeof saveAgrmtsResult != "undefined" && saveAgrmtsResult != null && saveAgrmtsResult.hasOwnProperty("saveAgrmntResp") && saveAgrmtsResult.saveAgrmntResp.success) {
                    let agsRepProfileConfigs = config.ao.ags.repProfile;
                    let payLoad = {};
                    let version = agsRepProfileConfigs.versions.hasOwnProperty(mrktCd) ? agsRepProfileConfigs.versions[mrktCd] : agsRepProfileConfigs.versions["default"];
                    payLoad.profile = {
                        devKey: agsConfigs.devKey,
                        version: version,
                        mrktCd: mrktCd,
                        langCd: langCd,
                        userId: userId,
                        acctNr: acctNr,
                        token: saveAgrmtsResult.saveAgrmntResp.token
                    };
                    let agsURL = agsRepProfileConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsRepProfileConfigs.classicBaseURLs[mrktCd] : agsRepProfileConfigs.classicBaseURLs["default"];
                    let options = {
                        method: agsRepProfileConfigs.method,
                        url: agsURL + agsRepProfileConfigs.apiEndPoint,
                        data: payLoad,
                        timeout: agsConfigs.timeOut
                    };
                    let inputs = {
                        controllerName: "AO User Controller",
                        respObjName: "profile",
                        serviceName: options.url
                    };
                    helper.invokeService(options, inputs)
                        .then(agsResponse => {
                            if (agsResponse.success) {
                                agsResponse = agsResponse.response;
                                if (typeof agsResponse != "undefined" && agsResponse.hasOwnProperty("profile") && !agsResponse.profile.success) {
                                    logger.log(config.logger.level, 'AO User - Exception handler', {
                                        type: 'AGS returned error response - Rep Profile',
                                        env: config.env,
                                        message: "Rep Profile Service failed",
                                        stack: agsResponse
                                    });
                                    res.status(401);
                                    return res.send({
                                        error: {
                                            "code": agsResponse.profile.code,
                                            "message": agsResponse.profile.msg
                                        }
                                    });
                                }
                                else if (typeof agsResponse != "undefined" && agsResponse.profile.hasOwnProperty("success") && agsResponse.profile.success) {
                                    res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, agsResponse.profile.token);
                                    let response = {};
                                    response.acctNr = acctNr;
                                    response.profile = {
                                        frstNm: validator.checkNotNull(agsResponse.profile.data.frstNm) ? agsResponse.profile.data.frstNm : null,
                                        lastNm: validator.checkNotNull(agsResponse.profile.data.lastNm) ? agsResponse.profile.data.lastNm : null,
                                        currSlsCmpgnNr: validator.checkNotNull(agsResponse.profile.data.currSlsCmpgnNr) ? agsResponse.profile.data.currSlsCmpgnNr : null,
                                        currSlsYrNr: validator.checkNotNull(agsResponse.profile.data.currSlsYrNr) ? agsResponse.profile.data.currSlsYrNr : null,
                                        campaignEndDt: validator.checkNotNull(agsResponse.profile.data.campaignEndDt) ? agsResponse.profile.data.campaignEndDt : null,
                                        campaignEndDsplyDt: agsResponse.profile.data.campaignEndDsplyDt
                                    };
                                    return res.send(response);
                                }
                                else {
                                    return res.send({
                                        error: {
                                            "code": agsConfigs.unknownErrorCode,
                                            "message": agsConfigs.unknownErrorMsg
                                        }
                                    });
                                }
                            }
                            else {
                                res.status(agsResponse.status);
                                return res.send({
                                    error: agsResponse.error
                                });
                            }
                        });
                }
                else if (typeof saveAgrmtsResult != "undefined" && saveAgrmtsResult.hasOwnProperty("success") && saveAgrmtsResult.success) {
                    return res.send({
                        error: saveAgrmtsResult.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO User - Exception handler', {
                    type: 'From Catch block of AO User Controller - Save Agreement',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    }
};