/**
 * @author Ashish Saurav
 */

//*************************Module Dependies********************//
// Import te Config settings
//const config = require('config');
const config = require('../configs/config.global');
// Import Helper Classes
const helper = require("../helpers/loginHelper");
const errorObjectHelper = require('../helpers/errorTemplate');
const dbInteractor = require('../helpers/databaseInteractor');
const mro_constant = require('../helpers/constants');
let profileFactory = require('./loginController');
let logger = require('../util/logger');
//SearchUser by Id for api
/**
 * @method for searching user
 * @param {HTTP Request} req
 * @param {HTTP Response} res
 * @param {cb to next middleware} next
 */
module.exports.tncUpdater = function(req, res) {
    // Case 1: Token Valid
    if (req.tokenPayload) {
        let option = {
            method: config.constant.PUT,
            url: config.constant.URL_BASE_PROFILE_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/termsAndConditions",
            token: req.tokenPayload.AGS_Token,
            data: {
                agrmntTyp: config.constant.TNCUPDATEKEY + req.body['acceptedT&CVersion'],
                agrmntVerNr: req.body['acceptedT&CVersion'],
                agrmntAcptdDt: new Date(),
                agrmntAcptdInd: true
            }
        };
        helper.agsDataFetcher(option, req).then(result => {
                return res.send({ success: true });
            }).catch(error => {
                logger.log(config.logger.level, 'tncUpdater - Exception handler.', {
                    type: 'tncUpdater-Catch.',
                    message: error.message,
                    status: error.status || mro_constant.httpErrCds.internalServerError
                });
                let errorData = errorObjectHelper(error);
                return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })
    }
    // Case 2: Token Expired
    else if (req.tokenExpired) {
        let error = new Error('Token Expired');
        error.status = mro_constant.middlwrCustErrCds.tokeExpired;
        logger.log(config.logger.level, 'tncUpdater - Exception handler.', {
            type: 'tncUpdater-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
    // Case 3: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'tncUpdater - Exception handler.', {
            type: 'tncUpdater-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
};

/**
 * @method for profiding profile data on refresh call.
 * @param {*} req 
 * @param {*} res 
 */
module.exports.profileFinder = function(req, res) {
    dbInteractor.getUserDetail(req.params.acctNr).then(function(userData) {
        let loginData = {};
        loginData.data = { acctNr: req.params.acctNr };
        loginData.token = userData.AGS_Token;
        return profileFactory.loginAggregator(req, loginData);
    }).then(function(loginRes) {
        return res.status(mro_constant.httpErrCds.ok).json({ data: loginRes });
    }).catch(err => {
        let errObj = errorObjectHelper(err);
        logger.log(config.logger.level, 'profileFinder - Exception handler.', {
            type: 'profileFinder-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        return res.status(err.httpCode || err.status).send(errObj.mwErrorArray);
    });
}