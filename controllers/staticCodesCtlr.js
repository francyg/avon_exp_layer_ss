'use strict'
/**
 * @author Mohit Bansal
 * @dated 6-June-2018
 * This class is the Controller class for Static Code data
 */

const helper = require("../helpers/loginHelper");
// Import Error Handler Template
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
const config = require('../configs/config.global');
let _ = require('lodash');
let redisClient = require('../server/client').client;
/**
 * @method for fetching list of possible statues
 * @param {Http} req
 */
let getleadownerprofiles = function(req,urls) {
    return new Promise((resolve, reject) => {
       let data = _.find(urls,{type: 'leadownerprofiles'});
        return resolve(helper.agsPublicDataFetcher(data.option, req));
    });
};

/**
 * @method for fetching list of ineligibity Reaons
 * @param {Http} req
 */
let getIneligibilityReasons = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'ineligibilityreasons'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

/**
 * @method for fetching list of LeadContactstatuses
 * @param {Http} req
 */
let getLeadContactstatuses = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'leadcontactstatuses'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

let getleadformtypes = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'leadformtypes'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
let getleadSources = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'leadSources'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
let getleadtypes = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'leadtypes'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
let getparameters = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'parameters'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
let getreassignreason = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'reassignreasons'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
let getsubstatuses = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'substatuses'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

let getstatuses = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'statuses'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

let getdelegationreasons = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'delegationreasons'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

let gettimetocontacts = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'timetocontacts'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};

let getzonemanageroverridereasons = function(req, urls) {
    return new Promise((resolve, reject) => {
        let data = _.find(urls,{type: 'zonemanageroverridereasons'});
         return resolve(helper.agsPublicDataFetcher(data.option, req));
     });
};
/**
 * @method for Aggregating all the satic codes into a single service
 * @param {Http} req
 */
let codesAggregator = (req, urls) => {
   return new Promise((resolve, reject) => {
        Promise.all([ getleadownerprofiles(req, urls), getIneligibilityReasons(req, urls), getleadformtypes(req, urls),
                    getleadSources(req, urls), getleadtypes(req, urls), getparameters(req, urls),
                    getreassignreason(req, urls), getsubstatuses(req, urls), getstatuses(req, urls),
                    getdelegationreasons(req, urls), gettimetocontacts(req, urls), getzonemanageroverridereasons(req, urls),
                    getLeadContactstatuses(req, urls) ])
        .then(result => {
            let dataHolder = {};
            _.each(result, function(value, index) {
                switch (index) {
                    case 0:
                        dataHolder.leadownerprofiles = value.data;
                        break;
                    case 1:
                        dataHolder.ineligibilityreasons = value.data;
                        break;
                    case 2:
                        dataHolder.leadformtypes = value.data;
                        break;
                    case 3:
                        dataHolder.leadSources = value.data;
                        break;
                    case 4:
                        dataHolder.leadtypes = value.data;
                        break;
                    case 5:
                        dataHolder.parameters = value.data;
                        break;
                    case 6:
                        dataHolder.reassignreason = value.data;
                        break;
                    case 7:
                        dataHolder.substatuses = value.data;
                        break;
                    case 8:
                        dataHolder.statuses = value.data;
                        break;
                    case 9:
                        dataHolder.delegationreasons = value.data;
                        break;
                    case 10:
                        dataHolder.timetocontacts = value.data;
                        break;
                    case 11:
                        dataHolder.zonemanageroverridereasons = value.data;
                        break;    
                    case 12:
                        dataHolder.LeadContactstatuses = value.data;
                        break;    
                    default:
                        let error = new Error('Unexpected Error');
                        error.status = mro_constant.middlwrCustErrCds.agsAPINotReacable;
                        logger.log(config.logger.level, 'CodesAggregator - Exception handler.', {
                            type: 'Codes Aggregator.',
                            env: config.env,
                            message: error.message,
                            status: error.status
                        });
                        error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                        return reject(error);
                        break;
                }
            });
            return resolve(dataHolder);
        })
        .catch(error => {
            logger.log(config.logger.level, 'CodesAggregator - Exception handler.', {
                type: 'CodesAggregator-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            return reject(error);
        });
   });
};
  

/**
 * @method for Controller Function for sending back the list of Static codes
 * @param {Http} req
 */
module.exports  = (req, res, next) => {
    
    let key = 'codes'+'_'+req.params.appNm+'_'+req.params.mrktCd+'_'+req.params.langCd+'_'+'_'+req.params.version
    checkKey(key)
    .then(function(result){
        if(result) {
            return redisDataFinder(key);
        } else {
            return agsUrlResolver(req, key);
        }
    }).then(function(data){
        return res.status(200).send(data);
    }).catch(error => {
        logger.log(config.logger.level, 'Get StaticCode - Exception handler - Catch.', {
                type: 'Get-StaticCode-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
        let errorData = errorObjectHelper(error);
        return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
    });
}

let checkKey = function(key) {
    return new Promise((resolve, reject) => {
        redisClient.exists(key, function(err, result){
            if(err) {
                return reject(err);
            } else {
                return resolve(result);
            }
        })
    });
}

let redisDataFinder = function (key) {
    return new Promise((resolve, reject) => {
        redisClient.get(key,function(err, dataset){
            if(err){
                return reject(err);
            } else {
                return resolve(JSON.parse(dataset));
            }
        });
    });
}

let agsUrlResolver = function(req, key) {
    return new Promise((resolve, reject) => {
    let dataSet;
       urlFinder(req)
       .then(function(urls) {
           return codesAggregator(req, urls);
       }).then(function(data) {
           dataSet = data;
           return dataCaching(key,data);
       }).then(function(cache) {
           return resolve(dataSet);
       }).catch(error => {
           return reject(error);
       });
    });
}

let urlFinder = function(req) {
    return new Promise((resolve, reject) => {
        let urls = [];
        _.each(config.URLCONST, function(val) {
            let option = {
                method: config.constant.GET,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                    "/" + req.params.langCd + "/leads/"+ val
            }
            urls.push({type:val,option:option});
        });
        return resolve(urls);
    });
}

let dataCaching = function(key, val) {
    return new Promise((resolve, reject) => {
        let value = JSON.stringify(val);
        redisClient.set(key, value, 'EX',config.constant.STATIC_DATA_EXPRY);
        return resolve('ok');
    });
}


