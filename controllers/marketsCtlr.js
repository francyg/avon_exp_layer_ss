/**
 * marketsCtlr
 *
 * @description :: Server-side logic for managing markets APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from marketsDao to marketsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var Markets = require('../models/markets');
var client = require('../server/client').client;
var Config = require('../configs/config.global');
var logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /markets
 * Returns market data.
 */
exports.getMarketList = function (req, res) {

    try {
        var apiVersion = parseInt(req.params.apiVrsn);
        var redisKey = 'markets_' + apiVersion;

        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, marketsData) {
                        if (err) {
                            resData = {status: false, code: 'SSMRKT001', message: 'Error while reading redis cache.'};
                            res.send(resData);
                        } else {
                            res.setHeader('Cache-Control', 'public, max-age=86400');
                            res.send(JSON.parse(marketsData));
                        }
                    });
                } else {
                    Markets.findOne({api_version: apiVersion}).lean().sort({_id: -1}).limit(1).exec(function (err, rows) {
                        if (!err) {
                            if (rows && Object.keys(rows).length != 0) {
                                client.set(redisKey, JSON.stringify(rows));
                            }
                            res.setHeader('Cache-Control', 'public, max-age=86400');
                            res.send(rows);
                        } else {
                            res.send({success: false, message: 'Error while fetching markets.'});
                            res.end();
                        }
                    });

                }
            }
        });

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Markets - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};
/**
 * GET /markets
 * Returns market data.
 */
exports.getAppMarketList = function (req, res) {

    try {
        var apiVersion = parseInt(req.params.apiVrsn);
        //var appNm = req.query.appNm||null;
        var appNm = null;
        // Support for new format where AppNm is URL Params
        if(req.params.appNm) {
            appNm = req.params.appNm;
        }
        // Support for the old API's where AppNm is in the Query Params
        else if(req.query.appNm) {
            appNm = req.query.appNm;
        }
        var redisKey = 'markets_' + apiVersion;
		if(appNm){
			redisKey = appNm+'_'+'markets_' + apiVersion;
		}
		logger.log(Config.logger.level, redisKey);
        logger.log(Config.logger.level, appNm);
        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, marketsData) {
                        if (err) {
                            resData = {status: false, code: 'SSMRKT001', message: 'Error while reading redis cache.'};
                            res.send(resData);
                        } else {
                            res.send(JSON.parse(marketsData));
                        }
                    });
                } else {
                    Markets.findOne({api_version: apiVersion,app_nm: appNm}).lean().sort({_id: -1}).limit(1).exec(function (err, rows) {
                        if (!err) {
                            if (rows && Object.keys(rows).length != 0) {
                                client.set(redisKey, JSON.stringify(rows));
                            }
                            res.send(rows);
                        } else {
                            res.send({success: false, message: 'Error while fetching markets.'});
                            res.end();
                        }
                    });

                }
            }
        });

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'App Markets - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};











