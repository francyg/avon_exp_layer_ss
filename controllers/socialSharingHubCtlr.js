/**
 * socialSharingHubCtlr
 *
 * @description :: Server-side logic for managing service version APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 * 		        0.3     -   App name included
 *                      -   Changed for MRA app's Help and Support feature
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var SSAppFeaturesAPIData = require('../models/SSAppFeaturesAPIData');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var JSONDiff = require('../util/jsondiff').JSONDiff;
var constants = require('../util/constants');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /socialsharinghub
 * Returns Social Sharing Hub details.
 */

exports.getSSH = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var downloadType = req.params.downloadType;
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';
        /* V0.3 changes begin */
        var appNm = null;
        // Support for new format where AppNm is URL Params
        if (req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if (req.query.appNm) {
            appNm = req.query.appNm
        }
        if (!downloadType) {
            downloadType = constants.URL.DOWNLOAD_TYPE_FULL;
        }
        /* V0.3 changes end */
        appNm = (typeof appNm != 'undefined' && appNm != null)? appNm.toString().toLowerCase(): '';
        var data = data || {};
        var condition = condition || {};
        condition.api_version = apiVersion;
        condition.api_type = 'social_sharing_hub';
        condition.lang_cd = langCd;
        condition.mrkt_cd = mrktCd;
        condition.app_nm = appNm;
        var redisKey = "";

        if (currDataVer && downloadType != constants.URL.DOWNLOAD_TYPE_FULL) {
            condition.data_version = parseInt(currDataVer);
            redisKey = appNm + '_ssh_' + mrktCd + '_' + langCd + '_' + apiVersion; + "_" + currDataVer; 
        }
        else {
            redisKey = appNm + '_ssh_' + mrktCd + '_' + langCd + '_' + apiVersion;
        }

        // V0.3 changes         
        if (downloadType == constants.URL.DOWNLOAD_TYPE_FULL) {            
            client.exists(redisKey, function (err, reply) {
                if (!err) {
                    if (reply === 1) {
                        client.get(redisKey, function (err, configsData) {
                            if (err) {
                                resData = { status: false, code: 'SSCONF001', message: 'Error while reading redis cache.' };
                                res.send(resData);
                            } else {
                                var redisConfigsData = JSON.parse(configsData);
                                data = redisConfigsData;
                                res.send(data);
                                res.end();
                            }
                        });
                    } else {
                        SSAppFeaturesAPIData.findOne(condition, { api_type: 0 }).lean().sort({ _id: -1 }).limit(1).exec(function (err, row) {
                            if (!err) {                                
                                if(typeof row != 'undefined' && row != null){                                        
                                    switch (appNm) {
                                        case 'ao':
                                            data = {"socialSharingHub":row.socialSharingHub};
                                            break;
                                        default:
                                            data = row;  
                                    }               
                                    client.set(redisKey, JSON.stringify(data));
                                }
                                res.send(data);
                                res.end();
                            } else {
                                res.send({ success: false, message: 'Error while fetching Social Sharing Hub.' });
                                res.end();
                            }
                        });
                    }
                }
            });
        } else if (downloadType == constants.URL.DOWNLOAD_TYPE_DELTA) {
            /* Delta logic */
            SSAppFeaturesAPIData.findOne(condition).lean().sort({ _id: -1 }).limit(1).exec(function (err, oldDoc) {
                delete condition.data_version;
                SSAppFeaturesAPIData.findOne(condition).lean().sort({ _id: -1 }).limit(1).exec(function (err, newDoc) {
                    var oldData = oldData || {};
                    var newData = newData || {};
                    oldData = oldDoc;
                    newData = newDoc;
                    if (typeof oldData != 'undefined' && oldData != null && oldData.hasOwnProperty('_id')) {
                        if ((oldData._id).toString() !== (newData._id).toString()) {
                            delete oldData._id;
                            var changedFaqArr = [];
                            (newData.socialSharingHub).forEach(function (newSSH) {
                                var hasMatch = JSONDiff.checkValueExists(oldData.socialSharingHub, 'ssh_content_id', newSSH.ssh_content_id);
                                if (hasMatch) {
                                    (oldData.socialSharingHub).forEach(function (oldSSH) {
                                        if (newSSH.ssh_content_id === oldSSH.ssh_content_id) {
                                            var delta = JSONDiff.delta(oldSSH, newSSH);
                                            if (delta !== false) {
                                                changedFaqArr.push(newSSH);
                                            }
                                        }
                                    });
                                } else {
                                    changedFaqArr.push(newSSH);
                                }
                            });

                            var faqRes = faqRes || {};
                            faqRes.socialSharingHub = changedFaqArr;

                            res.send(faqRes);
                            res.end();
                        } else {
                            res.send({});
                            res.end();
                        }
                    } else {
                        res.send({ message: 'Invalid data version.' });
                        res.end();
                    }
                });
            });
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'FAQ - Exception handler.' });
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({ status: false, message: 'Internal server error.' });
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};
