'use strict'
/**
 * This class is the Controller class for AO App Login
 */


const cfg = require('../configs/config.global');
const env = cfg.env || 'dev', config = require('../configs/config.' + env);
// let config = require('../configs/config.global');

const helper = require("../helpers/agsHelper");
let logger = require('../util/logger');
let validator = require('../util/validator');
const errorObjectHelper = require('../helpers/errorTemplate');
// Define the Controller file and export the same
/**
 * @method for login 
 * @param {Http} req
 * @param {Http} res
 */
module.exports = {
    login: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.body.loginRequest.userId;
        let password = req.body.loginRequest.password;
        let generateRefreshToken = req.body.loginRequest.generateRefreshToken;

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsLoginConfigs = config.ao.ags.login;
        let payLoad = {};
        let version = agsLoginConfigs.versions.hasOwnProperty(mrktCd) ? agsLoginConfigs.versions[mrktCd] : agsLoginConfigs.versions["default"];
        payLoad.login = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            langCd: langCd,
            userId: userId,
            password: password
        };
        let agsURL = agsLoginConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsLoginConfigs.classicBaseURLs[mrktCd] : agsLoginConfigs.classicBaseURLs["default"];
        let options = {
            method: agsLoginConfigs.method,
            url: agsURL + agsLoginConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO Login Controller",
            respObjName: "auth",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (agsResponse.hasOwnProperty("auth") && agsResponse.auth.hasOwnProperty("userAgrmnts") && agsResponse.auth.userAgrmnts.length == 0) {
                        if (generateRefreshToken) {
                            res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, agsResponse.profile.token);
                            res.setHeader(cfg.constant.HEADER_X_RFRSH_TOKEN, agsResponse.auth.refreshToken);
                        }
                        else {
                            res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, agsResponse.profile.token);
                        }
                        let response = {};
                        response.acctNr = validator.checkNotNull(agsResponse.auth.acctNr) ? agsResponse.auth.acctNr : null;
                        response.bskInd = validator.checkNotNull(agsResponse.auth.bskInd) ? agsResponse.auth.bskInd : null;
                        response.userAgrmnts = validator.checkNotNull(agsResponse.auth.userAgrmnts) ? agsResponse.auth.userAgrmnts : null;
                        return res.send(response);
                    }
                    else {
                        return agsResponse;
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .then(loginResult => {
                if (validator.checkNotNull(loginResult) && loginResult.hasOwnProperty("auth") && loginResult.auth.success) {
                    let agsRepProfileConfigs = config.ao.ags.repProfile;
                    let payLoad = {};
                    let version = agsRepProfileConfigs.versions.hasOwnProperty(mrktCd) ? agsRepProfileConfigs.versions[mrktCd] : agsRepProfileConfigs.versions["default"];
                    payLoad.profile = {
                        devKey: agsConfigs.devKey,
                        version: version,
                        mrktCd: mrktCd,
                        langCd: langCd,
                        userId: userId,
                        acctNr: loginResult.auth.acctNr,
                        token: loginResult.auth.token
                    };
                    let agsURL = agsRepProfileConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsRepProfileConfigs.classicBaseURLs[mrktCd] : agsRepProfileConfigs.classicBaseURLs["default"];
                    let options = {
                        method: agsRepProfileConfigs.method,
                        url: agsURL + agsRepProfileConfigs.apiEndPoint,
                        data: payLoad,
                        timeout: agsConfigs.timeOut
                    };
                    inputs = {
                        controllerName: "AO Login Controller",
                        respObjName: "profile",
                        serviceName: options.url
                    };
                    helper.invokeService(options, inputs)
                        .then(agsResponse => {
                            if (agsResponse.success) {
                                agsResponse = agsResponse.response;
                                if (typeof agsResponse != "undefined" && agsResponse.hasOwnProperty("profile") && !agsResponse.profile.success) {
                                    logger.log(config.logger.level, 'AO Login - Exception handler', {
                                        type: 'AGS returned error response - Rep Profile',
                                        env: config.env,
                                        message: "Rep Profile Service failed",
                                        stack: agsResponse
                                    });
                                    res.status(401);
                                    return res.send({
                                        error: {
                                            "code": agsResponse.profile.code,
                                            "message": agsResponse.profile.msg
                                        }
                                    });
                                }
                                else if (typeof agsResponse != "undefined" && agsResponse.hasOwnProperty("profile") && agsResponse.profile.success) {
                                    if (generateRefreshToken) {
                                        res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, agsResponse.profile.token);
                                        res.setHeader(cfg.constant.HEADER_X_RFRSH_TOKEN, loginResult.auth.refreshToken);
                                    }
                                    else {
                                        res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, agsResponse.profile.token);
                                    }
                                    let response = {};
                                    //response.success = true;
                                    response.acctNr = validator.checkNotNull(loginResult.auth.acctNr) ? loginResult.auth.acctNr : null;
                                    response.bskInd = validator.checkNotNull(loginResult.auth.bskInd) ? loginResult.auth.bskInd : null;
                                    response.userAgrmnts = validator.checkNotNull(loginResult.auth.userAgrmnts) ? loginResult.auth.userAgrmnts : null;
                                    response.profile = {
                                        frstNm: validator.checkNotNull(agsResponse.profile.data.frstNm) ? agsResponse.profile.data.frstNm : null,
                                        lastNm: validator.checkNotNull(agsResponse.profile.data.lastNm) ? agsResponse.profile.data.lastNm : null,
                                        currSlsCmpgnNr: validator.checkNotNull(agsResponse.profile.data.currSlsCmpgnNr) ? agsResponse.profile.data.currSlsCmpgnNr : null,
                                        currSlsYrNr: validator.checkNotNull(agsResponse.profile.data.currSlsYrNr) ? agsResponse.profile.data.currSlsYrNr : null,
                                        campaignEndDt: validator.checkNotNull(agsResponse.profile.data.campaignEndDt) ? agsResponse.profile.data.campaignEndDt : null,
                                        campaignEndDsplyDt: agsResponse.profile.data.campaignEndDsplyDt
                                    };
                                    return res.send(response);
                                }
                                else {
                                    return res.send({
                                        error: {
                                            "code": agsConfigs.unknownErrorCode,
                                            "message": agsConfigs.unknownErrorMsg
                                        }
                                    });
                                }
                            }
                            else {
                                res.status(agsResponse.status);
                                return res.send({
                                    error: agsResponse.error
                                });
                            }
                        });
                }
                else if (typeof loginResult != "undefined" && loginResult.hasOwnProperty("success") && loginResult.success) {
                    return res.send({
                        error: loginResult.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO Login - Exception handler', {
                    type: 'From Catch block of AO Login Controller - login',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    },
    logout: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.params.userId;
        let acctNr = req.params.acctNr;
        let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsMergeUsrPrfncConfigs = config.ao.ags.mergeUserPrfrnc;
        let logoutRequest = req.body.logoutRequest;
        let userPrfrncList = logoutRequest.userPrfrncList;
        let deviceInfo = logoutRequest;
        if (validator.checkNotNull(deviceInfo)) {
            deviceInfo.aplctnNm = agsMergeUsrPrfncConfigs.aplctnNm;
        }
        delete deviceInfo.userPrfrncList;
        let payLoad = {};
        let version = agsMergeUsrPrfncConfigs.versions.hasOwnProperty(mrktCd) ? agsMergeUsrPrfncConfigs.versions[mrktCd] : agsMergeUsrPrfncConfigs.versions["default"];
        payLoad.mergeUserPrfrncReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            userId: userId,
            acctNr: acctNr,
            token: token,
            deviceInfo: deviceInfo,
            userPrfrncList: userPrfrncList
        };
        let agsURL = agsMergeUsrPrfncConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsMergeUsrPrfncConfigs.classicBaseURLs[mrktCd] : agsMergeUsrPrfncConfigs.classicBaseURLs["default"];
        let options = {
            method: agsMergeUsrPrfncConfigs.method,
            url: agsURL + agsMergeUsrPrfncConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO Login Controller",
            respObjName: "mergeUserPrfrncResp",
            serviceName: options.url
        };
        helper.invokeService(options, req)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.mergeUserPrfrncResp.hasOwnProperty("success") && !agsResponse.mergeUserPrfrncResp.success) {
                        logger.log(config.logger.level, 'AO Login - Exception handler', {
                            type: 'AGS returned error response - Logout',
                            env: config.env,
                            message: "Logout Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.mergeUserPrfrncResp.code,
                                "message": agsResponse.mergeUserPrfrncResp.msg
                            }
                        });
                    }
                    else {
                        res.status(204);
                        return res.send();
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO Login - Exception handler', {
                    type: 'From Catch block of AO Login Controller - Logout',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    },
    refreshAccessToken: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.params.userId;
        let acctNr = req.params.acctNr;
        let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];
        // let refreshToken = req.headers["x-rfrsh-token"];

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsRefreshConfigs = config.ao.ags.renewAccessToken;
        let payLoad = {};
        let version = agsRefreshConfigs.versions.hasOwnProperty(mrktCd) ? agsRefreshConfigs.versions[mrktCd] : agsRefreshConfigs.versions["default"];
        payLoad.heartBeatReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            userId: userId,
            acctNr: acctNr,
            token: token
        };
        let agsURL = agsRefreshConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsRefreshConfigs.classicBaseURLs[mrktCd] : agsRefreshConfigs.classicBaseURLs["default"];
        let options = {
            method: agsRefreshConfigs.method,
            url: agsURL + agsRefreshConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO Login Controller",
            respObjName: "heartBeatResp",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.heartBeatResp.hasOwnProperty("success") && !agsResponse.heartBeatResp.success) {
                        logger.log(config.logger.level, 'AO Login - Exception handler', {
                            type: 'AGS returned error response - Refresh Heart Beat',
                            env: config.env,
                            message: "Refresh Heart Beat Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.heartBeatResp.code,
                                "message": agsResponse.heartBeatResp.msg
                            }
                        });
                    }
                    else {
                        res.status(204);
                        res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, validator.checkNotNull(agsResponse.heartBeatResp.token) ? agsResponse.heartBeatResp.token : null);
                        return res.send();
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO Login - Exception handler', {
                    type: 'From Catch block of AO Login Controller - Refresh Heart Beat',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    },
    renewAccessToken: function (req, res) {
        // Request Inputs
        let mrktCd = req.params.mrktCd;
        let langCd = req.params.langCd;
        let userId = req.params.userId;
        let token = req.headers[cfg.constant.HEADER_X_ACCESS_TOKEN];
        let refreshToken = req.headers["x-rfrsh-token"];

        // Fetch AGS Response
        let agsConfigs = config.ao.ags;
        let agsRefreshConfigs = config.ao.ags.refreshAccessToken;
        let payLoad = {};
        let version = agsRefreshConfigs.versions.hasOwnProperty(mrktCd) ? agsRefreshConfigs.versions[mrktCd] : agsRefreshConfigs.versions["default"];
        payLoad.refreshTokenReq = {
            devKey: agsConfigs.devKey,
            version: version,
            mrktCd: mrktCd,
            userId: userId,
            token: token,
            refreshToken: refreshToken
        };
        let agsURL = agsRefreshConfigs.classicBaseURLs.hasOwnProperty(mrktCd) ? agsRefreshConfigs.classicBaseURLs[mrktCd] : agsRefreshConfigs.classicBaseURLs["default"];
        let options = {
            method: agsRefreshConfigs.method,
            url: agsURL + agsRefreshConfigs.apiEndPoint,
            data: payLoad,
            timeout: agsConfigs.timeOut
        };
        let inputs = {
            controllerName: "AO Login Controller",
            respObjName: "refreshTokenResp",
            serviceName: options.url
        };
        helper.invokeService(options, inputs)
            .then(agsResponse => {
                if (agsResponse.success) {
                    agsResponse = agsResponse.response;
                    if (typeof agsResponse != "undefined" && agsResponse.refreshTokenResp.hasOwnProperty("success") && !agsResponse.refreshTokenResp.success) {
                        logger.log(config.logger.level, 'AO Login - Exception handler', {
                            type: 'AGS returned error response - Refresh Heart Beat',
                            env: config.env,
                            message: "Refresh Heart Beat Service failed",
                            stack: agsResponse
                        });
                        res.status(401);
                        return res.send({
                            error: {
                                "code": agsResponse.refreshTokenResp.code,
                                "message": agsResponse.refreshTokenResp.msg
                            }
                        });
                    }
                    else {
                        res.status(204);
                        res.setHeader(cfg.constant.HEADER_X_ACCESS_TOKEN, validator.checkNotNull(agsResponse.refreshTokenResp.token) ? agsResponse.refreshTokenResp.token : null);
                        res.setHeader(cfg.constant.HEADER_X_RFRSH_TOKEN, validator.checkNotNull(agsResponse.refreshTokenResp.refreshToken) ? agsResponse.refreshTokenResp.refreshToken : null);
                        return res.send();
                    }
                }
                else {
                    res.status(agsResponse.status);
                    return res.send({
                        error: agsResponse.error
                    });
                }
            })
            .catch(error => {
                logger.log(config.logger.level, 'AO Login - Exception handler', {
                    type: 'From Catch block of AO Login Controller - Refresh Heart Beat',
                    env: config.env,
                    message: error.message,
                    statusText: error.statusText,
                    status: error.status,
                    stack: error.stack
                });
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send({
                    error: {
                        "code": agsConfigs.unknownErrorCode,
                        "message": agsConfigs.unknownErrorMsg
                    }
                });
            });
    }
};