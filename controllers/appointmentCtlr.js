/**
 * @author Mohit Bansal
 */

const config = require('../configs/config.global');
const helper = require("../helpers/loginHelper");
const loggerHelper = require("../helpers/loggerHelper");
const genralHelper = require("../helpers/genralHelper");
const errorObjectHelper = require('../helpers/errorTemplate');
const dbInteractor = require('../helpers/databaseInteractor');
const mro_constant = require('../helpers/constants');
const duphelper = require('../helpers/duplicityHeler');
let profileFactory = require('./loginController');
let logger = require('../util/logger');
let _ = require('lodash');

/**
 * Function to create a new Avon FGL Lead
 * @param {*} option 
 * @param {*} req 
 */
let createLead = (option, req) => {
    return new Promise((resolve, reject) => {
        if (req && req.body && req.body.leadId && req.body.leadId != null) {
            return resolve({ leadId: req.body.leadId })
        }
        if (option) {
            //make a service call to AGS layer
            helper.agsDataFetcher(option, req).then(result => {
                //update the lead status to 'contacted'
                return updateLeadStatus(req, result);
            }).then(result => {
                //return the result
                return resolve(result.data)
            }).catch(error => {
                //log the error
                logger.log(config.logger.level, 'Create Lead - Exception handler - Catch.', {
                    type: 'Create Lead-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status || mro_constant.httpErrCds.internalServerError
                });
                let errorData = errorObjectHelper(error);
                //return error object
                if (errorData.mwErrorArray) {
                    return reject({ status: errorData.status, data: errorData.mwErrorArray })
                } else {
                    return reject(errorData)
                }

            });
        } else {
            //improper request object
            //log the error message
            let error = new Error('Invalid Request');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Create Lead - Exception handler.', {
                type: 'Create new Lead-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            //return err object
            return reject(errorData);
        }
    })
}

/**
 * This API is used to Cancel/Reject Appointment
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

let stopAppointment = (req, res, next) => {
    // Case 1: Token Valid
    if (req.tokenPayload) {
        //configure request object
        let option = {
            method: config.constant.DLETE,
            url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/lead/" + req.params.leadId,
            token: req.tokenPayload.AGS_Token
        };
        //make a call to AGS service
        helper.agsDataFetcher(option, req).then(result => {
            return res.status(200).send({ status: 'success' });
        }).catch(error => {
            //log the error
            logger.log(config.logger.level, 'Stop Appointment - Exception handler - Catch.', {
                type: 'Stop Appointment-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            //return error object
            return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        })
    }
    // Case 2: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Stop Appointment - Exception handler.', {
            type: 'Stop Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        //return error object
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
};
/**
 * @method for creating and update appointment.
 * @param {http} req 
 * @param {http} res 
 * @param {callback for next middleware} next `
 */
let createAppointment = (req, res, next) => {
    // Case 1: Token Valid
    if (req.tokenPayload) {
        next();
    }
    // Case 2: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
            type: 'Update Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        //return error object
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
};


/**
 * @method for creating and update appointment.
 * @param {http} req 
 * @param {http} res 
 * @param {callback for next middleware} next `
 */
let createAppointmentv2 = (req, res, next) => {
    // Case 1: Token Valid
    if (req.tokenPayload) {
        // Check for valid body
        if (!req.body) {
            let error = new Error('Body not found');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
        }
        if (!req.body.leadType) {
            let error = new Error('leadType is missing');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
        }
        let leadType = req.body.leadType.toLowerCase();
        delete req.body.leadType;

        switch (leadType) {
            case "agl":
            //fallthrough
            case "fgl":
                break;
            default:
                let error = new Error('Invalid leadType');
                error.status = mro_constant.middlwrCustErrCds.badRequest;
                let errorData = errorObjectHelper(error);
                return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);

        }

        try {
            if ((req.body.pids && req.body.pids[0] && req.body.pids[0].idntfctnTxt) && !req.body.brthDt) {
                let cnp;
                let decodedCNP;
                let dob;
                cnp = req.body.pids[0].idntfctnTxt;
                decodedCNP = genralHelper.decodeCNP(cnp);
                dob = decodedCNP.dateOfBirth;
                req.body.brthDt = dob;
            }
        } catch (er) {
            //log the error
            logger.log(config.logger.level, 'Submit Appointment - Exception handler.', {
                type: 'Submit Appointment-Catch.',
                env: config.env,
                message: mro_constant.httpErrCds.internalServerError,
                status: 500
            });
        }

        /**
         * In Step 1, call AGS create new Lead API and set its status to contacted
         * In Step 1 if LeadKey is already there, then simply update the Lead
         * From Step 2 onwards, always call Save AGS Appointment API
         */
        //  Lead Already Exists - Create new appointment or Update Existing Lead
        if (req && req.body && req.body.leadId && req.body.leadId != null) {
            if (leadType === "agl") {
                if (req.body.leadKey) {
                    delete req.body.leadKey;
                }
                let option = {
                    method: req.method,
                    url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd,
                    token: req.tokenPayload.AGS_Token,
                    data: req.body
                };
                // Call the AGS Save Appointment Service
                helper.agsDataFetcher(option, req)
                    .then(result => {
                        // Return the Appointment details
                        return res.status(200).send(result.data);
                    })
                    .catch(error => {
                        //log the error
                        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
                            type: 'Update Appointment-Catch.',
                            env: config.env,
                            message: error.message,
                            status: error.status
                        });
                        let errorData = errorObjectHelper(error);
                        //return the error
                        return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                    });
            } else {
                //we hav a FGL
                // User is in step 2 or subsequent steps and updating PID
                if (req && req.body && req.body.pids && req.body.pids != null) {
                    // Create options object for AGS Save Appointment API
                    let option = {
                        method: req.method,
                        url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd,
                        token: req.tokenPayload.AGS_Token,
                        data: req.body
                    };
                    // Call the AGS Save Appointment Service
                    helper.agsDataFetcher(option, req)
                        .then(result => {
                            // Return the Appointment details
                            return res.status(200).send(result.data);
                        })
                        .catch(error => {
                            //log the error
                            logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
                                type: 'Update Appointment-Catch.',
                                env: config.env,
                                message: error.message,
                                status: error.status
                            });
                            let errorData = errorObjectHelper(error);
                            //return the error
                            return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                        })
                } else {
                    // User is in Step 1 and only updating the lead
                    /* 
                     *  Lead Key is missing, hence throw error
                     */
                    if (!req.body.leadKey) {
                        let error = new Error('Bad Request- Lead Key Missing');
                        error.status = mro_constant.middlwrCustErrCds.leadKeyMissing;
                        let errorData = errorObjectHelper(error);
                        return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
                    }
                    // Update the existing Lead, since user is in Step 1
                    // Create new Lead Object
                    let updateLeadoption = {
                        method: config.constant.PUT,
                        url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                            "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead/" + req.body.leadKey,
                        token: req.tokenPayload.AGS_Token,
                        data: {
                            titlCd: req.body.titlCd,
                            frstNm: req.body.frstNm,
                            lastNm: req.body.lastNm,
                            emailAddrTxt: req.body.emailAddrTxt,
                            mobilePhoneNr: req.body.mobilePhoneNr,
                            prfrdLangCd: req.params.langCd.toLowerCase() + "_" + req.params.mrktCd,
                            leadSrceCd: req.body.apptSrceCd || null,
                            leadNtesTxt: req.body.leadNtesTxt || null
                        }
                    }
                    // Call the UpdateExistingLead Service to update the Lead details via LeadKey
                    helper.agsDataFetcher(updateLeadoption, req)
                        .then(result => {
                            // Return the Appointment details
                            return res.status(200).send({ success: true });
                        })
                        .catch(error => {
                            let errorData = errorObjectHelper(error);
                            return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                        });
                }
            }

        }
        // Step 1: We are in Step 1 or subsequent steps with no prior lead, hence create a new lead
        else {
            // Create new Lead Object
            let createLeadoption = {
                method: config.constant.POST,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                    "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead",
                token: req.tokenPayload.AGS_Token,
                data: {
                    titlCd: req.body.titlCd,
                    frstNm: req.body.frstNm,
                    lastNm: req.body.lastNm,
                    emailAddrTxt: req.body.emailAddrTxt,
                    mobilePhoneNr: req.body.mobilePhoneNr,
                    cnsntRecvdInd: config.constant.CNSNTRECVDIND, //true, //req.body.cnsntRecvdInd,
                    prfrdLangCd: req.params.langCd.toLowerCase() + "_" + req.params.mrktCd,
                    leadSrceCd: req.body.apptSrceCd || null,
                    leadNtesTxt: req.body.leadNtesTxt || null,
                    "formId": "PLESS_APPT"
                }
            }
            // Call AGS Create Lead Service and return the Lead ID & Lead Key
            createLead(createLeadoption, req)
                .then(result => {
                    // Return the Lead Details
                    // Check for PID, if yes, then create appointment 
                    // User is not in Sceen 1, hence create an Appointment
                    if (req && req.body && req.body.pids && req.body.pids != null) {
                        // Copy the generated Lead ID from create Lead service into create appointment service
                        let payload = req.body;
                        if (result.leadId) {
                            payload.leadId = result.leadId;
                        }
                        // Create options object for AGS Save Appointment API
                        let option = {
                            method: req.method,
                            url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd,
                            token: req.tokenPayload.AGS_Token,
                            data: payload
                        };
                        // Call the AGS Save Appointment Service
                        helper.agsDataFetcher(option, req)
                            .then(result => {
                                // Return the Appointment details
                                return res.status(200).send(result.data);
                            })
                            .catch(error => {
                                let errorData = errorObjectHelper(error);
                                return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
                            })
                    }
                    // User is still there on Screen 1, hence return LeadID and LeadKey data as it is
                    else {
                        // Return the LeadID and LeadKey data back to UI.
                        //save appointment :step 0 
                        return res.status(201).send(result);
                    }
                })
                .catch(error => {
                    //log the error
                    logger.log(config.logger.level, 'Create Lead - Exception handler.', {
                        type: 'Create Lead -Catch.',
                        env: config.env,
                        message: error.message,
                        status: error.status
                    });
                    let errorData = errorObjectHelper(error);
                    //return error object
                    return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                });
        }
    }
    // Case 2: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        //log the error
        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
            type: 'Update Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        //return error object
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
};


/**
 * Function to get Avon FGL appointment details
 * @param {*} leadId 
 * @param {*} req 
 */
let getAppointment = (req, res, next) => {
    //check if the user is logged in 
    if (req.tokenPayload) {
        //frame request object
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/lead/" + req.params.leadId,
            token: req.tokenPayload.AGS_Token
        };
        //make a call to AGS service
        helper.agsDataFetcher(option, req).then(result => {
            //return appointment details
            return res.status(200).send(result.data);
        }).catch(error => {
            //log the error object
            logger.log(config.logger.level, 'Get Appointment - Exception handler - Catch.', {
                type: 'Get Appointment-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            //return err object
            return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        })
    }
    // Case 2: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        //log err object
        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
            type: 'Update Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        //return err object
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
};

/**
 * Function to update lead status
 * @param {*} leadKey
 * @param {*} acctNr
 * @param {*} req 
 */
let updateLeadStatus = (req, result) => {
    return new Promise((resolve, reject) => {
        if (req.tokenPayload) {
            let option = {
                method: config.constant.PUT,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                    "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead/" + result.data.leadKey +
                    '/status/contacted',
                token: req.tokenPayload.AGS_Token,
                data: {
                    substatus: "CT_SD",
                    comment: null
                }
            }
            helper.agsDataFetcher(option, req).then(resp => {
                return resolve(result);
            }).catch(error => {
                logger.log(config.logger.level, 'update Lead - Exception handler - Catch.', {
                    type: 'Create Lead-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status || mro_constant.httpErrCds.internalServerError
                });
                let errorData = errorObjectHelper(error);
                return reject({ status: 500, data: errorData.mwErrorArray });
            });
        } else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Create Lead - Exception handler.', {
                type: 'Create new Lead-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            return reject(errorData.mwErrorArray);
        }
    });

}
/**
 * function to create new lead
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
let createNewLead = (req, res, next) => {
    /*return new Promise((resolve, reject) => {
        return resolve(res.status(200).json({ msg: 'ok' }));
    });*/
    if (req.tokenPayload) {
        let option = {
            method: config.constant.POST,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead",
            token: req.tokenPayload.AGS_Token,
            data: {
                frstNm: req.body.frstNm,
                lastNm: req.body.lastNm,
                emailAddrTxt: req.body.emailAddrTxt,
                mobilePhoneNr: req.body.mobilePhoneNr,
                cnsntRecvdInd: req.body.cnsntRecvdInd,
                prfrdLangCd: req.params.langCd.toLowerCase() + "_" + req.params.mrktCd,
                "formId": "PLESS_APPT"
            }
        }
        helper.agsDataFetcher(option, req).then(result => {
            return updateLeadStatus(req, result);
        }).then(result => {
            return res.status(200).send(result.data);
        }).catch(error => {
            logger.log(config.logger.level, 'Create Lead - Exception handler - Catch.', {
                type: 'Create Lead-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        });
    } // Case 2: Token Invalid
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Create Lead - Exception handler.', {
            type: 'Create new Lead-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
}



//
/**
 * Lookup version 2 service controller
 * @param {*} cnpNum
 * @param {*} apptId
 * @param {*} req 
 */
let lookup = (req, res, next) => {
    let resultData = {};
    let getTitleDataOption = {
        method: config.constant.GET,
        url: config.constant.URL_BASE_CATALOGUE + "/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + '/codes/DEFAULT_TAB/TITL_CD'
    }
    let getAddressDataOption = {
        method: config.constant.GET,
        url: config.constant.URL_BASE_CATALOGUE + "/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + '/codes/DEFAULT_TAB/STR_CD_TXT'
    }

    // Create Promise.all to fire the AGS services
    Promise.all([helper.agsPublicDataFetcher(getTitleDataOption, req), helper.agsPublicDataFetcher(getAddressDataOption, req)])
        .then(result => {
            let dataHolder = {};
            _.each(result, function (value, index) {
                switch (index) {
                    case 0:
                        dataHolder.titleData = value.data;
                        break;
                    case 1:
                        dataHolder.addressData = value.data;
                        break;
                    default:
                        break;
                }
            });
            res.setHeader('Cache-Control', 'public, max-age=86400');
            return res.status(200).send(dataHolder);
        })
        .catch(error => {
            logger.log(config.logger.level, 'Lookup V2 - Exception handler - Catch.', {
                type: 'Lookup V2-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        })
}

let frameURL = function (type, val, req, apptId) {
    return {
        method: config.constant.GET,
        url: config.constant.URL_BASE_APPT_SERVICE + "/" + req.params.mrktCd +
            "/" + req.params.langCd + "/duplicityCheck?scope=ALL&type=" + type + "&valToBeVerfd=" + val + ((apptId && apptId != null) ? "&apptId=" + apptId : ""),
        token: req.tokenPayload.AGS_Token
    }
}
/**
 * Function to verify email and mobile number
 * @param {*} cnpNum
 * @param {*} apptId
 * @param {*} req 
 */
let dupCheck = (req, res) => {
    //console.log(req.params);
    //console.log(req.body);
    if (req.tokenPayload) {
        let apptId = req.body.apptId ? req.body.apptId : null;
        if (req.body && req.body.hasOwnProperty("email") && req.body.hasOwnProperty("mobile")) {
            duphelper.dupEmail(frameURL("EMAIL", req.body.email, req, apptId), req).then(function (em_result) {
                return duphelper.dupMobile(frameURL("MOBILE", req.body.mobile, req, apptId), req, em_result);
            }).then(function (result) {
                return res.status(200).send(result);
            }).catch(err => {
                //console.log(err)
                logger.log(config.logger.level, 'Duplicity Check - Exception handler.', {
                    type: 'Duplicity Check -Catch.',
                    env: config.env,
                    message: err.message,
                    status: err.status
                });
                let errorData = errorObjectHelper(err);
                return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })

        } else {
            let error = new Error('Bad Request');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            logger.log(config.logger.level, 'Duplicity Check - Exception handler.', {
                type: 'Duplicity Check -Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
        }
    } else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
            type: 'Update Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
}
/**
 * Function to verify cnp number
 * @param {*} cnpNum
 * @param {*} apptId
 * @param {*} req 
 */
let cnpDupCheck = (req, res) => {
    if (req.tokenPayload) {
        let apptId = req.params.apptId ? req.params.apptId : null;
        duphelper.dupCNP(frameURL("PID", req.params.cnpNum, req, apptId), req).then(function (result) {
            return res.status(200).send(result);
        }).catch(err => {
            //console.log(err)
            logger.log(config.logger.level, 'Duplicity CNP Check - Exception handler.', {
                type: 'Duplicity CNP Check -Catch.',
                env: config.env,
                message: err.message,
                status: err.status
            });
            let errorData = errorObjectHelper(err);
            return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        })
    } else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Update Appointment - Exception handler.', {
            type: 'Update Appointment-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
}
/**
 * Function to notify appoint agreement
 * @param {*} leadId
 * @param {*} acctNr
 * @param {*} req 
 */
let notify = (req, res) => {
    if (req.tokenPayload) {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_APPT_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/lead/" + req.params.leadId + "/agreement/notify",
            token: req.tokenPayload.AGS_Token
        }
        helper.agsDataFetcher(option, req).then(result => {
            return res.status(200).send({ status: 'success' });
        }).catch(error => {
            //log the error
            logger.log(config.logger.level, 'notify agreement - Exception handler - Catch.', {
                type: 'notify agreement-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            if (error && error.data && error.data.length > 0) {
                if (error.data[0] && error.data[0]["errCd"] === "MAX_NTFCTN_REACHED") {
                    error.data[0]["errCd"] = mro_constant.middlwrCustErrCds.resendSMSCountExceeded;
                }

            }
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);

        });
    } else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'notify - Exception handler.', {
            type: 'notify-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
}
module.exports = {
    createAppointment,
    createAppointmentv2,
    getAppointment,
    stopAppointment,
    createNewLead,
    updateLeadStatus,
    lookup,
    dupCheck,
    cnpDupCheck,
    notify
};