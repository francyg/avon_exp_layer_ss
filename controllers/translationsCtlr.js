/**
 * translationsCtlr
 *
 * @description :: Server-side logic for managing translation APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from translationsDao to translationsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var Translations = require('../models/translations');
var JSONDiff = require('../util/jsondiff').JSONDiff;
var constants = require('../util/constants');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /translations
 * Returns translation data.
 */
exports.getMarketTranslations = function (req, res) {
    try {
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var mrktConfigType = req.params.mrktConfigType;
        var downloadType = req.params.downloadType;
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';
        var condition = condition || {};
        condition.api_version = apiVersion;
        condition.mrkt_cd = mrktCd;
        condition.lang_cd = langCd;
        if (currDataVer && downloadType != constants.URL.DOWNLOAD_TYPE_FULL) {
            condition.data_version = parseInt(currDataVer);
        }

        var data = data || {};
        if (downloadType == constants.URL.DOWNLOAD_TYPE_FULL) {

            var redisKey = 'translations_' + apiVersion + '_' + mrktCd + '_' + langCd;
            client.exists(redisKey, function (err, reply) {
                if (!err) {
                    if (reply === 1) {
                        client.get(redisKey, function (err, translationsData) {
                            if (err) {
                                resData = {status: false, code: 'SSTRAN001', message: 'Error while reading redis cache.'};
                                res.send(resData);
                            } else {
                                //res.send(JSON.parse(translationsData));
                                var redisTranslationsData = JSON.parse(translationsData);
                                if (typeof redisTranslationsData != 'undefined' && redisTranslationsData != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = redisTranslationsData._id;
                                        data.lookup_id = redisTranslationsData.lookup_id;
                                        data.api_version = redisTranslationsData.api_version;
                                        data.data_version = redisTranslationsData.data_version;
                                        data.mrkt_cd = redisTranslationsData.mrkt_cd;
                                        data.lang_cd = redisTranslationsData.lang_cd;
                                        data.translations = {};
                                        data.translations[mrktConfigType] = redisTranslationsData.translations[mrktConfigType];
                                    } else {
                                        data = redisTranslationsData;
                                    }
                                }
                                res.setHeader('Cache-Control', 'public, max-age=86400');
                                res.send(data);
                                res.end();
                            }
                        });
                    } else {
                        Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                            if (!err) {
                                if (typeof row != 'undefined' && row != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = row._id;
                                        data.lookup_id = row.lookup_id;
                                        data.api_version = row.api_version;
                                        data.data_version = row.data_version;
                                        data.mrkt_cd = row.mrkt_cd;
                                        data.lang_cd = row.lang_cd;
                                        data.translations = {};
                                        data.translations[mrktConfigType] = row.translations[mrktConfigType];
                                    } else {
                                        if (row && Object.keys(row).length != 0) {
                                            client.set(redisKey, JSON.stringify(row));
                                        }
                                        data = row;
                                    }
                                }
                                res.setHeader('Cache-Control', 'public, max-age=86400');
                                res.send(data);
                                res.end();
                            } else {
                                res.send({success: false, message: 'Error while fetching market translations.'});
                                res.end();
                            }
                        });


                    }
                }
            });

        } else if (downloadType == constants.URL.DOWNLOAD_TYPE_DELTA) {

            /* Delta logic */
            Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, oldDoc) {

                delete condition.data_version;
                Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, newDoc) {

                    var oldData = oldData || {};
                    var newData = newData || {};
                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                        oldData._id = oldDoc._id;
                        oldData.translations = {};
                        oldData.translations[mrktConfigType] = oldDoc.translations[mrktConfigType];
                        newData._id = newDoc._id;
                        newData.translations = {};
                        newData.translations[mrktConfigType] = newDoc.translations[mrktConfigType];
                    } else {
                        oldData = oldDoc;
                        newData = newDoc;
                    }

                    if ((oldData._id).toString() !== (newData._id).toString()) {
                        delete oldData._id;
                        if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                            delete newData._id;
                        }
                        var delta = JSONDiff.delta(oldData, newData);
                        delta = (delta == false) ? {} : delta;
                        res.send(delta);
                        res.end();
                    } else {
                        res.send({});
                        res.end();
                    }
                });
            });
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Translations - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};
/**
 * GET /translations
 * Returns translation data.
 */
exports.getAppMarketTranslations = function (req, res) {
    try {
        var mrktCd = req.params.mrktCd;
        //Initialize the App Name
        var appNm = null;
        // Support for new format where AppNm is URL Params
        if(req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if(req.query.appNm) {
            appNm = req.query.appNm
        }
        var langCd = req.params.langCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var mrktConfigType = req.params.mrktConfigType;
        var downloadType = req.params.downloadType;
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';
        var condition = condition || {};
        condition.api_version = apiVersion;
		condition.app_nm = appNm;
        condition.mrkt_cd = mrktCd;
        condition.lang_cd = langCd;
        if (currDataVer && downloadType != constants.URL.DOWNLOAD_TYPE_FULL) {
            condition.data_version = parseInt(currDataVer);
        }

        var data = data || {};
        if (downloadType == constants.URL.DOWNLOAD_TYPE_FULL) {

            var redisKey = 'translations_' + apiVersion + '_' + mrktCd + '_' + langCd;
			if(appNm){
				redisKey = appNm+'_'+'translations_' + apiVersion + '_' + mrktCd + '_' + langCd;
			}
            client.exists(redisKey, function (err, reply) {
                if (!err) {
                    if (reply === 1) {
                        client.get(redisKey, function (err, translationsData) {
                            if (err) {
                                resData = {status: false, code: 'SSTRAN001', message: 'Error while reading redis cache.'};
                                res.send(resData);
                            } else {
                                //res.send(JSON.parse(translationsData));
                                var redisTranslationsData = JSON.parse(translationsData);
                                if (typeof redisTranslationsData != 'undefined' && redisTranslationsData != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = redisTranslationsData._id;
                                        data.lookup_id = redisTranslationsData.lookup_id;
                                        data.api_version = redisTranslationsData.api_version;
                                        data.data_version = redisTranslationsData.data_version;
                                        data.mrkt_cd = redisTranslationsData.mrkt_cd;
                                        data.lang_cd = redisTranslationsData.lang_cd;
                                        data.translations = {};
                                        data.translations[mrktConfigType] = redisTranslationsData.translations[mrktConfigType];
                                    } else {
                                        data = redisTranslationsData;
                                    }
                                }
                                res.send(data);
                                res.end();
                            }
                        });
                    } else {
                        Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                            if (!err) {
                                if (typeof row != 'undefined' && row != null) {
                                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                                        data._id = row._id;
                                        data.lookup_id = row.lookup_id;
                                        data.api_version = row.api_version;
                                        data.data_version = row.data_version;
                                        data.mrkt_cd = row.mrkt_cd;
                                        data.lang_cd = row.lang_cd;
                                        data.translations = {};
                                        data.translations[mrktConfigType] = row.translations[mrktConfigType];
                                    } else {
                                        if (row && Object.keys(row).length != 0) {
                                            client.set(redisKey, JSON.stringify(row));
                                        }
                                        data = row;
                                    }
                                }
                                res.send(data);
                                res.end();
                            } else {
                                res.send({success: false, message: 'Error while fetching market translations.'});
                                res.end();
                            }
                        });


                    }
                }
            });

        } else if (downloadType == constants.URL.DOWNLOAD_TYPE_DELTA) {

            /* Delta logic */
            Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, oldDoc) {

                delete condition.data_version;
                Translations.findOne(condition).lean().sort({_id: -1}).limit(1).exec(function (err, newDoc) {

                    var oldData = oldData || {};
                    var newData = newData || {};
                    if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                        oldData._id = oldDoc._id;
                        oldData.translations = {};
                        oldData.translations[mrktConfigType] = oldDoc.translations[mrktConfigType];
                        newData._id = newDoc._id;
                        newData.translations = {};
                        newData.translations[mrktConfigType] = newDoc.translations[mrktConfigType];
                    } else {
                        oldData = oldDoc;
                        newData = newDoc;
                    }

                    if ((oldData._id).toString() !== (newData._id).toString()) {
                        delete oldData._id;
                        if (mrktConfigType != constants.URL.MRKT_CONFIG_TYPE_ALL) {
                            delete newData._id;
                        }
                        var delta = JSONDiff.delta(oldData, newData);
                        delta = (delta == false) ? {} : delta;
                        res.send(delta);
                        res.end();
                    } else {
                        res.send({});
                        res.end();
                    }
                });
            });
        }

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Translations - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};











