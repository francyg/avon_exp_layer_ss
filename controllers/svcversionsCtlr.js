/**
 * svcversionsCtlr
 *
 * @description :: Server-side logic for managing service version APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var Svcversions = require('../models/svcversions');
var Config = require('../configs/config.global');
var client = require('../server/client').client;
var logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');

var resData = function () {
};

/**
 * GET /svcversions
 * Returns service versions.
 */
exports.getServiceVersions = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var data = data || {};
        var redisKey = 'svcversions_' + apiVersion + '_' + mrktCd;

        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, svcversionsData) {
                        if (err) {
                            resData = {status: false, code: 'SSSRVC001', message: 'Error while reading redis cache.'};
                            res.send(resData);
                        } else {
                            if (typeof svcversionsData != 'undefined' && svcversionsData != null) {
                                data = JSON.parse(svcversionsData);
                            }
                            res.setHeader('Cache-Control', 'public, max-age=86400');
                            res.send(data);
                            res.end();
                        }
                    });
                } else {
                    Svcversions.findOne({api_version: apiVersion, mrkt_cd: mrktCd}).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                        if (!err) {
                            if (typeof row != 'undefined' && row != null) {
                                if (row && Object.keys(row).length != 0) {
                                    client.set(redisKey, JSON.stringify(row));
                                }
                                data = row;
                            }
                            res.setHeader('Cache-Control', 'public, max-age=86400');
                            res.send(data);
                            res.end();
                        } else {
                            res.send({success: false, message: 'Error while fetching svcversions.'});
                            res.end();
                        }
                    });

                }
            } else {
                res.send(data);
                res.end();
            }
        });

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Service version - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};


/**
 * GET /svcversions
 * Returns service versions.
 */
exports.getAppServiceVersions = function (req, res) {

    try {
        var mrktCd = req.params.mrktCd;
        //var appNm = req.query.appNm||null;
        var appNm = null;
        // Support for new format where AppNm is URL Params
        if(req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if(req.query.appNm) {
            appNm = req.query.appNm
        }
        var apiVersion = parseInt(req.params.apiVrsn);
        var data = data || {};
        var redisKey = 'svcversions_' + apiVersion + '_' + mrktCd;
		if(appNm){
			redisKey = appNm+'_'+'svcversions_' + apiVersion + '_' + mrktCd;
		}

        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, svcversionsData) {
                        if (err) {
                            resData = {status: false, code: 'SSSRVC001', message: 'Error while reading redis cache.'};
                            res.send(resData);
                        } else {
                            if (typeof svcversionsData != 'undefined' && svcversionsData != null) {
                                data = JSON.parse(svcversionsData);
                            }
                            res.send(data);
                            res.end();
                        }
                    });
                } else {
                    Svcversions.findOne({api_version: apiVersion, mrkt_cd: mrktCd,app_nm:appNm}).lean().sort({_id: -1}).limit(1).exec(function (err, row) {
                        if (!err) {
                            if (typeof row != 'undefined' && row != null) {
                                if (row && Object.keys(row).length != 0) {
                                    client.set(redisKey, JSON.stringify(row));
                                }
                                data = row;
                            }
                            res.send(data);
                            res.end();
                        } else {
                            res.send({success: false, message: 'Error while fetching svcversions.'});
                            res.end();
                        }
                    });

                }
            } else {
                res.send(data);
                res.end();
            }
        });

    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, {url: fullUrl, type: 'Service version - Exception handler.'});
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({status: false, message: 'Internal server error.'});
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};










