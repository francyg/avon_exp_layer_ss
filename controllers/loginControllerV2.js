'use strict'
/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This class is the Controller class for Login Routes
 */

const helper = require("../helpers/loginHelper");
const encrypter = require("../helpers/genralHelper");
//const config = require('config');
let config = require('../configs/config.global');
let _ = require('lodash');
const user = require("../models/user");
const jwt = require('jsonwebtoken');
// Import Error Handler Template
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
// Nodejs encryption with CTR
let crypto = require('crypto'),
    algorithm = 'aes-256-ctr';
let logger = require('../util/logger');
const dbhelper = require("../helpers/databaseInteractor");
const deviceRegPrefTemplate = require('../helpers/deviceRegPrefHelper');
const getUserPrefTemplate = require('../helpers/getDevicePrefTemplate');

// Define the Controller file and export the same
/**
 * @method for login 
 * @param {Http} req
 * @param {Http} res
 */
/**
 * @swagger
 * resourcePath: /apiJs
 * description: All about API
 */

/**
 * @swagger
 * path: /login
 * operations:
 *   -  httpMethod: POST
 *      summary: Login with username and password
 *      notes: Returns a user based on username
 *      responseClass: User
 *      nickname: login
 *      consumes: 
 *        - text/html
 *      parameters:
 *        - name: username
 *          description: Your username
 *          paramType: query
 *          required: true
 *          dataType: string
 *        - name: password
 *          description: Your password
 *          paramType: query
 *          required: true
 *          dataType: string
 */
let loginservice = (req, res) => {
        // Call the Axios Library to login and Get the AGS Token
        // Added the Flag to get the Refresh Token Data
        let option = {
            method: config.constant.PUT,
            url: config.constant.URL_BASE_LOGIN + "/" + req.params.mrktCd + "/" + req.params.langCd + "/logn",
            data: { userId: req.body.userId, password: req.body.password, generateRefreshToken: true }
        };
        if (!(req.body.deviceDetails &&
                req.body.deviceDetails.pltfrmNm &&
                req.body.deviceDetails.devcId &&
                req.body.deviceDetails.pltfrmVerTxt &&
                req.body.deviceDetails.devcMakeTxt &&
                req.body.deviceDetails.devcToknTxt &&
                req.body.deviceDetails.devcMdlTxt &&
                req.body.deviceDetails.aplctnVerTxt &&
                req.body.deviceDetails.devcLcleCd &&
                req.body.deviceDetails.aplctnNm)) {
            let error = new Error('Missing Params');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
        }
        if (req.body.userPrfrncList && Array.isArray(req.body.userPrfrncList) && req.body.userPrfrncList.length === 0) {
            let error = new Error('Bad Request');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
        }
        let login_respose, aggregateVal;
        helper.agsDataFetcher(option, req).then(loginRes => {
            // Setting the Login Response
            login_respose = loginRes;
            // Setting the Account Number for the Logged in User
            req.acctNr = loginRes.data.acctNr;
            return loginAggregator(req, loginRes, res);
        }).then(aggrResponse => {
            aggregateVal = aggrResponse;
            // logic for aggregating data to send in response.
            // Send the AGS Refresh Token also
            return agsTokenManager(req, login_respose, aggrResponse);
        }).then(dbOutput => {
            //logic for genrating token
            return middleWareTokenManager(req.headers['uniqueid'], login_respose.data.acctNr);
        }).then(tokenObj => {
            // send response back
            // Set the Session Token
            res.setHeader('x-access-token', tokenObj.sessionToken);
            res.setHeader('x-access-expiry', config.constant.TOKENLIFE)
            // Set the Refresh Token
            res.setHeader('x-refresh-token', tokenObj.refreshToken);
            res.setHeader('x-refresh-expiry', config.constant.REFRESHTOKENLIFE);
            res.status(200);
            return res.json({ data: aggregateVal });
        }).catch(error => {
            logger.log(config.logger.level, 'LoginStack - Exception handler.', {
                type: 'From Catch block of login Login Stack.',
                env: config.env,
                message: error.message,
                statusText: error.statusText,
                status: error.status,
                stack: error.stack
            });

            let errObj = errorObjectHelper(error);
            return res.status(error.httpCode || error.status).send(errObj.mwErrorArray);
        });

    }
    /**
     * @swagger
     * models:
     *   User:
     *     id: User
     *     properties:
     *       username:
     *         type: String
     *       password:
     *         type: String    
     */


    let loginv3service = (req, res) => {
        // Call the Axios Library to login and Get the AGS Token
        // Added the Flag to get the Refresh Token Data
        let option = {
            method: config.constant.PUT,
            url: config.constant.URL_BASE_LOGIN + "/" + req.params.mrktCd + "/" + req.params.langCd + "/logn",
            data: { userId: req.body.userId, password: req.body.password, generateRefreshToken: true }
        };
        let login_respose, aggregateVal;
        helper.agsDataFetcher(option, req).then(loginRes => {
            // Setting the Login Response
            login_respose = loginRes;
            // Setting the Account Number for the Logged in User
            req.acctNr = loginRes.data.acctNr;
            return loginV3Aggregator(req, loginRes, res);
        }).then(aggrResponse => {
            aggregateVal = aggrResponse;
            // logic for aggregating data to send in response.
            // Send the AGS Refresh Token also
            return agsTokenManager(req, login_respose, aggrResponse);
        }).then(dbOutput => {
            //logic for genrating token
            return middleWareTokenManager(req.headers['uniqueid'], login_respose.data.acctNr);
        }).then(tokenObj => {
            // send response back
            // Set the Session Token
            res.setHeader('x-access-token', tokenObj.sessionToken);
            // Set the Refresh Token
            res.setHeader('x-refresh-token', tokenObj.refreshToken);
            res.setHeader('x-access-expiry', config.constant.TOKENLIFE)
            res.setHeader('x-refresh-expiry', config.constant.REFRESHTOKENLIFE);
            res.status(200);
            return res.json({ data: aggregateVal });
        }).catch(error => {
            logger.log(config.logger.level, 'LoginStack - Exception handler.', {
                type: 'From Catch block of login Login Stack.',
                env: config.env,
                message: error.message,
                statusText: error.statusText,
                status: error.status,
                stack: error.stack
            });

            let errObj = errorObjectHelper(error);
            return res.status(error.httpCode || error.status).send(errObj.mwErrorArray);
        });

    }

/**
 * @method for aggregating details to provide at login
 * @param {Http} req
 * @param {Object} loginRes
 */
let loginV3Aggregator = function(req, loginRes, res) {
    return new Promise((resolve, reject) => {
        Promise.all([
                getBasicProfile(req, loginRes), 
                getCurrentCampaign(req, loginRes), 
                getLeadEligibility(req, loginRes),
                getLeadCount(req, loginRes)
            ])
            .then(result => {
                let dataHolder = {};
                _.each(result, function(value, index) {
                    switch (index) {
                        case 0:
                            // Check if User is Sales Leader or Zone Manager
                            // check for zm
                            if (value.data.acctTyp.toLowerCase() === mro_constant.constantData.SALES_LEAD ||
                                value.data.acctTyp.toLowerCase() === mro_constant.constantData.ZONE_MANAGER) {
                                dataHolder.acctNr = value.data.acctNr;
                                dataHolder.userId = value.data.userId;
                                dataHolder.acctTyp = value.data.acctTyp;
                                dataHolder.frstNm = value.data.frstNm;
                                dataHolder.lastNm = value.data.lastNm;
                                dataHolder.acctStus = value.data.acctStus;
                                dataHolder.tnc = getTNCdetail(loginRes);
                                break;
                            } else {
                                let error = new Error('Unauthorized account type');
                                error.status = mro_constant.middlwrCustErrCds.repLoginError;
                                error.httpCode = mro_constant.httpErrCds.forbiddenAccess;
                                return reject(error);
                                break;
                            }
                        case 1:
                            dataHolder.currCampaignDtls = _.first(value.data);
                            break;
                        case 2:
                            dataHolder.outOfOffcFromDt = value.data.outOfOffcFromDt;
                            dataHolder.outOfOffcToDt = value.data.outOfOffcToDt;
                            dataHolder.optInInd = value.data.optInInd;
                            break;
                        case 3:
                            // Get the Contacted Leads Count
                            dataHolder.contactedLeadsCount = value.data.cntCT;
                            // Get In progress Leads
                            dataHolder.inProgressLeadsCount = value.data.cntIP;
                            // Get New Leads
                            dataHolder.newLeadsCount = value.data.cntNW;
                             // Get the All Leads
                            dataHolder.allleadscount = value.data.cnt;
                            break;
                        default:
                            let error = new Error('Unexpected Error');
                            error.status = mro_constant.middlwrCustErrCds.agsAPINotReacable;
                            logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                                type: 'login.',
                                env: config.env,
                                message: error.message,
                                status: error.status
                            });
                            error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                            return reject(error);
                            break;
                    }
                });
                if( dataHolder.acctTyp.toLowerCase() === mro_constant.constantData.ZONE_MANAGER ) {
                    dataHolder.optInInd = true;
                }
                return resolve(dataHolder);
            }).catch(error => {
                logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                    type: 'login-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                return reject(error);
            });
    });
}     


/**
 * @method for fetching Contacted lead count
 * @param {Http} req
 * @param {Object} loginRes
 */
let getLeadCount = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/lead/count?status=IP,NW,CT",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for aggregating details to provide at login
 * @param {Http} req
 * @param {Object} loginRes
 */
let loginAggregator = function(req, loginRes, res) {
    return new Promise((resolve, reject) => {
        Promise.all([
                getBasicProfile(req, loginRes), getCurrentCampaign(req, loginRes), getLeadEligibility(req, loginRes),
                getContactedLeadCount(req, loginRes), getIPLeadCount(req, loginRes), getNewLeadCount(req, loginRes),
                registerDeviceNSetuserPrefs(req, loginRes.data.acctNr, res)
            ])
            .then(result => {
                let dataHolder = {};
                _.each(result, function(value, index) {
                    switch (index) {
                        case 0:
                            // Check if User is Sales Leader or Zone Manager
                            // check for zm
                            if (value.data.acctTyp.toLowerCase() === mro_constant.constantData.SALES_LEAD ||
                                value.data.acctTyp.toLowerCase() === mro_constant.constantData.ZONE_MANAGER) {
                                dataHolder.acctNr = value.data.acctNr;
                                dataHolder.userId = value.data.userId;
                                dataHolder.acctTyp = value.data.acctTyp;
                                dataHolder.frstNm = value.data.frstNm;
                                dataHolder.lastNm = value.data.lastNm;
                                dataHolder.acctStus = value.data.acctStus;
                                dataHolder.tnc = getTNCdetail(loginRes);
                                break;
                            } else {
                                let error = new Error('Unauthorized account type');
                                error.status = mro_constant.middlwrCustErrCds.repLoginError;
                                logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                                    type: 'login.',
                                    env: config.env,
                                    message: error.message,
                                    status: error.status
                                });
                                error.httpCode = mro_constant.httpErrCds.forbiddenAccess;
                                return reject(error);
                                break;
                            }
                        case 1:
                            dataHolder.currCampaignDtls = _.first(value.data);
                            break;
                        case 2:
                            dataHolder.outOfOffcFromDt = value.data.outOfOffcFromDt;
                            dataHolder.outOfOffcToDt = value.data.outOfOffcToDt;
                            dataHolder.optInInd = value.data.optInInd;
                            break;
                        case 3:
                            // Get the Contacted Leads Count
                            dataHolder.contactedLeadsCount = value.data.cnt;
                            break;
                        case 4:
                            // Get the In Progress Leads Count
                            dataHolder.inProgressLeadsCount = value.data.cnt;
                            break;
                        case 5:
                            // Get the New Leads Count
                            dataHolder.newLeadsCount = value.data.cnt;
                            break;
                        case 6:
                            dataHolder.userPrfrncList = res.userPrfrncList;
                            break;
                        default:
                            let error = new Error('Unexpected Error');
                            error.status = mro_constant.middlwrCustErrCds.agsAPINotReacable;
                            logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                                type: 'login.',
                                env: config.env,
                                message: error.message,
                                status: error.status
                            });
                            error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                            return reject(error);
                            break;
                    }
                });
                if( dataHolder.acctTyp.toLowerCase() === mro_constant.constantData.ZONE_MANAGER ) {
                    dataHolder.optInInd = true;
                }
                // Count the total Leads ( In Progress + New + Contacted )
                dataHolder.allleadscount = dataHolder.contactedLeadsCount + dataHolder.inProgressLeadsCount + dataHolder.newLeadsCount;
                return resolve(dataHolder);
            }).catch(error => {
                logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                    type: 'login-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                return reject(error);
            });
    });
}

/**
 * @method for fetching profile data.
 * @param {Http} req
 * @param {Object} loginRes
 */
let getBasicProfile = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_PROFILE_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/basicProfile",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for fetching current camping detail
 * @param {Http} req
 * @param {Object} loginRes
 */
let getCurrentCampaign = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_REPORTING_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/reporting/reportCampaigns",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for fetching Contacted lead count
 * @param {Http} req
 * @param {Object} loginRes
 */
let getContactedLeadCount = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/lead/count?status=CT",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for fetching In Progress Lead Count
 * @param {Http} req
 * @param {Object} loginRes
 */
let getIPLeadCount = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/lead/count?status=IP",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for fetching New Leads Count
 * @param {Http} req
 * @param {Object} loginRes
 */
let getNewLeadCount = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/lead/count?status=NW",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * API to Edit User Preferences
*/

let editUserPreferences = (req, res) => {
    if (!(req.body.deviceDetails &&
            req.body.deviceDetails.pltfrmNm &&
            req.body.deviceDetails.devcId &&
            req.body.deviceDetails.pltfrmVerTxt &&
            req.body.deviceDetails.devcMakeTxt &&
            req.body.deviceDetails.devcToknTxt &&
            req.body.deviceDetails.devcMdlTxt &&
            req.body.deviceDetails.aplctnVerTxt &&
            req.body.deviceDetails.devcLcleCd &&
            req.body.deviceDetails.aplctnNm)) {
        let error = new Error('Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        let errorFormater = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
    }

    if (!(req.body.userPrfrncList && Array.isArray(req.body.userPrfrncList) && req.body.userPrfrncList.length !== 0)) {
        let error = new Error('Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        let errorFormater = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
    }


    registerDeviceNSetuserPrefs(req, req.params.acctNr, res)
        .then(logoutResult => {
            res.status(200);
            return res.send(res.userPrfrncList);
        })
        .catch(error => {
            let errObj = errorObjectHelper(error);
            return res.status(error.httpCode || error.status).send(errObj.mwErrorArray);
        })
}

/**
 * API for logging the User Out
 */

let logoutUser = (req, res) => {
        if (!(req.body.deviceDetails &&
                req.body.deviceDetails.pltfrmNm &&
                req.body.deviceDetails.devcId &&
                req.body.deviceDetails.pltfrmVerTxt &&
                req.body.deviceDetails.devcMakeTxt &&
                req.body.deviceDetails.devcMdlTxt &&
                req.body.deviceDetails.aplctnVerTxt &&
                req.body.deviceDetails.devcLcleCd &&
                req.body.deviceDetails.aplctnNm)) {
            let error = new Error('Bad Request');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
        }
        if (!(req.body.userPrfrncList && Array.isArray(req.body.userPrfrncList) && req.body.userPrfrncList.length !== 0)) {
            let error = new Error('Bad Request');
            error.status = mro_constant.middlwrCustErrCds.badRequest;
            let errorFormater = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.badRequest).send(errorFormater.mwErrorArray);
        }
        req.body.deviceDetails.devcToknTxt = "";
        registerDeviceNSetuserPrefs(req, req.params.acctNr, res)
            .then(logoutResult => {
                res.status(200);
                return res.json({ loggedOut: "true" });
            })
            .catch(error => {
                let errObj = errorObjectHelper(error);
                return res.status(error.httpCode || error.status).send(errObj.mwErrorArray);
            })
    }
    /**
     * Function to set the Push Notifications User preferences on Login
     * @param {*} req 
     * @param {*} loginRes 
     */
let registerDeviceNSetuserPrefs = function(req, acctNr, res= null) {
    return new Promise((resolve, reject) => { 
        //Dafault Preferences for Login, All Yes
        acctNr = acctNr.toString();
        let defaultUserPreferences = [];
        // Variable to check if this is part of Login Request or not
        let isLoginRequest = false;
        if (req.query.isLogin == "true") {
            isLoginRequest = true;
        }
        // Check if Req Body contains Preferences
        if (req.body.userPrfrncList && Array.isArray(req.body.userPrfrncList) && req.body.userPrfrncList.length !== 0) {
            // Reset the Preferences Array and populate the same from Login Request
            defaultUserPreferences = [];
            req.body.userPrfrncList.forEach(function(item) {
                if (item.optInInd && (item.optInInd === "Y" || item.optInInd === "N")) {
                    defaultUserPreferences.push({
                        "prfrncCd": item.prfrncCd,
                        "optInInd": item.optInInd
                    })
                }
            });
        }
        let mergeOptions = {
            method: config.constant.POST,
            url: config.constant.URL_BASE_PUSH_NOTIFICATION + "/merge",
            data: deviceRegPrefTemplate(req, acctNr, defaultUserPreferences)
        }
        // Assign the User Prefernces in the API Response
        res.userPrfrncList = defaultUserPreferences;
        // If this is a Login based Request
        if (isLoginRequest) {
            let getPrefOptions = {
                method: config.constant.POST,
                url: config.constant.URL_BASE_PUSH_NOTIFICATION + "/get",
                data: getUserPrefTemplate(req, acctNr)
            }
            // Get the saved Preferences from AGS
            helper.agsPublicDataFetcher(getPrefOptions, req)
                .then(response => {
                    // If Preferences Received from AGS, populate the same
                    if(response.data.getUserPrfrncResp.userPrfrncList) {
                        defaultUserPreferences = response.data.getUserPrfrncResp.userPrfrncList;
                        res.userPrfrncList = response.data.getUserPrfrncResp.userPrfrncList;
                        mergeOptions.data = deviceRegPrefTemplate(req, acctNr, defaultUserPreferences);
                        return resolve(helper.agsPublicDataFetcher(mergeOptions, req));
                    }
                    else {
                        return resolve(helper.agsPublicDataFetcher(mergeOptions, req));
                    }
                })
                .catch(error => {
                    let Error = error;
                    return resolve(helper.agsPublicDataFetcher(mergeOptions, req));
                })
        }
        else {
            return resolve(helper.agsPublicDataFetcher(mergeOptions, req));
        }
    });
}

/**
 * @method for calling helper to get lead elgibilty.
 * @param {Http} req
 * @param {Object} loginRes
 */
let getLeadEligibility = function(req, loginRes) {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + loginRes.data.acctNr + "/lead/eligibility",
            token: loginRes.token
        }
        return resolve(helper.agsDataFetcher(option, req));
    });
}

/**
 * @method for managing access token to cosmodb.
 * @param {Http} req
 * @param {Object} loginRes
 * @param {Object} dataEntity
 */
let agsTokenManager = function(req, loginRes, dataEntity) {
    return new Promise((resolve, reject) => {
        user.findOneAndUpdate({ acctNr: dataEntity.acctNr }, {
                acctNr: dataEntity.acctNr,
                userId: dataEntity.userId,
                mrktCd: req.params.mrktCd,
                lngCd: req.params.langCd,
                acctType: dataEntity.acctTyp,
                AGS_Token: loginRes.token,
                AGS_Refresh_Token: loginRes.refreshToken
            }, { upsert: true },
            function(err, userData) {
                if (err) {
                    logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
                        type: 'login-agsTokenManager-momgo error.',
                        env: config.env,
                        message: err,
                    });
                    return reject(err);
                } else {
                    // Need to return original data
                    return resolve(userData);
                }
            }
        );
    });
}

/**
 * @method for genrating token at middleware.
 * @param {String} deviceId
 * @param {Object} data
 */
let middleWareTokenManager = function(deviceId, acctnr) {
    return new Promise((resolve, reject) => {
        const data = {
                acctNr: encrypter.encrypt(acctnr.toString()),
                uniqueid: encrypter.encrypt(deviceId)
            }
            // Create the Session Token
        const token = jwt.sign(data, config.constant.SECRET, { expiresIn: config.constant.TOKENLIFE })
            // Create the Refresh Token
        const refreshToken = jwt.sign(data, config.constant.REFRESHTOKENSECRET, { expiresIn: config.constant.REFRESHTOKENLIFE })
            // Create the Token Object
        const tokenObj = {
                sessionToken: token,
                refreshToken: refreshToken
            }
            // Return the Response
        return resolve(tokenObj);
    });
}

/**
 * @method for fetching the lst tnc
 * @param {Object} loginRes 
 */
let getTNCdetail = function(loginRes) {
    let data = {
        version: null,
        accepted: false,
        registerationDate: null
    }
    try {
        if (loginRes.data.agrmnts.length > 0) {
            let mraTnCAccptdArry = loginRes.data.agrmnts.filter(
                tncRecrd => {
                return tncRecrd.agrmntTyp.indexOf(config.constant.TNCUPDATEKEY) !== -1
            })
            mraTnCAccptdArry = mraTnCAccptdArry.sort(function (a, b) {
                if(a.agrmntVerNr > b.agrmntVerNr) {
                    return 1;
                }
                else if(a.agrmntVerNr < b.agrmntVerNr) {
                    return -1;
                }
                return 0;
            })
            if(mraTnCAccptdArry.length > 0) {
                let obj = _.last(mraTnCAccptdArry);
                data.version = obj.agrmntVerNr;
                data.accepted = obj.agrmntAcptdInd;
                data.registerationDate = obj.agrmntAcptdDt;
            }
        } 
        return data;
    } catch (error) {
        logger.log(config.logger.level, 'loginAggregator - Exception handler.', {
            type: 'login-getTNCdetail.',
            env: config.env,
            message: error.message
        });
        return 'na';
    }

}

/**
 * @method for the Refresh Token API
 */

let refreshtokenController =(req, res) => {
    return new Promise((resolve, reject) => {
        // Create the Session and Refresh Token Object
        const tokenObj = {
                securityToken: req.tokenPayload.AGS_Token,
                refreshToken: req.tokenPayload.AGS_Refresh_Token
            }
            // Setup the AGS Rfresh Token Request Options Object
        let option = {
            method: config.constant.POST,
            url: config.constant.URL_BASE_LOGIN + "/" + req.params.mrktCd + "/" + req.params.langCd + "/rfrshSecTokn",
            data: tokenObj
        };
        //calling AGS Refresh Token Service
        helper.agsPublicDataFetcher(option, req)
            .then(result => {
                // Update the DB with the AGS Session and Resfresh Token
                return dbhelper.agsTokenUpdateByAccNum(req.tokenPayload.acctNr, result.data.securityToken, result.data.refreshToken);
            })
            .then(dbOutput => {
                // Create Middleware Session and Refresh Token
                return middleWareTokenManager(req.headers['uniqueid'], req.params.acctNr);
            })
            .then(tokenObj => {
                return resolve(tokenObj);
            })
            .catch(error => {
                return reject(error);
            })
    });
}


module.exports = { loginservice, loginAggregator, refreshtokenController, logoutUser, editUserPreferences, loginv3service };