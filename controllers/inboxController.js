'use strict'
/**
 * @author ajay dua
 * @dated 31-july-2018
 * 
/** Inbox APIs Purpose :
 
* To return list of messages for the account number received in request.
* messages associated with the account number not older then 4 weeks must be returned. 
* viewed_ind to be used to mark the message as read/unread must be part of response for each message.
* internal support for broadcast messages is there
* only the broadcast messages that are generated after the account number was registered must be returned in response. 
* for this part of the functionality, Optional registration date of the user is send as part of Get Message API request for 
to get broadcast messages to be part of response.
 As broadcast messages will be maintained as a single independent document; the viewed_ind 
of broadcast message should be managed by updating the brdcst_read_msg_list attribute of each user document with the broadcast
 message id once it is read by the user.
 */
const DateDiff = require('date-diff'), Docs = require('../models/avonnotification');

let mongoose = require('mongoose'), constants = require('../helpers/constants'), msgList = [], flag = '', resp = { msgList: msgList },
	filterParams = { user_id: '', mrkt_cd: '', lang_cd: '', ntfctn_typ: constants.AGNS_NTFCTN_TYP.INDV };

const getInboxData = function (req, res) {
	try {
		setFilterParams(req.params.acctNr, req.params.mrktCd, req.params.langCd.toLowerCase());
		let qryAcctIndMsgs = Docs.findOne(filterParams), usrRegDate = new Date(req.query.usrRegDate), brdMsgCrtionDate = null,
			indMsgRow = null, brdMsgRow = null, regMsgDiff = null;
		//lean is used here as it returns only Plain JS Object, and not the whole mongoose object(save method, getters/setters)
		qryAcctIndMsgs.lean().exec().then(function (row) {
			indMsgRow = row;
			//internal support for BRDCAST msgs here.
			if (usrRegDate instanceof Date && !isNaN(usrRegDate)) {
				delete filterParams.user_id;
				filterParams.ntfctn_typ = constants.AGNS_NTFCTN_TYP.BRODC;
				const qryAcctBrdMsgs = Docs.findOne(filterParams);
				let broadcastMsgReadList = indMsgRow.brdcst_read_msg_list;
				qryAcctBrdMsgs.lean().exec().then(function (row) {
					brdMsgRow = row;
					setIndMsgList(indMsgRow);
					brdMsgRow.messages.forEach(brdMsg => {
						brdMsgCrtionDate = brdMsg.creat_ts;
						regMsgDiff = new DateDiff(brdMsgCrtionDate, usrRegDate);
						if (regMsgDiff.seconds() > 0) {
							var bcmsg = brdMsg;
							//SET delete and viewed index values
							if (broadcastMsgReadList && broadcastMsgReadList[bcmsg.inbox_id]) {
								bcmsg.viewed_ind = broadcastMsgReadList[bcmsg.inbox_id].viewed_ind;
								bcmsg.deleted_ind = broadcastMsgReadList[bcmsg.inbox_id].deleted_ind;
							} else {
								bcmsg.viewed_ind = 0;
								bcmsg.deleted_ind = 0;
							}
							bcmsg.ntfctn_typ = constants.AGNS_NTFCTN_TYP.BRODC;
							msgList.push(bcmsg);
						}
					});
					resp = {
						msgList: msgList
					}
					setFinalResponse(res);
				}).catch(function (e) {
					setFinalResponse(res);
				});
			} else {
				setIndMsgList(indMsgRow);
				setFinalResponse(res);
			}
		}).catch(function () {
			//record not found
			setFinalResponse(res);
		});
	} catch (e) {
		setErrRes(e, res, 'getInboxData-ExpHandler.');
	}
}

const putInboxData = function (req, res) {
	try {
		flag = 'viewed_ind';
		setFilterParams(req.params.acctNr, req.params.mrktCd, req.params.langCd.toLowerCase());
		let msgToUpdate = req.body.msgList;
		let qryAcctIndMsgs = Docs.findOne(filterParams);
		modifyInboxData(qryAcctIndMsgs, msgToUpdate, res, flag);
	} catch (e) {
		setErrRes(e, res, 'putInboxData-ExpHandler');
	}
};

const delInboxData = function (req, res) {
	try {
		flag = 'deleted_ind';
		setFilterParams(req.params.acctNr, req.params.mrktCd, req.params.langCd.toLowerCase());
		let msgToUpdate = req.body.msgList;
		let qryAcctIndMsgs = Docs.findOne(filterParams);
		modifyInboxData(qryAcctIndMsgs, msgToUpdate, res, flag);
	} catch (e) {
		setErrRes(e, res, 'delInboxData-ExpHandler');
	}
};
/**
 * Helper Methods tightly coupled with the Inbox APIs
 */

/**
Purpose of this functinality is:
   *	To Modify messages based on flag in request
   *	deleted_ind or viewed_ind for INDIVIDUAL messages
   *	brdcst_read_msg_list for BROADCAST messages
*/
function modifyInboxData(qryAcctIndMsgs, msgToUpdate, res, flag) {
	qryAcctIndMsgs.lean().exec().then(function (dbResp) {
		var broadcastReadList = dbResp.brdcst_read_msg_list || {};
		msgToUpdate.forEach(e => {
			dbResp.messages.forEach(el => {
				if (e.inbox_id === el.inbox_id && e.ntfctn_typ === constants.AGNS_NTFCTN_TYP.INDV) {
					el[flag] = e[flag];
				} //Note: below code is a logic for BRODCAST shoud not be deleted.
				else if (e.ntfctn_typ === constants.AGNS_NTFCTN_TYP.BRODC) {
					if (broadcastReadList[e.inbox_id]) {
						broadcastReadList[e.inbox_id][flag] = e[flag];
					} else {
						var newBrdcastReadMsg = {
							flag: e[flag]
						};
						broadcastReadList[e.inbox_id] = newBrdcastReadMsg;
					}
				}
			});
		});

		dbResp.brdcst_read_msg_list = broadcastReadList;
		delete dbResp._id;
		Docs.findOneAndUpdate(filterParams, dbResp, {
			'new': true
		}, function (err, updatedRow) {
			if (err) {
				setExpError(res);
			} else {
				// respond with modified records
				let finalResult = [];
				msgToUpdate.forEach(e => {
					updatedRow.messages.forEach(el => {
						if (el.inbox_id == e.inbox_id) {
							finalResult.push(el);
						}
					});
				});
				res.status(200).send(finalResult);
				res.end();
			}
		});
	}).catch(function () {
		setExpError(res);
	});
}

function setErrRes(e, res, type) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	logger.log(Config.logger.level, e.message, {
		url: fullUrl,
		type: type
	});
	aiLogger.log(Config.logger.level, e.message);
	res.status(500).send({
		status: false,
		message: 'Internal server error.'
	});
	res.end();

}
function setFilterParams(a, m, l) {
	filterParams.mrkt_cd = m;
	//TODO: once finalize remove the lower case switch.
	filterParams.lang_cd = l.toLowerCase();
	filterParams.user_id = a;
}
function setFinalResponse(res) {
	res.status(200).send(resp);
	res.end();
}
function setExpError(res) {
	let errorObj = new Error('DB Error');
	errorObj.status = mro_constant.middlwrCustErrCds.dbError;
	let errorFormater = errorObjectHelper(errorObj);
	return res.status(mro_constant.httpErrCds.internalServerError)
		.send(errorFormater.mwErrorArray);

}
function setIndMsgList(indMsgRow) {
	let curDate = new Date();
	let timeDiff = null;
	let msgCrtionDate = null;

	indMsgRow.messages.forEach(indMsg => {
		indMsg.ntfctn_typ = constants.AGNS_NTFCTN_TYP.INDV;
		msgCrtionDate = indMsg.creat_ts;
		timeDiff = new DateDiff(curDate, msgCrtionDate);
		//chk msg fresh being @least not older then four weeks & not deleted by user sofar.
		if (indMsg.deleted_ind === 0 && timeDiff.weeks() < 4) {
			msgList.push(indMsg);
		}
	});
	resp = {
		msgList: msgList
	}
}

module.exports = { getInboxData, putInboxData, delInboxData }