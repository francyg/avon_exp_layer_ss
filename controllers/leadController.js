'use strict';
/**
 * @author Ashish Saurav.
 * dated- 31 may 2018
 * Controller for Lead
 */

//*************************Module Dependies********************//
// Import te Config settings
//const config = require('config');
const config = require('../configs/config.global');
// Import Helper Classes
const helper = require("../helpers/loginHelper");
const loggerHelper = require("../helpers/loggerHelper");
const errorObjectHelper = require('../helpers/errorTemplate');
const dbInteractor = require('../helpers/databaseInteractor');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
let redisClient = require('../server/client').client;
let _ = require('lodash');
let moment = require('moment');

/**
 * @method for fetching all leads for a user
 * 
 */

let getAllLeads = function(req, res, next) {
    if (!req.query) {
        let error = new Error('Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        logger.log(config.logger.level, 'Lead - Exception handler.', {
            type: 'getAllLeads - Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
    }
    /**
     * Check if the none of the Lead Status (IP, NW or CT) isprovied in the request
     */
    if (!req.query.status) {
        let error = new Error('Bad Request: Lead Status Missing');
        error.status = mro_constant.middlwrCustErrCds.leadStatusNotProvided;
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
    }
    // Added Version Support
    if(req.params.version == 2) {
        // If Version 2 , then move over to the Version 2 Controller file
        next();
    }
    else {
        // If Version 1, then stay with the current controller
        if (req.tokenPayload) {
            let statusData = req.query.status.replace(/,/g, "_");
            let key = req.params.mrktCd + '_' + req.params.langCd + '_' + req.params.version + '_' + req.params.acctNr + '_' + statusData;
            checkCache(key)
                .then(function(result) {
                    if (result) {
                        return cacheDataFinder(key);
                    } else {
                        return agsDataCaller(req, key);
                    }
                }).then(function(data) {
                    return dataJuggler(req, data);
                }).then(function(pagedData) {
                    return res.status(200).send(pagedData);
                }).catch(err => {
                    let errorData = errorObjectHelper(err);
                    return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                });
        } else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Lead - Exception handler.', {
                type: 'Lead by Id-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
        }
    }
}


/**
 * API- Get All Leads Version2
*/

let getAllLeadsV2 = function(req, res, next) {
    if (!req.query) {
        let error = new Error('Bad Request');
        error.status = mro_constant.middlwrCustErrCds.badRequest;
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
    }
    /**
     * Check if the none of the Lead Status (IP, NW or CT) isprovied in the request
     */
    if (!req.query.status) {
        let error = new Error('Bad Request: Lead Status Missing');
        error.status = mro_constant.middlwrCustErrCds.leadStatusNotProvided;
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
    }


    let statusData = req.query.status.replace(/,/g, "_");
    let key = req.params.mrktCd + '_' + req.params.langCd + '_' + req.params.version + '_' + req.params.acctNr + '_' + statusData;
    checkCache(key).then(function(result) {
            if (result) {
                return getAllLeadsDataFromRedisCache(req, key);
            } 
            else {
                return getAllLeadsFromAGS(req, key);
            }
        }).then(result => {
            return res.status(200).send(result);
        }).catch(error => {
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        }) 
}

/**
 * API to Get All Leads Data from Redis Cache
 * @param {*} req 
 * @param {*} key 
 */
let getAllLeadsDataFromRedisCache = (req, key) => {
    return new Promise((resolve, reject) => { 
        cacheDataFinder(key, "v2")
            .then(cachedData => {
                return dataJuggler(req, cachedData);
            })
            .then(pagedData => {
                return resolve(pagedData);
            })
            .catch(error => {
                return reject(error);
            })
    });
}

/**
 * API to get All leads Data from AGS Sevice
 * @param {*} req 
 * @param {*} key 
 */
let getAllLeadsFromAGS = (req, key) => {
    return new Promise((resolve, reject) => {
        let aggregatedResponse = null;
        aggregateLeadsnApptmentData(req)
            .then(result => {
                return combineLeadsnApptData(result); 
            })
            .then(finalResult => {
                aggregatedResponse = finalResult;
                return setChache(key, aggregatedResponse, "v2");
            })
            .then(cachedData => {
                return dataJuggler(req, aggregatedResponse);
            })
            .then(pagedData => {
                return resolve(pagedData);
            })
            .catch(error => {
                return reject(error);
            })
    })
}

/**
 * @method for lead by leadKey
 */
let getLeadByKey = function(req, res, next) {
        if (req.tokenPayload) {
            let option = {
                method: config.constant.GET,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + '/rep/' + req.params.acctNr + "/lead/" + req.params.leadkey,
                token: req.tokenPayload.AGS_Token
            };

            // calling helper for fetching ags data
            helper.agsDataFetcher(option, req).then(result => {
                return res.status(200).json({ data: result.data });
            }).catch(error => {
                //let error1 = new Error('Unexptected Error');
                error.status = mro_constant.middlwrCustErrCds.agsAPINotReacable;
                logger.log(config.logger.level, 'Lead - Exception handler.', {
                    type: 'Lead by Id-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                let errorData = errorObjectHelper(error);
                return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })
        } else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Lead - Exception handler.', {
                type: 'Lead by Id-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
        }
    }
    /**
     * @method for checking cache data
     * @param {string} key 
     */
let checkCache = function(key) {
        return new Promise((resolve, reject) => {
            redisClient.exists(key, function(err, result) {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(result);
                }
            })
        });
    }
    /**
     * @method for fetching data from redis.
     * @param {String} key 
     */
let cacheDataFinder = function(key, version = null) {
        return new Promise((resolve, reject) => {
            redisClient.get(key, function(err, dataset) {
                if (err) {
                    return reject(err);
                } else {
                    let finalCachedData = null;
                    if(version) {
                        let dataSet = JSON.parse(dataset);
                        finalCachedData = dataSet.data;
                    }
                    else {
                        finalCachedData = JSON.parse(dataset);
                    }
                    return resolve(finalCachedData);
                }
            })
        });
    }
    /**
     * @method for calling ags service for fetching all leads.
     * @param {Http request} req 
     * @param {*} key 
     */
let agsDataCaller = function(req, key) {
        return new Promise((resolve, reject) => {
            let option = {
                method: config.constant.GET,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + "/leads?status=" + req.query.status,
                token: req.tokenPayload.AGS_Token
            };
            let resultData;
            helper.agsDataFetcher(option, req).then(result => {
                resultData = result;
                return setChache(key, result);
            }).then(function(cached) {
                logger.log(config.logger.level, 'Redis caching for all lead of ' + req.params.acctNr, {
                    type: 'getAllLeads',
                    env: config.env,
                    message: cached
                });
                return resolve(resultData.data);
            }).catch(error => {
                return reject(error);
            });
        });
    }
    /**
     * @method for seting cache if it is not present in redis.
     * @param {string} key 
     * @param {Object} val 
     */
let setChache = function(key, val, version = null) {
        return new Promise((resolve, reject) => {

            let value = null;
            if(version) {
                let cachedData = {};
                cachedData.data = val;
                value = JSON.stringify(cachedData);
            }
            else {
                value = JSON.stringify(val.data);
            }
            redisClient.set(key, value, 'EX', config.constant.ALL_LEAD_EXPRY);
            return resolve('ok');
        });
    }
    /**
     * @method for paging
     * @param {Http Request} req 
     * @param {Object} data 
     */
let dataJuggler = function(req, data) {
    return new Promise((resolve, reject) => {
        let pageNo = parseInt(req.query.pageNo) || 0;
        // Reverse the Array.
        let sortData = data.reverse();
        let dataLen = sortData.length;
        let size = parseInt(req.query.size) >= 0 ? parseInt(req.query.size): dataLen;
        if (pageNo < 0 || pageNo === 0) {
            let error = new Error('invalid page number, should start with 1');
            error.status = mro_constant.middlwrCustErrCds.INVALID_PAGENO;
            return reject(error);
        }
        
        // if(req.params.version == 1) {
        //     sortData = data.reverse();
        //     sortData = _.orderBy(data, function(o) { return new moment(o.creatTs).format('YYYY-MM-DDTHH:mm:ss'); }, ['desc']);
        // }
        // else {
        //     sortData = _.orderBy(data, function(o) { 
        //         return new moment(o.leadData.creatTs).format('YYYY-MM-DDTHH:mm:ss'); }, ['desc']
        //     );
        // }
        
        let skip = size * (pageNo - 1);
        let finalLen = (skip + size) > 0 ? (skip + size) :dataLen;
        let finalData = _.slice(sortData, skip, finalLen);
        return resolve({ data: finalData, length: dataLen });
    });
}

/**
 * @method for changing the Lead's status
 * @param {Http Request} req 
 * @param {Object} data 
 */
/**
 *Service modified dated - 30/1/19
 *It will support lead reject and appointment delete from now in case of REJECT
 */
let updateLeadStatus = (req, res, next) => {
    // Check if Request has valid token
    if (req.tokenPayload) {
        let option = {
            method: config.constant.PUT,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + '/rep/' + req.params.acctNr + "/lead/" + req.params.leadkey + "/status/" + req.params.status,
            token: req.tokenPayload.AGS_Token,
            data: req.body
        };
        // calling helper for fetching ags data
        helper.agsDataCrawler(option, req)
            .then(result => {
               return fetchLeadDetail(req);
            }).then(leadDetail => {
                if(req.params.status.toLowerCase() == 'reject'){
                    req.params.leadId = leadDetail.leadId;
                }
                return fetApptDetail(req);
            }).then(flag => {
                if(flag) {
                    return stopAppointment(req);
                } else {
                    return true;
                }
            }).then( status => {
                return res.status(200).json({ status: "success" });
            }).catch(error => {
                logger.log(config.logger.level, 'Update Lead Status - Exception handler.', {
                    type: 'Update Lead Status-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                let errorData = errorObjectHelper(error);
                return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })
    }
    // Throw Error since valid token is not present
    else {
        let error = new Error('Invalid Token');
        error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
        logger.log(config.logger.level, 'Update Lead Status - Exception handler.', {
            type: 'Update Lead Status-Catch.',
            env: config.env,
            message: error.message,
            status: error.status
        });
        let errorData = errorObjectHelper(error);
        return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
    }
}

/**
 * @method for updating the Out of Office and Opt In/ Opt out Information
 */
let updateLeadsOutOfOffceStatus = (req, res, next) => {
        // Check if the token is valid
        if (req.tokenPayload) {
            // Check for valid body
            if (req.body.optIn === null || req.body.optIn === undefined) {
                let error = new Error('Missing Params in Body');
                error.status = mro_constant.middlwrCustErrCds.missingParams;
                let errorData = errorObjectHelper(error);
                return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
            }
            // Setup the AGS Update SL Profile Opts Options
            let option = {
                method: config.constant.PUT,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + '/rep/' + req.params.acctNr + "/lead/slProfileOpts",
                token: req.tokenPayload.AGS_Token,
                data: req.body
            };
            // calling helper for fetching ags data
            helper.agsDataFetcher(option, req).then(result => {
                // Send back 200 OK
                return res.status(200).send({ sucess: true });
            }).catch(error => {
                logger.log(config.logger.level, 'Update Lead OOO - Exception handler.', {
                    type: 'Update Lead OOO-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                let errorData = errorObjectHelper(error);
                return res.status(mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })
        } else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Update Lead OOO - Exception handler.', {
                type: 'Update Lead OOO-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
        }
    }

/**
 * Master Function to update multiple notes in one attempt
 * @param {*} req 
 * @param {*} res 
 */
let updateLeadNotesArrayFunc = (req, res) => {
    if (req.body && req.body.noteTxts && Array.isArray(req.body.noteTxts) && req.body.noteTxts.length > 0) { 
        let notesPromise = [];
        for (let note of req.body.noteTxts) {
            notesPromise.push(updateLeadNotesPromise(note, req, res));
        }
        Promise.all(notesPromise)
            .then(result => {
                return getLeadByKey(req, res);
            })
            .then(result => {
                return result;
            })
            .catch(error => {
                let errorData = errorObjectHelper(error);
                return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
            })
    }
    else {
         //throw error if the notes is empty
         let error = new Error('Invalid lead notes');
         error.status = mro_constant.middlwrCustErrCds.emptyLeadNotes;
         let errorData = errorObjectHelper(error);
         return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
    }
}    

/**
 * This function returns a Promise to Call Update Lead Notes AGS Service
 * @param {*} note 
 * @param {*} req 
 * @param {*} res 
 */
let updateLeadNotesPromise = (note, req, res) => {
    return new Promise((resolve, reject) => {
        let option = {
                method: config.constant.PUT,
                url: config.constant.URL_BASE + "/leads/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead/" + req.params.leadkey + "/note",
                token: req.tokenPayload.AGS_Token,
                data: note
        };
        return resolve(helper.agsDataFetcher(option, req)); 
    })
}
    /**
     * @method for changing the Lead's status
     * @param {Http Request} req 
     * @param {Object} payload 
     */
let updateLeadNotes = (req, res) => {
        //check if the user is logged in or not
        if (req.tokenPayload) {
            if (req.body && req.body.noteTxt && req.body.noteTxt.trim().length > 0) {
                let option = {
                    method: config.constant.PUT,
                    url: config.constant.URL_BASE + "/leads/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead/" + req.params.leadkey + "/note",
                    token: req.tokenPayload.AGS_Token,
                    data: req.body
                };

                helper.agsDataFetcher(option, req).then(result => {
                        //return res.status(200).send({ "status": "success" });
                        return getLeadByKey(req, res);
                    }).then(result => {
                        return result;
                    })
                    .catch(error => {
                        logger.log(config.logger.level, 'update lead notes - Exception handler - Catch.', {
                            type: 'update lead notes-Catch.',
                            env: config.env,
                            message: error.message,
                            status: error.status || mro_constant.httpErrCds.internalServerError
                        });
                        let errorData = errorObjectHelper(error);
                        return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
                    })
            } else {
                //throw error if the notes is empty
                let error = new Error('Invalid lead notes');
                error.status = mro_constant.middlwrCustErrCds.emptyLeadNotes;
                let errorData = errorObjectHelper(error);
                //return error object
                return res.status(mro_constant.httpErrCds.badRequest).send(errorData.mwErrorArray);
            }
        } else {
            // Case 2: Token Invalid -- Invalid session -- throw error
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            //log the error
            logger.log(config.logger.level, 'Update lead notes - Exception handler.', {
                type: 'Update lead notes-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            let errorData = errorObjectHelper(error);
            //return error object
            return res.status(mro_constant.httpErrCds.UnauthorizedAccess).send(errorData.mwErrorArray);
        }
    }
    /**
     * @method for fetching list substatus based on status
     * @param {http} req 
     * @param {http} res 
     */
let getSubStatus = (req, res) => {
        //framing requset object
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + "/leads/statuses/" + req.params.status + "/substatuses"
        };
        //calling AGS service
        helper.agsPublicDataFetcher(option, req).then(result => {
            res.setHeader('Cache-Control', 'public, max-age=86400');
            return res.status(200).send(result.data);
        }).catch(error => {
            //logging error
            let err = {
                "message": error.message,
                "status": error.status || mro_constant.httpErrCds.internalServerError
            }
            loggerHelper.log(err)

            let errorData = errorObjectHelper(error);
            //return error object
            return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
        })
    }
    /**
     * @method method for fetching eligibilty of lead.
     * @param {*} req 
     * @param {*} res 
     */
let getLeadEligibilty = (req, res, next) => {
    //Framing the request object for AGS service
    let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd +
                "/" + req.params.langCd + "/rep/" + req.params.acctNr + "/lead/eligibility",
            token: req.tokenPayload.AGS_Token
        }
        //Making a call to AGS service
    helper.agsDataFetcher(option, req).then(result => {
        return res.status(200).send(result.data);
    }).catch(error => {
        //Logging error
        logger.log(config.logger.level, 'Get Lead eligibility - Exception handler - Catch.', {
            type: 'Get Lead eligibility',
            env: config.env,
            message: error.message,
            status: error.status || mro_constant.httpErrCds.internalServerError
        });
        let errorData = errorObjectHelper(error);
        //sending back error data
        return res.status(error.status || mro_constant.httpErrCds.internalServerError).send(errorData.mwErrorArray);
    });
}

let combineLeadsnApptData = (dataholder) => {
   return new Promise((resolve, reject) => {
            // Array to hole the final Data   
            let finalResult = []; 
            // Iterate over the All Leads Array  
            _.each(dataholder.allLeads, function(leadsData, index) {
                if (Object.prototype.hasOwnProperty.call(leadsData, "daysToPurge")) {
                    leadsData.daysToPurge = leadsData.daysToPurge.toString();
                }
                if (Object.prototype.hasOwnProperty.call(leadsData, "daysToRjctn")) {
                    leadsData.daysToRjctn = leadsData.daysToRjctn.toString();
                }
                // Make an Empty Object for the aggregated Data
                let leadApptAggregatedObj = {};
                // Copy the Lead Portion of the data
                leadApptAggregatedObj.leadData = leadsData;
                // Set the Appointment portion of the data to null
                leadApptAggregatedObj.apptData = null;
                // Find the matching Appointment in Appointments Array
                dataholder.allAppts.find(apptData => {
                    if(leadsData.leadId == apptData.leadId) {
                        leadApptAggregatedObj.apptData = apptData;
                        if(Object.prototype.hasOwnProperty.call(apptData,"brthDt")){
                            leadApptAggregatedObj.leadData.brthDt = apptData.brthDt;
                        }
                        //delete leadApptAggregatedObj.apptData.addresses;
                        if(apptData.addresses) {
                            leadApptAggregatedObj.leadData.addresses = apptData.addresses;
                            if((!leadsData.geo1Key || !leadsData.geo1Nm) && apptData.addresses.length > 0) {
                                let adrs = _.find(apptData.addresses, {'addrLocTyp': 'HOME'});
                                    leadApptAggregatedObj.leadData.geo1Key = adrs.prvncKey ? adrs.prvncKey : apptData.addresses[0].prvncKey;
                                    leadApptAggregatedObj.leadData.geo2Key = adrs.countyKey ? adrs.countyKey : apptData.addresses[0].countyKey;
                                    leadApptAggregatedObj.leadData.geo3Key = adrs.cityKey ? adrs.cityKey : apptData.addresses[0].cityKey;
                                    leadApptAggregatedObj.leadData.geo4Key = adrs.postCdKey ? adrs.postCdKey : apptData.addresses[0].postCdKey;
                                    leadApptAggregatedObj.leadData.geo1Nm = adrs.prvnc ? adrs.prvnc : apptData.addresses[0].prvnc;
                                    leadApptAggregatedObj.leadData.geo2Nm = adrs.county ? adrs.county : apptData.addresses[0].county;
                                    leadApptAggregatedObj.leadData.geo3Nm = adrs.city ? adrs.city : apptData.addresses[0].city;
                                    leadApptAggregatedObj.leadData.zip = adrs.postCd ? adrs.postCd : apptData.addresses[0].postCd;
                                    leadApptAggregatedObj.leadData.addr1 =  (adrs.strAddr1Txt ? adrs.strAddr1Txt:'')+' '+(adrs.strAddr2Txt ? adrs.strAddr2Txt:'')+' '+ (adrs.bldgTxt ? adrs.bldgTxt:'')+' '+(adrs.porchTxt ? adrs.porchTxt:'')+' '+(adrs.flrTxt ? adrs.flrTxt:'' )+' '+(adrs.flatTxt ? adrs.flatTxt:'' )+' '+(adrs.doorPhonTxt ? adrs.doorPhonTxt:'')+' '+(adrs.strCdTxt ? adrs.strCdTxt:'');

                            }
                           // delete leadApptAggregatedObj.apptData.addresses;
                        }
                    }
                });
                finalResult.push(leadApptAggregatedObj);
        });
        return resolve(finalResult);
   });
}

/**
 * ALl Leads and All Appointment Data Aggregator
*/

let aggregateLeadsnApptmentData = (req) => {
    return new Promise((resolve, reject) => {
        Promise.all([getAllLeadsData(req), getAllAppintmentsData(req)])
            .then(result => {
                let dataholder = {};
                _.each(result, function(value, index) {
                        switch (index) { 
                            case 0:
                                dataholder.allLeads = value.data;
                                break;
                            case 1:
                                dataholder.allAppts = value.data;
                                break; 
                        }
                });
                return resolve(dataholder);    
            })
            .catch(error => {
                return reject(error);
            })
    });
}

/**
 * API to call Get All Leads Data from AGS
 */
let getAllLeadsData = (req) => {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + "/leads?status=" + req.query.status,
            token: req.tokenPayload.AGS_Token
        };
        helper.agsDataFetcher(option, req)
            .then(result => {
              return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    });
}


/**
 * API to call Get All Appointments Data from AGS
 */
let getAllAppintmentsData = (req) => {
    return new Promise((resolve, reject) => {
        let option = {
            method: config.constant.GET,
            url: config.constant.URL_BASE_APPT_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + "/appts?groups=all",
            token: req.tokenPayload.AGS_Token
        };
        helper.agsDataFetcher(option, req)
            .then(result => {
              return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    });
}
/**
 * 
 * @param {http} req 
 */
let fetchLeadDetail = (req) => {
    return new Promise((resolve, reject) => {
        if(req.params.status.toLowerCase() == 'reject'){
        if (req.tokenPayload) {
            let option = {
                method: config.constant.GET,
                url: config.constant.URL_BASE_LEAD_SERVICE + "/" + req.params.mrktCd + "/" + req.params.langCd + '/rep/' + req.params.acctNr + "/lead/" + req.params.leadkey,
                token: req.tokenPayload.AGS_Token
            };

            // calling helper for fetching ags data
            helper.agsDataFetcher(option, req).then(result => {
                return resolve(result.data);
            }).catch(error => {
                return reject(error);
            })
        } else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Lead - Exception handler.', {
                type: 'Lead by Id-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            return reject(error);
        }
    } else {
        return resolve(true);
    }
    });
}
/**
 * 
 * @param {http} req 
 */
let stopAppointment = (req) => {
    return new Promise((resolve, reject) => {
    if(req.params.status.toLowerCase() == 'reject'){
        if (req.tokenPayload) {
            //configure request object
            let option = {
                method: config.constant.DLETE,
                url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/lead/" + req.params.leadId,
                token: req.tokenPayload.AGS_Token
            };
            //make a call to AGS service
            helper.agsDataFetcher(option, req).then(result => {
                return resolve(true);
            }).catch(error => {
                //log the error
                logger.log(config.logger.level, 'Stop Appointment - Exception handler - Catch.', {
                    type: 'Stop Appointment-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status || mro_constant.httpErrCds.internalServerError
                });
                //return error object
                return reject(error);
            })
        }
        // Case 2: Token Invalid
        else {
            let error = new Error('Invalid Token');
            error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
            logger.log(config.logger.level, 'Stop Appointment - Exception handler.', {
                type: 'Stop Appointment-Catch.',
                env: config.env,
                message: error.message,
                status: error.status
            });
            return reject(error);
        }
    } else {
        return resolve(true);
    }
    });
    
}

let fetApptDetail = (req) => {
    return new Promise((resolve, reject) => {
        if(req.params.status.toLowerCase() == 'reject'){
            if (req.tokenPayload) {
                //configure request object
                let option = {
                    method: config.constant.GET,
                    url: config.constant.URL_BASE + "/appt/v1/rest/" + req.params.mrktCd + "/" + req.params.langCd + "/lead/" + req.params.leadId,
                    token: req.tokenPayload.AGS_Token
                };
                //make a call to AGS service
                helper.agsDataFetcher(option, req).then(result => {
                    return resolve(true);
                }).catch(error => {
                    //log the error
                    logger.log(config.logger.level, 'Fetch Appointment - Exception handler - Catch.', {
                        type: 'Fetch Appointment-Catch.',
                        env: config.env,
                        message: error.message,
                        status: error.status || mro_constant.httpErrCds.internalServerError
                    });
                    //return error object
                    if(error.data.length > 0) {
                        if(error.data[0].errCd == "100" || error.data[0].errCd == "404"){
                            return resolve(false); 
                        }
                    } else {
                        if(error.status == 404 ) {
                            return resolve(false); 
                        }else {
                            return reject(false);
                        }
                    }
                    
                })
            }
            // Case 2: Token Invalid
            else {
                let error = new Error('Invalid Token');
                error.status = mro_constant.middlwrCustErrCds.tokenInvalid;
                logger.log(config.logger.level, 'Stop Appointment - Exception handler.', {
                    type: 'Stop Appointment-Catch.',
                    env: config.env,
                    message: error.message,
                    status: error.status
                });
                return reject(error);
            }
        } else {
            return resolve(false);
        }
    });
}

module.exports = {
    getAllLeads,
    getAllLeadsV2,
    getLeadByKey,
    updateLeadStatus,
    updateLeadsOutOfOffceStatus,
    updateLeadNotes,
    getSubStatus,
    getLeadEligibilty,
    updateLeadNotesArrayFunc
};