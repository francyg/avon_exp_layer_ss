/**
 * serviceurlCtlr
 *
 * @description :: Server-side logic for managing service version APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var SSAppFeaturesAPIData = require('../models/SSAppFeaturesAPIData');
var Config = require('../configs/config.global');
var logger = require('../util/logger');
var aiLogger = require('../util/aiLogger');
var client = require('../server/client').client;

var resData = function () {
};

/**
 * GET /serviceurljson
 * Returns service Url details.
 */
exports.getserviceUrlJson = function (req, res) {
    try {
        var apiVersion = parseInt(req.params.apiVrsn);
        //var appNm = req.query.appNm || null;
        var appNm = null;
        // Setting the Envoirnment variable
        var env = process.env.ENV || null;
        // Support for new format where AppNm is URL Params
        if (req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if (req.query.appNm) {
            appNm = req.query.appNm
        }
        var data = data || {};
        var condition = condition || {};
        condition.api_version = apiVersion;
        condition.api_type = 'serviceUrl';
        condition.app_nm = appNm;
        condition.env = env;

        var redisKey = appNm + '_serviceUrl_' + apiVersion + '_' + env;
        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, configsData) {
                        if (err) {
                            resData = { status: false, code: 'SSCONF001', message: 'Error while reading redis cache.' };
                            res.send(resData);
                        } else {
                            var redisConfigsData = JSON.parse(configsData);
                            data = redisConfigsData;
                            res.setHeader('Cache-Control', 'public, max-age=86400');
                            res.send(data);
                            res.end();
                        }
                    });
                } else {
                    SSAppFeaturesAPIData.findOne(condition, { api_type: 0 }).lean().sort({ _id: -1 }).limit(1).exec(function (err, row) {
                        if (!err) {
                            if (typeof row != 'undefined' && row != null) {
                                data = row;
                                client.set(redisKey, JSON.stringify(row));
                            }  
                            res.setHeader('Cache-Control', 'public, max-age=86400');                          
                            res.send(data);
                            res.end();
                        } else {
                            res.send({ success: false, message: 'Error while fetching Service url.' });
                            res.end();
                        }
                    });
                }
            }
        });
    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'Service url - Exception handler.' });
        aiLogger.log(Config.logger.level, e.message);
        res.status(500).send({ status: false, message: 'Internal server error.' });
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};












