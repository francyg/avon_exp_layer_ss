/**
 * termscondtionsCtlr
 *
 * @description :: Server-side logic for managing lms content APIs
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 *                      -   Exception handling changes.
 *                      -   File remaned from svcversionsDao to svcversionsCtlr.
 *                      -   Folder renamed from dao to controllers.
 */

/**
 * Module dependencies.
 */
var _ = require('underscore');
var mongoose = require('mongoose');
var SSAppFeaturesAPIData = require('../models/SSAppFeaturesAPIData');
var Config = require('../configs/config.global');
var logger = require('../util/logger');
var client = require('../server/client').client;

var resData = function () {
};

/**
 * GET /lmscontent
 * Returns Terms and Conditions Content details.
 */
exports.getTgransCondtions = function (req, res) {
    try {
        var appNm = null;
        // Support for new format where AppNm is URL Params
        if (req.params.appNm) {
            appNm = req.params.appNm
        }
        // Support for the old API's where AppNm is in the Query Params
        else if (req.query.appNm) {
            appNm = req.query.appNm
        }
        appNm = (typeof appNm != 'undefined' && appNm != null)? appNm.toString().toLowerCase(): '';
        var mrktCd = req.params.mrktCd;
        var langCd = req.params.langCd;
        var apiVersion = parseInt(req.params.apiVrsn);
        var currDataVer = (typeof req.query.currDataVer != 'undefined' && req.query.currDataVer != null) ? req.query.currDataVer : '';

        var data = data || {};
        var condition = condition || {};
        condition.api_version = apiVersion;
        condition.api_type = 'terms_conditions';
        condition.app_nm = appNm;
        condition.mrkt_cd = mrktCd;
        condition.lang_cd = langCd;
        var redisKey = "";
        
        if (currDataVer) {
            condition.data_version = parseInt(currDataVer);
            redisKey = appNm + '_terms_conditions_' + mrktCd + "_" + langCd + "_" + apiVersion + "_" + currDataVer; // 'configs_' + apiVersion + '_' + mrktCd;
        }
        else {
            redisKey = appNm + '_terms_conditions_' + mrktCd + "_" + langCd + "_" + apiVersion; // 'configs_' + apiVersion + '_' + mrktCd;
        }
        client.exists(redisKey, function (err, reply) {
            if (!err) {
                if (reply === 1) {
                    client.get(redisKey, function (err, configsData) {
                        if (err) {
                            resData = { status: false, code: 'SSCONF001', message: 'Error while reading redis cache.' };
                            res.send(resData);
                        } else {
                            var redisConfigsData = JSON.parse(configsData);
                            if (typeof redisConfigsData != 'undefined' && redisConfigsData != null) {
                                data = redisConfigsData;
                            }
                            res.send(data);
                            res.end();
                        }
                    });
                } else {
                    SSAppFeaturesAPIData.findOne(condition, { api_type: 0 }).lean().sort({ _id: -1 }).limit(1).exec(function (err, row) {
                        if (!err){ 
                            if(typeof row != 'undefined' && row != null){                                        
                                switch (appNm) {
                                    case 'ao':
                                        data = {"terms_conditions":row.terms_conditions};
                                        break;
                                    default:
                                        data = row;  
                                }               
                                client.set(redisKey, JSON.stringify(data));
                            }
                            res.send(data);
                            res.end();
                            
                        } else {
                            var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
                            // console.log(err);
                            logger.log(Config.logger.level, err, { url: fullUrl, type: 'Terms conditions - Exception handler.' });
                            res.send({ success: false, message: 'Error while fetching Terms & Conditions.' });
                            res.end();
                        }
                    });
                }
            }
        });
    } catch (e) {
        /* v0.2 - Begin : Exception handling changes. */
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        logger.log(Config.logger.level, e.message, { url: fullUrl, type: 'Terms conditions - Exception handler.' });
        res.status(500).send({ status: false, message: 'Internal server error.' });
        res.end();
        /* v0.2 - End : Exception handling changes. */
    }
};