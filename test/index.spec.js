//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//let mongoose = require("mongoose");
let user = require('../models/user');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let testServer = require('../app');
let should = chai.should();
let expect = chai.expect;


chai.use(chaiHttp);

module.exports = { chai, testServer, user, should, expect};