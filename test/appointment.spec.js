var spec = require('./index.spec');
var fs = require("fs");

var should = spec.should;


describe("login and create appointment", () => {
    var token, appReqData, loginReqData, newAppData;
    before((done) => {
        fs.readFile("./test/data/login.json", "utf8", (err, data) => {
            if (err) {
                console.log(err);
            }
            data = JSON.parse(data)
            loginReqData = data;
            fs.readFile("./test/data/appointmentData.json", "utf8", (err, data) => {
                if (err) {
                    console.log(err);
                }
                data = JSON.parse(data)
                appReqData = data;
                spec.chai.request(spec.testServer)
                    .post('/login/mro/1/RO/RO')
                    .set(loginReqData.request1.headers)
                    .send(loginReqData.request1.data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        token = res.headers['x-access-token'];
                        done();
                    });
            });
        });
    });
    it("should login and create appointment", (done) => {
        spec.chai.request(spec.testServer)
            .put('/appt/mro/v1/RO/RO/' + loginReqData.response1.acctNr)
            .set('x-access-token', token)
            .set(appReqData.request1.headers)
            .send(appReqData.request1.data)
            .end((err, res) => {
                //console.log(res.body)
                // console.log(err)
                res.body.should.be.a('object');
                //res.body.should.have.property('data');
                //res.body.should.have.property('apptReqId');
                res.body.should.have.property('leadId');
                newAppData = res.body;
                if (newAppData && newAppData.apptReqId) {
                    appReqData.request1.data.apptReqId = newAppData.apptReqId
                }
                if (newAppData && newAppData.apptId) {
                    appReqData.request1.data.apptId = newAppData.apptId
                }
                if (newAppData && newAppData.leadId) {
                    appReqData.request1.data.leadId = newAppData.leadId
                }
                done();
            });
    });
    it("should get the above created appointment details", (done) => {
        //console.log(newAppData)
        spec.chai.request(spec.testServer)
            .get('/appt/mro/v1/RO/RO/' + loginReqData.response1.acctNr + '/lead/' + newAppData.leadId)
            .set('x-access-token', token)
            .set(appReqData.request1.headers)
            .end((err, res) => {
                //console.log(res.body)
                res.body.should.be.a('object');
                res.body.should.have.property('leadId');
                done();
            })
    });

    it("should check save and submit API", (done) => {

        spec.chai.request(spec.testServer)
            .post("/appt/mro/v1/RO/RO/" + loginReqData.response1.acctNr)
            .set('x-access-token', token)
            .set(appReqData.request1.headers)
            .send(appReqData.request1.data)
            .end((err, res) => {
                //console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                //res.body.should.have.property('apptReqId');
                res.body.should.have.property('leadId');
                done();
            })
    })
});


//-- -- -- -- -- -- -- --
describe("login and verify CNP, email and mobile number", () => {
    var token, appReqData, loginReqData, dupDataReq;
    before((done) => {
        fs.readFile("./test/data/login.json", "utf8", (err, data) => {
            if (err) {
                console.log(err);
            }
            data = JSON.parse(data)
            loginReqData = data;
            fs.readFile("./test/data/duplicateCheck.json", "utf8", (err, data) => {
                if (err) {
                    console.log(err);
                }
                dupDataReq = data;
                spec.chai.request(spec.testServer)
                    .post('/login/mro/1/RO/RO')
                    .set(loginReqData.request1.headers)
                    .send(loginReqData.request1.data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('data');
                        token = res.headers['x-access-token'];
                        done();
                    });
            })

        });
    });


    it("should check CNP number", (done) => {
        spec.chai.request(spec.testServer)
            .get("/dupcnp/mro/v1/RO/RO/" + loginReqData.response1.acctNr + "/" + dupDataReq.cnpNum)
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .end((err, res) => {
                //console.log(err);
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('duplicityInd');
                done();
            })
    })
    it("should check email and mobile number", (done) => {
        dupDataReq = JSON.parse(dupDataReq)
        spec.chai.request(spec.testServer)
            .post("/dupcheck/mro/v1/RO/RO/" + loginReqData.response1.acctNr)
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .send(dupDataReq.data)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('emailDup');
                res.body.should.have.property('mobileDup');
                done();
            })
    })
});