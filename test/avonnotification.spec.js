/**
 * @author ajay dua
 * @dated 26-june-2018
 * This is main intergration tests file for Inbox API
 * Mocha as test runner,chai as assertion,supertest for http requests
*/


let supertest = require('supertest');
var fs = require("fs");
//imports index is experience layer's base-framework for testing components.  
let spec = require('./index.spec');



//imports the app object for the test Enviornment
let api = supertest(spec.testServer);
let expect = spec.expect;


//middle ware function require to check the login
//let validateLogin = require('../middlewares/validateLogin');

/**
 Pre test requirement for setting headers
 
 * need user-token(x-access-token): before all tests the sub itself generate a user token.
 * need dev key: gets the  devkey from tokenAuth.
 * need uniqueid: gets the uniqueid as hardcoded value
 * need Content-Type: sets it to application/json
 */

/**
 Pre test requirement for setting request params 
 
 * need userId: gets the userId as hardcoded value.
 * need mrktCd: gets the  mrktCd as hardcoded value.
 * need langCd: gets the uniqueid as hardcoded value
 * need regDate: sets it to application/json
 */

/**
 * right now the test are for happy path only.
 * TODO: need to write test cases for unhappy path.
 * 
 */

describe("inboxAPIs integration test scripts", () => {
    console.log('start');
    var token, appReqData;
    before((done) => {
        fs.readFile("./test/data/inboxData.json", "utf8", (err, data) => {
            if (err) {
                console.log(err);
            }
            data = JSON.parse(data)
            //console.log(data);
            appReqData = data;
            api
                .post('/login/mro/1/RO/RO')
                .set({ "uniqueid": appReqData.userDetails.uniqueid })
                .send({ "userId": appReqData.userDetails.userId, "password": appReqData.userDetails.password })

                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    token = res.headers['x-access-token'];
                    done();

                });
        });
    });
    it("It MUST return corresponding messages of calling user", (done) => {
        api
            .get('/agns/inbox/1/360/messages?userId=96011000203&mrktCd=RO&langCd=ro&regDate=05/05/2018')
            .set('x-access-token', token)
            .set('uniqueid', appReqData.userDetails.uniqueid)
            .set(appReqData.headers)
            //.send(appReqData.params)
            //.send({"userId" : appReqData.params.userId,"langCd":appReqData.params.langCd,"mrktCd" : appReqData.params.mrktCd,"regDate":appReqData.params.regDate})
            .expect(200)
            .end((err, res) => {
                if (!err) {
                    expect(res.body).to.have.property('msgList');
                    done();
                }
            });
    });

    it.skip("It MUST update and set message flag after read", (done) => {
        api
            .post('/agns/inbox/1/360/messages?userId=96011000203&mrktCd=RO&langCd=ro')
            .set('x-access-token', token)
            .set('uniqueid', appReqData.userDetails.uniqueid)
            .set(appReqData.headers)
            .send(appReqData.rowData)
            .expect(200)
            .expect('OK')
            .end((err, res) => {
                if (!err) {
                    //expect(res.body).to.have.property('IncomingMessage.text');
                    //console.log(res.body);
                    done();
                }
            });

    });

    it.skip("It MUST remove msgs with flag read true", (done) => {
        api
            .delete('/agns/inbox/1/360/messages?userId=96011000203&mrktCd=RO&langCd=ro')
            .set('x-access-token', token)
            .set('uniqueid', appReqData.userDetails.uniqueid)
            .set(appReqData.headers)
            .send(appReqData.rowData)
            .expect(200)
            .end((err, res) => {
                if (!err) {
                    expect(res.body).to.have.property('IncomingMessage.text');
                    done();
                }
            });
    });

});

