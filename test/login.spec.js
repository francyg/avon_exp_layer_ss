//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//let mongoose = require("mongoose");
let user = require('../models/user');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let token;

chai.use(chaiHttp);

//Our parent block
describe('Testing Login APIs', () => {
	before((done) => { //Before each test we empty the database
		user.remove({}, (err) => { 
		   done();		   
		});	
		console.log('Removing User Collection');	
	});
 
 /*
  * Test the /login route
  */
  describe('/login/:appNm/:version/:mrktCd/:langCd', () => {
	  it('it should not login without uniqueid field', (done) => {
	  	let credentials = {userId:"0073301",password:"123abc"};
			chai.request(server)
		    .post('/login/mro/1/RO/RO')
            .send(credentials)
		    .end((err, res) => {
			  	res.should.have.status(400);
			  	res.body.should.be.a('array');
			  	res.body[0].should.have.property('errCd');
			  	res.body[0].should.have.property('errMsg').eql('Missing Params');
		      done();
		    });
	  });
	  it('it should allow a user to login', (done) => {
        let credentials = {userId:"0000360",password:"123abc"};
			chai.request(server)
            .post('/login/mro/1/RO/RO')
            .set('uniqueid','123456')
		    .send(credentials)
		    .end((err, res) => {
                res.should.have.status(200);
                res.headers.should.have.property('x-access-token');
			  	res.body.should.be.a('object');
			  	res.body.should.have.property('data');
				res.body.data.should.have.property('acctNr').eql(360);
				token = res.headers['x-access-token'];
		      done();
		    });
	  });
	  it('it should test update tnc', (done) => {
		let credentials = { "acceptedT&CVersion": 408};
		if(token){
			chai.request(server)
            .put('/tnc/mro/1/RO/RO/360')
			.set('uniqueid','123456')
			.set('x-access-token',token)
		    .send(credentials)
		    .end((err, res) => {
                res.should.have.status(200);
			  	res.body.should.be.a('object');
			  	res.body.should.have.property('success');
		      done();
		    });
		} else {
			console.error('Login Failed');
			process.exit(1);
		}
	  });
	  after(function(done) {
		// runs after all tests in this block
		user.remove({}, (err) => { 
			done();		   
		 });	
		 console.log('dumping User Collection');
	  });
  });
});
  
