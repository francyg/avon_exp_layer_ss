var spec = require('./index.spec');
var fs = require("fs");

var should = spec.should;
describe("get all substatus", () => {
    it("should get all sub status", (done) => {
        spec.chai.request(spec.testServer)
            .get('/leads/substatus/mra/1/RO/RO')
            .set('uniqueid', '123456')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            })
    })
})
describe("login and update lead notes", () => {
    var token, appReqData, loginReqData, newAppData;
    before((done) => {
        fs.readFile("./test/data/lead_login.json", "utf8", (err, data) => {
            if (err) {
                console.log(err);
            }
            data = JSON.parse(data)
            loginReqData = data;
            spec.chai.request(spec.testServer)
                .post('/login/mro/1/RO/RO')
                .set(loginReqData.request1.headers)
                .send(loginReqData.request1.data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('data');
                    token = res.headers['x-access-token'];
                    done();
                });
        });
    });
    it("should login and update lead notes", (done) => {
        spec.chai.request(spec.testServer)
            .put('/leads/mro/v1/RO/RO/' + loginReqData.response1.acctNr + "/lead/" + loginReqData.request1.req_specific_data.leadKey + "/updatenotes")
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .send(loginReqData.request1.leadNotes_data)
            .end((err, res) => {
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                done();
            });
    });
    it("should update the lead status - appoint", (done) => {
        spec.chai.request(spec.testServer)
            .put('/leads/mro/v1/RO/RO/' + loginReqData.response1.acctNr + "/lead/" + loginReqData.request1.req_specific_data.leadKey + "/status/appoint")
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('status');
                done();
            });
    })
    it("should update the lead status - contacted", (done) => {
        spec.chai.request(spec.testServer)
            .put('/leads/mro/v1/RO/RO/' + loginReqData.response1.acctNr + "/lead/" + loginReqData.request1.req_specific_data.leadKey + "/status/contacted")
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('status');
                done();
            });
    })
    it("should update the lead status - markAsCustomer", (done) => {
        spec.chai.request(spec.testServer)
            .put('/leads/mro/v1/RO/RO/' + loginReqData.response1.acctNr + "/lead/" + loginReqData.request1.req_specific_data.leadKey + "/status/markAsCustomer")
            .set('x-access-token', token)
            .set(loginReqData.request1.headers)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('status');
                done();
            });
    })
});