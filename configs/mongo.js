const config = require('./config.mongo');
var mongoose = require('mongoose');
//connect mongodb database
mongoose.connect(config.db.mongo, {server: {poolSize: 20}});
// // Handle connected event
// mongoose.connection.on('connected', function(){
// 	//logger.info('Mongoose connected to : '+config.db.mongo);
// });

// // Handle error event
// mongoose.connection.on('error', function(err){
// 	//logger.error('Mongoose connection error : '+err);
// });

// // Handle application termination event
// process.on('SIGINT', function(){
// 	mongoose.connection.close(function(){
// 		process.exit(0);
// 	});
// });