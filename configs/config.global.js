'use strict'
/**
 * Global configurations
 *
 * @description :: Defines Global configurations
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Create config object.
 */
let config = module.exports = {};

config.env = 'prod'; /* dev, uat, prod */
config.port = process.env.SSLCFG_API_PORT || 5037;
config.devkey = process.env.SSLCFG_API_DEVKEY || 'f6fe1345e0736960dab8f7e9370b44f2';
/**
 * Login settings.
 */
config.login = {};
config.login.secretkey = "developmentkey";

/**
 * Logger settings.
 */
config.logger = {};
config.logger.level = 'error'; /* error, warn, info, debug */
config.logger.logging = true; /* true, false */
config.logger.logfile = 'log'; /* log file name */
config.logger.levels = { error: 0, warn: 1, info: 2, debug: 3 }; /* 0 - 3 : high - low */
config.logger.colors = { info: "green", warn: "yellow", error: "red" };

/**
 * Session settings.
 */
config.session = {};
config.session.lifetime = 20 * 60; /* 20 minutes */

/**
 * Redis server settings.
 */
config.redis = {};
config.redis.cacheExpiryTime = 1 * 60 * 60; /* In seconds */

/**
 * Winston - Azure application insights settings.
 */
config.insights = {};
config.insights.instrumentationKey = process.env.APPINSIGHTS_INSTRUMENTATIONKEY || 'b7ba4fa9-c436-43fe-9914-c26b2b999ff6';
config.insights.autoDependencyCorrelation = true;
config.insights.autoCollectRequests = true;
config.insights.autoCollectPerformance = true;
config.insights.autoCollectExceptions = true;
config.insights.autoCollectDependencies = true;

// Token Life - 28 mins
// Refresh Token Life - 30 Days
const constant = {
    "URL_BASE_LOGIN": "https://choiceservices-uk.avon.com:443/myavon/repScrty/v1/rest",
    "URL_BASE_PROFILE_SERVICE": "https://choiceservices-uk.avon.com:443/myavon/repprofile/v1/rest",
    "URL_BASE_REPORTING_SERVICE": "https://choiceservices-uk.avon.com:443/myavon/reporting/v1/rest",
    "URL_BASE_LEAD_SERVICE": "https://choiceservices-uk.avon.com:443/myavon/leads/v1/rest",
    "URL_BASE_APPT_SERVICE": "https://choiceservices-uk.avon.com:443/myavon/appt/v1/rest",
    "URL_BASE": "https://choiceservices-uk.avon.com:443/myavon",
    "URL_BASE_CATALOGUE": "https://choiceservices-uk.avon.com/myavon/catalog",
    "URL_BASE_PUSH_NOTIFICATION": "https://webeservices3.avon.com/agws/srvc/userprfrnc",
    "SECRET": "ayfwytf2376347ashvbdajhsb!@@##$%67tshbdasbduasd7ty87hjb",
    "CONTENT_TYPE": "application/json",
    "TOKENLIFE": 1680,
    "REFRESHTOKENLIFE": 2590000,
    "secret": "383856567767467644444888",
    "REFRESHTOKENSECRET": "rurudstr4e4958ndniwswoiqaok",
    "SERVICE_TIMEOUT": 30000,
    "PUBLIC_SERVICE_TOUT": 30000,
    "GET": "get",
    "POST": "post",
    "PUT": "put",
    "DLETE": "delete",
    "CNSNTRECVDIND": false,
    "STATIC_DATA_EXPRY": 86400,
    "ALL_LEAD_EXPRY": 20,
    "APP_NAME": "QA Avon MRA",
    "PUSH_NOTIFICATION_DEV_KEY": "euRC61prFLXVRUjzVbxNQtjiUd12pDyE",
    "PUSH_NOTIFICATION_SERVICE_VERSION": "3",
    "TRUE": "Y",
    "FALSE": "N",
    "TNCUPDATEKEY": "MRA_TNC",
    "HEADER_X_ACCESS_TOKEN": "x-access-token",
    "HEADER_X_RFRSH_TOKEN": "x-rfrsh-token",
}
const URLCONST = [
    'leadcontactstatuses',
    'leadownerprofiles',
    'ineligibilityreasons',
    'leadformtypes',
    'leadSources',
    'leadtypes',
    'parameters',
    'reassignreasons',
    'substatuses',
    'statuses',
    'delegationreasons',
    'timetocontacts',
    'zonemanageroverridereasons'
]
module.exports.constant = constant;
module.exports.URLCONST = URLCONST;