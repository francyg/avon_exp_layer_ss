/**
 * MongoDB configurations.
 *
 * @description :: Defines MongoDB connection string.
 * @help        :: See https://docs.microsoft.com/en-us/azure/documentdb/documentdb-connect-mongodb-account
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

module.exports = {
    db: {
        //mongo: process.env.NODE_ENV == 'test' ? 'mongodb://localhost/mobileapiTest' : 'mongodb://localhost/mobileapi'
        //mongo: 'mongodb://172.18.64.9:27017/dev'
        //mongo: process.env.NODE_ENV == 'test' ? 'mongodb://azupsdsstddb1:03JeAaHUsD0GulUk18RuSyRpHg02GuLYVQkcRY2djPVDAvU0y20XqZgB6YqL28ziUjBRyRd59y5VLxJB5GR7SQ==@azupsdsstddb1.documents.azure.com:10250/mobappdTest?ssl=true' : 'mongodb://azupsdsstddb1:03JeAaHUsD0GulUk18RuSyRpHg02GuLYVQkcRY2djPVDAvU0y20XqZgB6YqL28ziUjBRyRd59y5VLxJB5GR7SQ==@azupsdsstddb1.documents.azure.com:10250/mobappd?ssl=true'
        mongo: process.env.SSLCFG_MONGODB_CONNSTR
    }
};


