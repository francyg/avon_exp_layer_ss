/**
 * index.js, the main configuration file. 
 *
 * @description :: All configurations inherited here.
 * @help        :: See http://expressjs.com/
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var config = require('./config.global');

var env = config.env || 'dev', cfg = require('./config.' + env);
//var env = process.env.NODE_ENV || 'dev', cfg = require('./config.'+env);

/**
 * Exports cfg object.
 */
module.exports = cfg;

