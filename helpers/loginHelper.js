'use strict'
/**
 * @author Ashish Saurav
 * @dated 04-april-2018
 * This class will act as provider that will call subsiquent method
 */


// Import Config Settings
//const config = require('config');
const config = require('../configs/config.global');
// Import library
const jwt = require('jsonwebtoken');
// Importing Axios Http Library
const axios = require("axios");
// Setting Axios Global Variables
let _ = require('lodash');
axios.defaults.headers.common['Content-Type'] = 'application/json';
// Import Error Handler Template
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
//let ailogger = require('../util/aiLogger');
const Agent = require('agentkeepalive');
//const user = require("../models/user");
const dbhelper = require("../helpers/databaseInteractor");
//-------------------Added Changes to Track Login API Via App Insights----------------
const appInsightClient = require("applicationinsights").getClient(config.insights.instrumentationKey);
module.exports = {
    agsPublicDataFetcher: (option, req) => {
        return new Promise(function(resolve, reject) {
            const keepaliveAgent = new Agent({
                keepAlive: true,
                maxSockets: 40,
                maxFreeSockets: 10
            });
            let dataHandler = {
                method: option.method,
                url: option.url,
                timeout: config.constant.PUBLIC_SERVICE_TOUT
            };
            if (option.url.match('rfrshSecTokn')) {
                dataHandler = {
                    method: option.method,
                    url: option.url,
                    data: option.data,
                    timeout: config.constant.SERVICE_TIMEOUT
                }
            }
            else if(option.url.match('userprfrnc')) {
                dataHandler = {
                    method: option.method,
                    url: option.url,
                    data: option.data,
                    timeout: config.constant.SERVICE_TIMEOUT
                }
            }
            dataHandler.agent = keepaliveAgent;
            let agsStartTime = new Date();
            axios(dataHandler)
                .then(response => {
                    let AGS_RESPONSE_TIME = new Date() - agsStartTime;
                    //---Added to Track Login API via App Insights-------------------------
                    let agsServiceName = "AGS Service:- " + dataHandler.url + "  AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                    let AGSTimings = "AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                    appInsightClient.trackRequest({ name: agsServiceName, duration: AGS_RESPONSE_TIME, resultCode: 200, success: true });
                    appInsightClient.trackTrace({ message: agsServiceName });
                    appInsightClient.trackDependency({ target: agsServiceName, name: agsServiceName, data: AGSTimings, duration: response.headers['x-elapsed-time'] });
                    return resolve(response);
                })
                .catch(error => {
                    // Check for AGS Error
                    if (error.response) {
                        let dataSet ;
                        if(error.response.data.length > 0){
                             dataSet = error.response.data[0].logId +"-" +error.response.data[0].errMsg;
                        }else {
                            dataSet = null;
                        }
                        let AGS_RESPONSE_TIME = new Date() - agsStartTime;
                        let agsServiceName = "AGS Service:- " + error.config.url + "  AGS Elapsed Time:- " + error.response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                        let AGSTimings = "AGS Elapsed Time:- " + error.response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                        appInsightClient.trackDependency({ target: error.response.headers['x-api-name'], type:error.config.method, name: error.config.url, data: dataSet, duration: error.response.headers['x-elapsed-time'] });
                        logger.log(config.logger.level, 'AGS Public Data Fetcher - Exception handler.', {
                            type: 'AGS Public Data Fetcher - Exception handler.',
                            env: config.env,
                            message: error.message,
                            status: error.response.status
                        });
                        return reject(error.response);
                    }
                    // Throw Generic Error
                    else {
                        let error = new Error('Internal Server Error');
                        error.status = mro_constant.middlwrCustErrCds.agsAccesError;
                        logger.log(config.logger.level, 'AGS Public Data Fetcher - Exception handler.', {
                            type: 'AGS Public Data Fetcher - Exception handler.',
                            env: config.env,
                            message: error.message,
                            statusText: error,
                            Stack: error.stack
                        });
                        error.httpCode = mro_constant.httpErrCds.internalServerError;
                        return reject(error);
                    }
                });
        });
    },
    agsDataFetcher: (option, req) => {
        return new Promise(function(resolve, reject) {
            const keepaliveAgent = new Agent({
                keepAlive: true,
                maxSockets: 40,
                maxFreeSockets: 10
            });
            let dataHandler;
            // check for login url
            if (!option.url.match('logn') && option.method.toLowerCase() == 'get') {
                dataHandler = {
                    method: option.method,
                    url: option.url,
                    headers: { 'x-sec-token': option.token },
                    timeout: config.constant.SERVICE_TIMEOUT
                };
            } else if (_.includes(['put', 'post'], option.method.toLowerCase()) && !option.url.match('logn')) {

                dataHandler = {
                    method: option.method,
                    url: option.url,
                    data: option.data,
                    headers: { 'x-sec-token': option.token },
                    timeout: config.constant.SERVICE_TIMEOUT
                };
            } else if (option.url.match('logn')) {
                dataHandler = {
                    method: option.method,
                    url: option.url,
                    data: option.data,
                    timeout: config.constant.SERVICE_TIMEOUT
                };
            } else {
                // check for other method in future
                dataHandler = {
                    method: option.method,
                    url: option.url,
                    headers: { 'x-sec-token': option.token },
                    timeout: config.constant.SERVICE_TIMEOUT
                };
            }
            let axisRes;
            dataHandler.agent = keepaliveAgent;
            let agsStartTime = new Date();
            axios(dataHandler).then(response => {
                let AGS_RESPONSE_TIME = new Date() - agsStartTime;
                //---Added to Track Login API via App Insights-------------------------
                let agsServiceName = "AGS Service:- " + dataHandler.url + "  AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                let AGSTimings = "AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                appInsightClient.trackRequest({ name: agsServiceName, duration: AGS_RESPONSE_TIME, resultCode: 200, success: true });
                appInsightClient.trackTrace({ message: agsServiceName });
                appInsightClient.trackDependency({ target: agsServiceName, name: agsServiceName, data: AGSTimings, duration: response.headers['x-elapsed-time'] });

                axisRes = response;
                let acno = null;
                if (req.tokenPayload) {
                    acno = req.tokenPayload.acctNr;
                } else if (req.acctNr) {
                    acno = req.acctNr;
                } else {
                    acno = response.data.acctNr;
                }
                return dbhelper.agsTokenUpdateByAccNum(acno, response.headers['x-sec-token']);

            }).then(dbOut => {
                // Added code for setting the Refresh Token
                return resolve({
                    token: axisRes.headers['x-sec-token'] || null,
                    refreshToken: axisRes.headers['x-rfrsh-token'] || null,
                    data: axisRes.data
                });
            }).catch(error => {
                // Check for AGS Error
                if (error.response) {
                    //AI logging for error
                    let dataSet ;
                    if(error.response.data.length > 0){
                         dataSet = error.response.data[0].logId +"-" +error.response.data[0].errMsg;
                    }else {
                        dataSet = null;
                    }
                    
                    appInsightClient.trackDependency({ target: error.response.headers['x-api-name'], type:error.config.method, name: error.config.url, data: dataSet, duration: error.response.headers['x-elapsed-time'] });
                    logger.log(config.logger.level, 'LoginHelper - Exception handler.', {
                        type: 'LoginHelper - Exception handler.',
                        env: config.env,
                        message: error.message,
                        statusText: error.response.statusText,
                        status: error.response.status
                    });
                    return reject(error.response);
                }
                // Throw Generic Error
                else {
                    // let error = new Error('Internal Server Error');
                    error.status = mro_constant.middlwrCustErrCds.agsAccesError;
                    logger.log(config.logger.level, 'LoginHelper - Exception handler.', {
                        type: 'LoginHelper - Exception handler.',
                        env: config.env,
                        message: error.message,
                        api: error.config.url
                    });
                    error.httpCode = mro_constant.httpErrCds.internalServerError;
                    return reject(error);
                }


            });

        });
    },
    agsDataCrawler: (option, req) => {
        return new Promise((resolve, reject) => {
            let axisRes, reqObjectHeaders, data;
            reqObjectHeaders = {
                "Content-Type": "application/json",
                'x-sec-token': option.token
            }
            data = (option.data && Object.keys(option.data).length > 0) ? option.data : ""
            let agsStartTime = new Date();
            axios.put(option.url, data, { headers: reqObjectHeaders })
                .then(response => {
                    let AGS_RESPONSE_TIME = new Date() - agsStartTime;
                    //---Added to Track Login API via App Insights-------------------------
                    let agsServiceName = "AGS Service:- " + option.url + "  AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                    let AGSTimings = "AGS Elapsed Time:- " + response.headers['x-elapsed-time'] + "  AGS Connect Time:- " + AGS_RESPONSE_TIME;
                    appInsightClient.trackRequest({ name: agsServiceName, duration: AGS_RESPONSE_TIME, resultCode: 200, success: true });
                    appInsightClient.trackTrace({ message: agsServiceName });
                    appInsightClient.trackDependency({ target: agsServiceName, name: agsServiceName, data: AGSTimings, duration: response.headers['x-elapsed-time'] });
                    axisRes = response;
                    let acno = null;
                    if (req.tokenPayload) {
                        acno = req.tokenPayload.acctNr;
                    } else if (req.acctNr) {
                        acno = req.acctNr;
                    } else {
                        acno = response.data.acctNr;
                    }
                    return dbhelper.agsTokenUpdateByAccNum(acno, response.headers['x-sec-token']);
                }).then(r => {
                    return resolve(axisRes);
                }).catch(error => {
                    //return reject(error);
                    if (error.response) {

                        let dataSet ;
                        if(error.response.data.length > 0){
                             dataSet = error.response.data[0].logId +"-" +error.response.data[0].errMsg;
                        }else {
                            dataSet = null;
                        }

                        appInsightClient.trackDependency({ target: error.response.headers['x-api-name'], type:error.config.method, name: error.config.url, data: dataSet, duration: error.response.headers['x-elapsed-time'] });
                        logger.log(config.logger.level, 'LoginHelper - Exception handler.', {
                            type: 'LoginHelper - Exception handler.',
                            env: config.env,
                            message: error.message,
                            statusText: error.response.statusText,
                            status: error.response.status
                        });
                        return reject(error.response);
                    }
                    // Throw Generic Error
                    else {
                        // let error = new Error('Internal Server Error');
                        error.status = mro_constant.middlwrCustErrCds.agsAccesError;
                        logger.log(config.logger.level, 'LoginHelper - Exception handler.', {
                            type: 'LoginHelper - Exception handler.',
                            env: config.env,
                            message: error.message,
                            api: error.config.url
                        });
                        error.httpCode = mro_constant.httpErrCds.internalServerError;
                        return reject(error);
                    }
                });
        })
    }
}