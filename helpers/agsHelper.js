'use strict'
/**
 * This class will act as provider that will invoke AGS services
 */


// Import Config Settings
const config = require('../configs/config.global');

// Importing Axios Http Library
const axios = require("axios");
// Setting Axios Global Variables
let _ = require('lodash');
axios.defaults.headers.common['Content-Type'] = 'application/json';

// Import Error Handler Template
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');

let logger = require('../util/logger');
let validator = require('../util/validator');

//const user = require("../models/user");
const dbhelper = require("../helpers/databaseInteractor");

module.exports = {
    invokeService: (options, inputs) => {
        return new Promise(function (resolve, reject) {
            axios(options)
                .then(response => {
                    // return resolve(response.data);
                    response = response.data;
                    if (validator.checkNotNull(response) && response.hasOwnProperty(inputs.respObjName) && response[inputs.respObjName].hasOwnProperty("success") && !response[inputs.respObjName].success) {
                        logger.log(config.logger.level, inputs.controllerName + ' - Exception handler', {
                            type: 'AGS returned error response - ' + inputs.serviceName,
                            env: config.env,
                            message: inputs.serviceName + " Service failed",
                            stack: response
                        });
                        return resolve({
                            success: false,
                            status: 401,
                            error: {
                                "code": response[inputs.respObjName].code,
                                "message": response[inputs.respObjName].msg
                            }
                        });
                    }
                    else if (validator.checkNotNull(response) && response.hasOwnProperty("AGSFault")) {
                        logger.log(config.logger.level, inputs.controllerName + ' - Exception handler', {
                            type: 'AGS returned validation error - ' + inputs.serviceName,
                            env: config.env,
                            message: inputs.serviceName + " Service Validation Error",
                            stack: response
                        });
                        return resolve({
                            success: false,
                            status: 500,
                            error: {
                                "code": response.AGSFault.code,
                                "message": response.AGSFault.msg
                            }
                        });
                    }
                    else {
                        return resolve({
                            success: true,
                            response: response
                        });
                    }
                })
                .catch(error => {
                    // let error = new Error('Internal Server Error');
                    logger.log(config.logger.level, 'AGS Helper - invokeService - Generic Exception handler', {
                        type: 'AGS Helper - invokeService - Generic Exception handler',
                        env: config.env,
                        message: error.message,
                        statusText: error,
                        Stack: error.stack
                    });
                    error.httpCode = config.ao.ags.internalServerErrorCode;
                    return reject(error);
                });
        });
    }
}