'use strict'
/**
 * @author Mohit Bansal
 * This class is the Controller class for Login Routes
 */

const helper = require("../helpers/loginHelper");
const encrypter = require("../helpers/genralHelper");
//const config = require('config');
let config = require('../configs/config.global');
let _ = require('lodash');
const jwt = require('jsonwebtoken');
// Import Error Handler Template
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
// Nodejs encryption with CTR
let crypto = require('crypto'), algorithm = 'aes-256-ctr';
let logger = require('../util/logger');

/**
 * @method for genrating token at middleware.
 * @param {String} deviceId
 * @param {Object} data
 */
let middleWareTokenGenerator = function(deviceId, acctnr) {
    return new Promise((resolve, reject) => {
        const data = {
            acctNr: encrypter.encrypt(acctnr.toString()),
            uniqueid: encrypter.encrypt(deviceId)
        }
        // Create the Session Token
        const token = jwt.sign(data, config.constant.SECRET, { expiresIn: config.constant.TOKENLIFE })
        // Return the Response
        return resolve(token);
    });
}

module.exports = middleWareTokenGenerator;