let config = require('../configs/config.global');

module.exports = (req, acctNr) => {
    return {
        "getUserPrfrncReq": {
            "acctNr": acctNr,
            "mrktCd": req.params.mrktCd,
            "version": config.constant.PUSH_NOTIFICATION_SERVICE_VERSION,
            "devKey": config.constant.PUSH_NOTIFICATION_DEV_KEY,
            "langCd": req.params.langCd,
            "deviceInfo": {
                "devcId": req.body.deviceDetails.devcId,
                "pltfrmNm": req.body.deviceDetails.pltfrmNm,
                "aplctnNm": req.body.deviceDetails.aplctnNm
            }
        }
    }
};
  