let config = require('../configs/config.global');

module.exports = (req, acctNr, defaultUserPreferences) => {
    return {
        "mergeUserPrfrncReq": {
            "deviceInfo": {
                "pltfrmNm": req.body.deviceDetails.pltfrmNm,
                "devcId": req.body.deviceDetails.devcId,
                "pltfrmVerTxt": req.body.deviceDetails.pltfrmVerTxt,
                "devcMakeTxt": req.body.deviceDetails.devcMakeTxt,
                "devcToknTxt": req.body.deviceDetails.devcToknTxt,
                "devcMdlTxt": req.body.deviceDetails.devcMdlTxt,
                "aplctnVerTxt": req.body.deviceDetails.aplctnVerTxt,
                "devcLcleCd": req.body.deviceDetails.devcLcleCd,
                "aplctnNm": req.body.deviceDetails.aplctnNm
            },
            "devKey": config.constant.PUSH_NOTIFICATION_DEV_KEY,
            "mrktCd": req.params.mrktCd,
            "acctNr": acctNr,
            "version": config.constant.PUSH_NOTIFICATION_SERVICE_VERSION,
            "userPrfrncList": defaultUserPreferences
        }
    }
};