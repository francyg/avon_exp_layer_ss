const helper = require("../helpers/loginHelper");
let logger = require('../util/logger');
const config = require('../configs/config.global');
const errorObjectHelper = require('../helpers/errorTemplate');

let dupEmail = (option, req) => {
    return new Promise(function(resolve, reject) {
        helper.agsDataFetcher(option, req).then(result => {
            if (result && result.data && result.data.length > 0) {
                return resolve({ "emailDup": result.data[0]['duplicityInd'] })
            } else {
                return resolve({ "emailDup": null })
            }
        }).catch(error => {
            logger.log(config.logger.level, 'dupEmail - Exception handler - Catch.', {
                type: 'dupEmail-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            if (errorData.mwErrorArray) {
                return reject({ status: errorData.status, data: errorData.mwErrorArray })
            } else {
                return reject(errorData)
            }
        })
    })
}
let dupMobile = (option, req, em_result) => {
    return new Promise(function(resolve, reject) {
        helper.agsDataFetcher(option, req).then(result => {

            if (result && result.data && result.data.length > 0) {
                em_result['mobileDup'] = result.data[0]['duplicityInd']
            } else {
                em_result['mobileDup'] = null;
            }
            return resolve(em_result)
        }).catch(error => {
            logger.log(config.logger.level, 'dupEmail - Exception handler - Catch.', {
                type: 'dupEmail-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            if (errorData.mwErrorArray) {
                return reject({ status: errorData.status, data: errorData.mwErrorArray })
            } else {
                return reject(errorData)
            }
        })
    })
}

let dupCNP = (option, req) => {
    return new Promise(function(resolve, reject) {
        helper.agsDataFetcher(option, req).then(result => {
            if (result && result.data && result.data.length > 0) {
                return resolve(result.data[0])
            } else {
                return reject({ status: mro_constant.httpErrCds.internalServerError, message: "Server error" })
            }

        }).catch(error => {
            logger.log(config.logger.level, 'dupCNP - Exception handler - Catch.', {
                type: 'dupCNP-Catch.',
                env: config.env,
                message: error.message,
                status: error.status || mro_constant.httpErrCds.internalServerError
            });
            let errorData = errorObjectHelper(error);
            if (errorData.mwErrorArray) {
                return reject({ status: errorData.status, data: errorData.mwErrorArray })
            } else {
                return reject(errorData)
            }
        })
    })
}
module.exports = { dupEmail, dupMobile, dupCNP }