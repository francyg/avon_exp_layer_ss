'use strict'
/**
 * @author Mohit Bansal
 * @dated 29april-2018
 * This class will act as provider for Middleware Error Msg template
 */
let _ = require('lodash');
const mro_constants = require('./constants');

module.exports = (errorResponse, status = null) => {
    // Placeholders for Middleware Error Message Object
    let mwErrorArray = [];
    let mwErrorObject = {};
    let mwErrorStatus = "";

    // Case 1: Check for Valid Error from AGS 
    if (errorResponse.status && _.isArray(errorResponse.data) && errorResponse.data.length) {
        mwErrorStatus = errorResponse.status;
        _.each(errorResponse.data, function(val) {
            let errCode;
            let errVal;
            let errMsg;
            if (_.indexOf(mro_constants.STRING_ERROR, val.errCd) != -1) {
                errVal = val.errCd;
                errCode = mro_constants.middlwrCustErrCds[errVal];
            } else {
                errCode = val.errCd;
                errMsg =  val.errMsg;
            }
            if(!errMsg){
                errMsg =  errVal ? errVal:mro_constants.middlwrCustErrMsgs[errCode];
            }
            mwErrorArray.push({
                errCd: errCode,
                errMsg: errMsg
            });
        });
    }
    // Case 2: Middleware Custom Error with HTTP and Custome Error Code
    else if (errorResponse.status || errorResponse.message) {
        mwErrorArray.push({
            errCd: errorResponse.status,
            errMsg: errorResponse.message || 'Internal Server Error'
        });
    }
    //Case 3: Middleware Generic Error / Internal Server Error
    else if (status) {
        mwErrorStatus = status;
        mwErrorArray.push({
            errCd: status,
            errMsg: errorResponse.message || 'Internal Server Error'
        });
    }
    //Case 3: Generic errors/ AGS/ MW
    else {
        mwErrorStatus = errorResponse.status;
        mwErrorArray.push({
            errCd: mro_constants.middlwrCustErrCds.unexpectedError,
            errMsg: errorResponse.message || 'agsAPINotReacable'
        });
    }
    // Return the Middleware Error Obj
    return {
        status: mwErrorStatus || mro_constants.httpErrCds.internalServerError,
        mwErrorArray: mwErrorArray
    }
}