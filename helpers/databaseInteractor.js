'use strict';

const user = require("../models/user");
const errorObjectHelper = require('../helpers/errorTemplate');
const mro_constant = require('../helpers/constants');
let logger = require('../util/logger');
let config = require('../configs/config.global');

let getUserDetail = function(acctNr) {
    return new Promise((resolve, reject) => {
        user.findOne({ acctNr: acctNr }, function(err, result) {
            if (err) {
                logger.log(config.logger.level, 'getUserDetail - Exception handler.', {
                    type: 'getUserDetail-momgo error.',
                    env: config.env,
                    message: err,
                });
                return reject(err);
            }
            if (result) {
                return resolve(result);
            } else {
                let error = new Error('Error finding data');
                error.status = mro_constant.middlwrCustErrCds.dbError;
                logger.log(config.logger.level, 'getUserDetail - Exception handler.', {
                    type: 'getUserDetail-momgo error.',
                    env: config.env,
                    message: err,
                });
                error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                return reject(error);
            }
        });
    });
}
let agsTokenUpdateByAccNum = function(acctNum, AGStoken, AGSRefreshToken = null) {
    return new Promise((resolve, reject) => {
        if (acctNum != null) {
            let AGSTokenObj = null;
            if(AGSRefreshToken) {
                AGSTokenObj = {AGS_Token: AGStoken,AGS_Refresh_Token: AGSRefreshToken}
            }
            else {
                AGSTokenObj = {AGS_Token: AGStoken}
            }
            user.findOneAndUpdate({ acctNr: acctNum }, 
                AGSTokenObj, 
                { upsert: true },
                function(err, data) {
                    if (err) {
                        let error = new Error('Error finding data');
                        error.status = mro_constant.middlwrCustErrCds.dbError;
                        logger.log(config.logger.level, 'getUserDetail - Exception handler.', {
                        type: 'agsTokenUpdateByAccNum-momgo error.',
                        env: config.env,
                        message: err,
                    });
                    error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                    return reject(error);
                    }
                    return resolve(data);
                }
            );
        }else {
            let error = new Error('Error finding data');
                error.status = mro_constant.middlwrCustErrCds.dbError;
                logger.log(config.logger.level, 'getUserDetail - Exception handler.', {
                type: 'agsTokenUpdateByAccNum-momgo error.',
                env: config.env,
                message: err,
            });
                error.httpCode = mro_constant.httpErrCds.service_Unavailable;
                return reject(error);
        }
    });

}
module.exports = { getUserDetail, agsTokenUpdateByAccNum };