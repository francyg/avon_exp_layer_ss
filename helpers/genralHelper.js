'use strict';
let crypto = require('crypto'),
  algorithm = 'aes-256-ctr';
const config = require('../configs/config.global');
let logger = require('../util/logger');

let encrypt = function (text) {
  var cipher = crypto.createCipher(algorithm, config.constant.SECRET)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex');
  return crypted;
}

let decrypt = function (text) {
  var decipher = crypto.createDecipher(algorithm, config.constant.SECRET)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}
const getBirthYearFromCnp = (cnp) => {
  //int INVALID_CENTURY = 99; //Inorder to match the iOS app, this field is not used
  let dateOfBirth = cnp.substring(1, 7);
  let birthYear = dateOfBirth.substring(0, 2);
  switch (cnp.charAt(0)) {
    case '1':
    case '2':
      birthYear = `19${birthYear}`; //user belongs to 20th Century
      break;
    case '3':
    case '4':
      birthYear = `18${birthYear}`; //user belongs to 19th Century
      break;
    case '5':
    case '6':
      birthYear = `20${birthYear}`; //user belongs to 21st Century
      break;
    case '7':
    case '8':
    case '9':
    case '0':
      //(7,8) user is a legal resident, not a citizen and (9,0) erroneous CNP input.
      //Inorder to match the iOS app, the above checks are not used
      birthYear = `19${birthYear}`;
      break;
    default:
      //Handle Default case
      break;
  }
  return Number(birthYear);
};

let decodeCNP = (cnp) => {
  //we need "1990-10-23", format
  let dateOfBirth = cnp.substring(1, 7);
  let birthYear = getBirthYearFromCnp(cnp);
  let birthMonth = dateOfBirth.substring(2, 4);
  let birthDay = dateOfBirth.substring(4);
  return {
    dateOfBirth: `${birthYear}-${birthMonth}-${birthDay}`
  };
};
let reWriter = function (req) {
  let whitelist = {
    "/translations/": true,
    "/faqs/": true,
    "/tnc/": true
  };
  if (req.params && req.params.mrktCd && req.params.langCd) {
    let mrktCd = req.params.mrktCd;
    let langCd = req.params.langCd;
    let rewrite = true;
    for (let api in whitelist) {
      if (Object.prototype.hasOwnProperty.call(whitelist, api) && req.path.includes(api)) {
        rewrite = false;
        if ((api == "/tnc/") && (req.method == "PUT")) {
          rewrite = true;
        }
        break;
      }
    }
    if (rewrite) {
      switch (mrktCd.toLowerCase()) {
        case "ro":
          switch (langCd.toLowerCase()) {
            case 'en':
              langCd = 'ro';
              req.params.langCd = langCd;
              break;
          }
          break;
      }
    }
  }
  return req;
}
module.exports = { encrypt, decrypt, decodeCNP, reWriter };