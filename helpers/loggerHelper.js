/**
 * @author Sri Harsha
 */
Object.defineProperty(global, '__stack', {
    get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
    get: function() {
        return __stack[2].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
    get: function() {
        return __stack[2].getFunctionName();
    }
});

Object.defineProperty(global, '__file', {
    get: function() {
        return __stack[2].getFileName();
    }
});



let logger = require('../util/logger');
const config = require('../configs/config.global');

let log = (error) => {
    /*console.log(__line);
    console.log(__function);
    console.log(__file);*/
    //let method = "Line: " + __line + ", function: " + __function

    logger.log(
        config.logger.level,
        error.message, {
            file: __file,
            line: __line,
            method: __function,
            env: config.env,
            status: error.status
                // message: error.message,
        });
}
module.exports = {
    log
}