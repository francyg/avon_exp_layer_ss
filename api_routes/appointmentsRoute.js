/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This class will hold the Appointment Routes
 */
const appointmentsController = require('../controllers/appointmentCtlr');
// Validate Request Middleware
const validateRequestMW = require('../middlewares/validateRequest');
// Import Validate Login Middleware
const validateLogin = require('../middlewares/validateLogin');
// Validate Request Middleware
const validatePublicRequestMW = require('../middlewares/validatePublicRequest');
var express = require('express');
var router = express.Router();

/**
 * login api stack
 * @param {mrktCd}
 * @param {langCd}
 * @body {userId,password}
 * @header {uniqueId}
 */
/**
 * API to create/update an Appointment
 */
router.put('/appt/:appNm/:version/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], appointmentsController.createAppointment, appointmentsController.createAppointmentv2);

/**
 * API to submit an Appointment
 */
router.post('/appt/:appNm/:version/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], appointmentsController.createAppointment, appointmentsController.createAppointmentv2);

/**
 * API to get appointment status
 */
router.get('/appt/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/:leadId', [validateRequestMW, validateLogin], appointmentsController.getAppointment);

/**
 * API to stop the Appointment process
 */
router.delete('/appt/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/:leadId', [validateRequestMW, validateLogin], appointmentsController.stopAppointment);

/**
 * API to get Static Lookup data
 */
router.get('/lookup/:appNm/:version/:mrktCd/:langCd', [validatePublicRequestMW], appointmentsController.lookup);

/**
 * API to Create a new Lead
 */

router.post('/lead/:appNm/:version/:mrktCd/:langCd/:acctNr/lead', [validateRequestMW, validateLogin], appointmentsController.createNewLead);

//router.put('/lead/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/:leadKey', [validateRequestMW, validateLogin], appointmentsController.updateLeadStatus);
/**
 * API to check duplicate mobile and email address
 */
router.post('/dupcheck/:appNm/:version/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], appointmentsController.dupCheck);

/**
 * API to check duplicate CNP number
 */
router.get('/dupcnp/:appNm/:version/:mrktCd/:langCd/:acctNr/:cnpNum/:apptId', [validateRequestMW, validateLogin], appointmentsController.cnpDupCheck);
router.get('/dupcnp/:appNm/:version/:mrktCd/:langCd/:acctNr/:cnpNum', [validateRequestMW, validateLogin], appointmentsController.cnpDupCheck);

/**
 * API to check duplicate CNP number
 */
router.get('/appt/:appNm/:version/:mrktCd/:langCd/:acctNr/:leadId/notify', [validateRequestMW, validateLogin], appointmentsController.notify);
/**
 * Exports router object.
 */
module.exports = router;