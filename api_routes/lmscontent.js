/**
 * LMS Content route
 *
 * @description :: Defines app routes for LMS Content 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var lmscontentCtlr = require('../controllers/lmscontentCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Service versions route.
 */
router.get('/:apiVrsn/lmscontent/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    lmscontentCtlr.getLMSContent(req, res);
});

// New API to be used with appNm in URL Params
router.get('/lmscontent/:appNm/:apiVrsn/:mrktCd/:langCd/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    var appNm = req.query.appNm ||'ss';
	lmscontentCtlr.getLMSContent(req, res);
});
/**
 * Exports router object.
 */
module.exports = router;

