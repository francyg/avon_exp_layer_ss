/**
 * @author Mohit Bansal
 * @dated 6 June-2018
 * This class will hold the static code routes
 */

// Import the Static Codes Controller
var staticCodesController = require('../controllers/staticCodesCtlr');
// Validate Request Middleware
const validatePublicRequestMW = require('../middlewares/validatePublicRequest');
var express = require('express');
var router = express.Router();

/**
 * login api stack
 * @param {mrktCd}
 * @param {langCd}
 * @body {T&C Accepted Version}
 * @header {uniqueId}
 */
// Version 1 - to be deprecated
router.get('/codes/:appNm/:version/:mrktCd/:langCd', validatePublicRequestMW , staticCodesController);

/**
 * Exports router object.
 */
module.exports = router;


