/**
 * Notification route
 *
 * @description :: Defines app routes for Notification
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var notificationCtlr = require('../controllers/notificationCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Service versions route.
 */
router.get('/userTasks/:appNm/:apiVrsn/:mrktCd/:langCd', tokenAuth.validateToken, function (req, res, next) {
    notificationCtlr.getUserTasks(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

