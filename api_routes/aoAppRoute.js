/**
 * This class will hold login Routes
 */

// Import the Login Controller
const aoLoginController = require('../controllers/aoLoginController');
const aoUserController = require('../controllers/aoUserController');
const avonnotificationCtlr = require('../controllers/avonNotificationCtrl');
const validateAOLogin = require('../middlewares/validateAOLogin');

// Validate Request Middleware
var express = require('express');
var router = express.Router();

/* Avon Light On Services - AGS Classic Wrapper services - Start */

router.put('/login/ao/:version/:mrktCd/:langCd', aoLoginController.login);
router.put('/logout/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoLoginController.logout);
// router.put('/logout/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoLoginController.logout);
router.post('/renewAccessToken/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoLoginController.renewAccessToken);
router.get('/refreshAccessToken/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoLoginController.refreshAccessToken);
// router.get('/refreshAccessToken/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoLoginController.refreshAccessToken);
router.put('/agreements/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoUserController.saveAgreements);
// router.put('/agreements/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoUserController.saveAgreements);
router.get('/preferences/ao/:version/:mrktCd/:langCd/:userId/:acctNr/:platfrmNm/:devcId', aoUserController.getPreferences);
// router.get('/preferences/ao/:version/:mrktCd/:langCd/:acctNr/:platfrmNm/:devcId', aoUserController.getPreferences);
// router.get('/deviceInfo/ao/:version/:mrktCd/:langCd/:acctNr', aoUserController.getDeviceInfo);
router.post('/preferences/ao/:version/:mrktCd/:langCd/:userId/:acctNr', aoUserController.mergePreferences);
// router.post('/preferences/ao/:version/:mrktCd/:langCd/:acctNr', aoUserController.mergePreferences);

/* Avon Light On Services - AGS Classic Wrapper services - End */

/* AO Get/Put/Delete Inbox Messages */

router.get('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:userId/:acctNr', validateAOLogin, function (req, res, next) {
    avonnotificationCtlr.getInboxData(req, res);
});

router.put('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:userId/:acctNr', validateAOLogin, function (req, res, next) {
    avonnotificationCtlr.updateInboxData(req, res);
});

router.delete('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:userId/:acctNr', validateAOLogin, function (req, res, next) {
    avonnotificationCtlr.deleteInboxData(req, res);
});


module.exports = router;