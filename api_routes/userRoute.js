

	/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This class will hold the User Profile Routes
 */

// Import the User Profile Controller
var userController = require('../controllers/userController');
// Validate Request Middleware
const validateRequestMW = require('../middlewares/validateRequest');
// Import Validate Login Middleware
const validateLogin = require('../middlewares/validateLogin');
var express = require('express');
var router = express.Router();

/**
 * login api stack
 * @param {mrktCd}
 * @param {langCd}
 * @body {T&C Accepted Version}
 * @header {uniqueId}
 */

/**
 * API to set the TnC version accepted by the user
 */
router.put('/tnc/:appNm/:version/:mrktCd/:langCd/:acctNr', [ validateLogin, validateRequestMW], userController.tncUpdater);
/**
 * API to get the Profile data for the Sales Lead/ Zone Manager
 */
router.get('/profile/:appNm/:version/:mrktCd/:langCd/:acctNr', [ validateLogin, validateRequestMW], userController.profileFinder);

/**
 * Exports router object.
 */
module.exports = router;


