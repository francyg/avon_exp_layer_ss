/**
 * Markets route
 *
 * @description :: Defines app routes for markets 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var marketsCtlr = require('../controllers/marketsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
var Config = require('../configs/config.global');
var logger = require('../util/logger');

/**
 * Markets route.
 */

// Old API to be used where AppName comes in Query Params
router.get('/markets/:apiVrsn', tokenAuth.validateToken, function (req, res, next) {
	var appNm = req.query.appNm||'ss';
	logger.log(Config.logger.level, appNm);
	marketsCtlr.getAppMarketList(req, res);
});

// Old API to be used where AppName comes in URL Params
router.get('/markets/:appNm/:apiVrsn', tokenAuth.validateToken, function (req, res, next) {
	var appNm = req.query.appNm||'ss';
	logger.log(Config.logger.level, appNm);
	marketsCtlr.getAppMarketList(req, res);
});


/**
 * Exports router object.
 */
module.exports = router;

