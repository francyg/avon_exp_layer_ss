/**
 * @author ajay dua
 * @dated 18-june-2018
 * 
 * Purpose follows
 * This class will hold the INBOX-API Routes and would call coresponding handler(avonNotificationCtrl) and response back 
 * the calling client.
 * note: the code was already written and working for socialsailing app.
 * requirement: needed to be a part of exp. layer so the code is moved here in experience layer.
 * added functionality: all the routes will be authenticated via AGS3,for this added functionlity few request params and headers 
 * are added -> [headers {x-access-token},{uniqueid} * 

 * Module dependencies. */
let express = require('express');
let router = express.Router();
// Import the Login Contgroller
const loginController = require('../controllers/loginControllerV2');
// Import the avonNotification Controller
var avonnotificationCtlr = require('../controllers/avonNotificationCtrl');
//** below line of code is refactor version inboxAPI Controller */


// Import the 'tokenauth' static variable
var tokenAuth = require('../util/tokenauth').TokenAuth;

// Import the 'tokenauth' static variable
var validateLogin = require('../middlewares/validateLogin');

const validateRequestMW = require('../middlewares/validateRequest');

//=========================GET Inbox======================================
/**
 * Deprecated Route for Get Inbox API.
 * Please switch over to the new Route
*/
router.get('/agns/inbox/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.getInboxData(req, res);
});

/**
 * Latest route to get Inbox API Messages
*/

router.get('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.getInboxData(req, res);
});

//=========================Put Inbox======================================
/**
 * the route will update the msgList for the specified User.
 * this route is deprecated now
 */
router.put('/agns/inbox/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.updateInboxData(req, res);
});

/**
 * the route will update the msgList for the specified User.
 * this is the updated Route for Put Inbox API
*/
router.put('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.updateInboxData(req, res);
});
 

//=========================Delete Inbox====================================
/**
 * the route will delete the msgList for the specified User.
 * this route is deprecated now. Please use the below route
 */
router.delete('/agns/inbox/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.deleteInboxData(req, res);
});

/**
 * the route will delete the msgList for the specified User.
 * this route is deprecated now. Please use the below route
 */
router.delete('/inbox/:appNm/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], function (req, res, next) {
  avonnotificationCtlr.deleteInboxData(req, res);
});


//==================Edit Device Preferences================================

/**
 * This API will be used for Device Registration
 * This API is deprecated now
 */
router.put('/agns/userPref/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], loginController.editUserPreferences);

/**
 * This API will be used for Device Registration
 * This API is the latest API for use
 */
router.put('/userPref/:appNm/:apiVrsn/:mrktCd/:langCd/:acctNr', [validateRequestMW, validateLogin], loginController.editUserPreferences);

module.exports = router;



