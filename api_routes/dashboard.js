/**
 * Dashboard route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var dashboardSshCtlr = require('../controllers/dashboardCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

// API to be used for Apps that send AppName in Query Params
router.get('/dashboard/:apiVrsn/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    dashboardSshCtlr.getdashboardSSH(req, res);
});

// API to be used for Apps that send AppName in URL Params
router.get('/dashboard/:appNm/:apiVrsn/:mrktCd/:langCd/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    dashboardSshCtlr.getdashboardSSH(req, res);
});

router.get('/:apiVrsn/dashboard/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    dashboardSshCtlr.getdashboardSSH(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

