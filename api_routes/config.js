/**
 * Configs route
 *
 * @description :: Defines app routes for configs 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var appConfigCtlr = require('../controllers/appConfigCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Configs route.
 */


// Old API to be used
router.get('/configs/:apiVrsn/:mrktCd/:mrktConfigType/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    var appNm = req.query.appNm ||'ss';
	appConfigCtlr.getAppConfig(req, res);
});

// New API to be used with appNm in URL Params
router.get('/configs/:appNm/:apiVrsn/:mrktCd/:mrktConfigType/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    var appNm = req.query.appNm ||'ss';
	appConfigCtlr.getAppConfig(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

