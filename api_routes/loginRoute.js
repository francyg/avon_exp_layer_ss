/**
 * @author Mohit Bansal
 * @dated 21-april-2018
 * This class will hold the login Routes
 */

// Import the Login Controller
const loginController = require('../controllers/loginController');
// Importing the Login Controller Version 2
const loginControllerV2 = require('../controllers/loginControllerV2');
// Validate Request Middleware
const validateRequestMW = require('../middlewares/validateRequest');
// Validate Refresh Token
const validateLogin = require('../middlewares/validateLogin');
var express = require('express');
var router = express.Router();
var tokenAuth = require('../util/tokenauth').TokenAuth;


/**
 * login api stack
 * @param {mrktCd}
 * @param {langCd}
 * @body {userId,password}
 * @header {uniqueId}
 */

/**
 * @swagger
 * definitions:
 *   Credentials:
 *     properties:
 *       userId:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * /login/{appNm}/{version}/{mrktCd}/{langCd}:
 *   post:
 *     tags:
 *       - User
 *     description: login with userId and password
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: version
 *         in: path
 *         required: true
 *         example: 1
 *         description: API Version number
 *         schema:
 *           type: string
 *       - name: mrktCd
 *         in: path
 *         required: true
 *         description: market code
 *         schema:
 *           type: string
 *       - name: langCd
 *         in: path
 *         required: true
 *         description: language code
 *         schema:
 *           type: string
 *       - name: appNm
 *         in: path
 *         required: true
 *         description: App name
 *         example: mra
 *         schema:
 *           type: string    
 *       - name: uniqueId
 *         in: header
 *         required: true
 *         description: unique-Mobile/DeviceId
 *         schema:
 *           type: string
 *       - name: puppy
 *         description: Login object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Credentials'
 *     responses:
 *       200:
 *         description: User login response
 *         schema:
 *           type: object
 *       400:
 *         description: Error format
 *         schema: 
 */
router.post('/login/:appNm/1/:mrktCd/:langCd', validateRequestMW, loginController.loginservice);

router.post('/login/:appNm/2/:mrktCd/:langCd', validateRequestMW, loginControllerV2.loginservice);

router.post('/login/:appNm/3/:mrktCd/:langCd', validateRequestMW, loginControllerV2.loginv3service);

router.post('/login/:appNm/4/:mrktCd/:langCd', tokenAuth.validateToken, validateRequestMW, loginControllerV2.loginv3service);

router.get('/testapi', (req, res) => {
    res.status(200).send("Test API Response");
})


/**
 * API for Logout
*/

router.post('/logout/:appNm/:versio/:mrktCd/:langCd/:acctNr', validateRequestMW, validateLogin, loginControllerV2.logoutUser)

/**
 * API to get new Middleware Refresh Token
*/

router.post('/refreshtoken/:appNm/:version/:mrktCd/:langCd/:acctNr', validateRequestMW, validateLogin, loginController.refreshtokenController)


/**
 * Exports router object.
 */
module.exports = router;