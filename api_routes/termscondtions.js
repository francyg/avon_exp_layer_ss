/**
 * Termscondtions Content route
 *
 * @description :: Defines app routes for LMS Content 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var termscondtionsCtlr = require('../controllers/termscondtionsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Service versions route.
 */

// API to be used for Apps that send AppName in Query Params
router.get('/tnc/:apiVrsn/:mrktCd/:langCd', tokenAuth.validateToken, function (req, res, next) {
    termscondtionsCtlr.getTgransCondtions(req, res);
});

// API to be used for Apps that send AppName in URL Params
router.get('/tnc/:appNm/:apiVrsn/:mrktCd/:langCd', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    termscondtionsCtlr.getTgransCondtions(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;