/**
 * Translations route
 *
 * @description :: Defines app routes for translations
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var translationsCtlr = require('../controllers/translationsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Translations route.
 */

// Old API to be used - for routes AppNm is on Query Params
router.get('/translations/:apiVrsn/:mrktCd/:langCd/:mrktConfigType/:downloadType', tokenAuth.validateToken, function (req, res, next) {
	var appNm = req.query.appNm||'ss';
    translationsCtlr.getAppMarketTranslations(req, res);
});

// New API to be used - for new routes where appNm is on Path Params
router.get('/translations/:appNm/:apiVrsn/:mrktCd/:langCd/:mrktConfigType/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    translationsCtlr.getAppMarketTranslations(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

