'use strict';
/**
 * @author Ashish Saurav.
 * dated- 31 may 2018
 * Route handler for Lead
 */
const leadController = require('../controllers/leadController');
// Validate Request Middleware
const validateRequestMW = require('../middlewares/validateRequest');
// Import Validate Login Middleware
const validateLogin = require('../middlewares/validateLogin');
// Validate Request Middleware
const validatePublicRequestMW = require('../middlewares/validatePublicRequest');
let express = require('express');
let router = express.Router();


/**
 * API to get all leads
 */
router.get('/leads/:appNm/:version/:mrktCd/:langCd/:acctNr/leads', [validateRequestMW, validateLogin], leadController.getAllLeads, leadController.getAllLeadsV2);
/**
 * API to get Lead's profile
 */
router.get('/leads/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/:leadkey', [validateRequestMW, validateLogin], leadController.getLeadByKey);
/**
 * API to change the status of Lead in DB
 */
router.put('/leads/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/:leadkey/status/:status', [validateRequestMW, validateLogin], leadController.updateLeadStatus);
/**
 * API to update the Out of status and Opt In/ Opt Out status for an individual SalesLeader/
 * or Zone Manager
 */
router.put('/leads/:appNm/:version/:mrktCd/:langCd/:acctNr/lead/slProfileOpts', [validateRequestMW, validateLogin], leadController.updateLeadsOutOfOffceStatus);

/**
 * API to update lead notes
 **/

router.put('/leads/:appNm/1/:mrktCd/:langCd/:acctNr/lead/:leadkey/updatenotes', [validateRequestMW, validateLogin], leadController.updateLeadNotes);

router.put('/leads/:appNm/2/:mrktCd/:langCd/:acctNr/lead/:leadkey/updatenotes', [validateRequestMW, validateLogin], leadController.updateLeadNotesArrayFunc);

/**
 * API to get all sub statuses based on a particular Status code
 * This is Public API
 */
router.get('/leads/:appNm/:version/:mrktCd/:langCd/statuses/:status/substatuses', validatePublicRequestMW, leadController.getSubStatus);

/**
 * API to get lead eligibility
 */
router.get('/leads/:appNm/:version/:mrktCd/:langCd/:acctNr/leads/slProfileOpts', [validateRequestMW, validateLogin], leadController.getLeadEligibilty);

module.exports = router;