/**
 * Helps route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var helpsCtlr = require('../controllers/helpsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Service versions route.
 */
router.get('/:apiVrsn/helps/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    helpsCtlr.getHelps(req, res);
});

// New API to be used with appNm in URL Params
router.get('/helps/:appNm/:apiVrsn/:mrktCd/:langCd/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    var appNm = req.query.appNm ||'ss';
	helpsCtlr.getHelps(req, res);
});

/**
 * Exports router object.
 */
module.exports = router;

