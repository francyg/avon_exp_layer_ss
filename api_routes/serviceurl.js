/**
 * Service url route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var serviceurlCtlr = require('../controllers/serviceurlCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;

/**
 * Service versions route.
 */

 
// New API to be used - Version 2 Final API
router.get('/serviceendpoints/:appNm/:apiVrsn', tokenAuth.validateToken, function (req, res, next) {
    serviceurlCtlr.getserviceUrlJson(req, res);
});


/**
 * Exports router object.
 */
module.exports = router;

