/**
 * Help support route
 *
 * @description :: Defines app routes for Notification
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var helpsupportCtlr = require('../controllers/helpsupportCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Service versions route.
 */
router.get('/:apiVrsn/helpSupports/:mrktCd/:langCd/:downloadType', tokenAuth.validateToken, function (req, res, next) {
    helpsupportCtlr.getHelpSupports(req, res);
});

// New API to be used with appNm in URL Params
router.get('/helpSupports/:appNm/:apiVrsn/:mrktCd/:langCd/:downloadType', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    var appNm = req.query.appNm ||'ss';
	helpsupportCtlr.getHelpSupports(req, res);
});
/**
 * Exports router object.
 */
module.exports = router;

