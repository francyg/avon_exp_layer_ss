/**
 * Service version route
 *
 * @description :: Defines app routes for service versions 
 * @help        :: See http://expressjs.com/en/guide/routing.html
 * Version History ::
 *              0.1     -   Initial Draft
 * 		0.2     -   Review comment fixes done.
 *                      -   Appropriate comments added in the code.
 */

/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router();
var svcversionsCtlr = require('../controllers/svcversionsCtlr');
var tokenAuth = require('../util/tokenauth').TokenAuth;
// Validate Request Middleware
const validateReqParams = require('../middlewares/validateReqParams');

/**
 * Service versions route.
 */


// Old API - To be used for is AppName is specified in Query Params
router.get('/svcversions/:apiVrsn/:mrktCd', tokenAuth.validateToken, function (req, res, next) {
    svcversionsCtlr.getAppServiceVersions(req, res);
});

// New API - To be used for is AppName is specified in URL Params
router.get('/svcversions/:appNm/:apiVrsn/:mrktCd', [validateReqParams, tokenAuth.validateToken], function (req, res, next) {
    svcversionsCtlr.getAppServiceVersions(req, res);
});


/**
 * Exports router object.
 */
module.exports = router;

